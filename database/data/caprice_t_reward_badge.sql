-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: caprice
-- ------------------------------------------------------
-- Server version	5.7.20-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `t_reward_badge`
--

LOCK TABLES `t_reward_badge` WRITE;
/*!40000 ALTER TABLE `t_reward_badge` DISABLE KEYS */;
INSERT INTO `t_reward_badge` VALUES (1,'Vote Up','Simple',NULL,'#007bff',1),(2,'Vote Down','Simple',NULL,'#007bff',1),(3,'Annotation Created','Simple',NULL,'#007bff',0),(4,'Document Added','Simple',NULL,'#007bff',0),(5,'Product Added','Simple',NULL,'#007bff',0),(6,'Annotation Upvoted','Simple',NULL,'#28a745',2),(7,'Document Upvoted','Simple',NULL,'#28a745',2),(8,'Product Upvoted','Simple',NULL,'#28a745',2),(9,'Annotation Downvoted','Simple',NULL,'#dc3545',-1),(10,'Document Downvoted','Simple',NULL,'#dc3545',-3),(11,'Product Downvoted','Simple',NULL,'#dc3545',-5),(12,'Annotation Validated','Special',NULL,'#28a745',2),(13,'Document Validated','Special',NULL,'#28a745',5),(14,'Product Validated','Special',NULL,'#28a745',10),(15,'Annotation Invalidated','Special',NULL,'#dc3545',-2),(16,'Document Invalidated','Special',NULL,'#dc3545',-5),(17,'Product Invalidated','Special',NULL,'#dc3545',-10),(18,'Total Valid Annotations 1','Goal',NULL,'#007bff',1),(19,'Total Valid Annotations 10','Goal',NULL,'#007bff',10),(20,'Total Valid Annotations 50','Goal',NULL,'#007bff',50),(21,'Total Valid Annotations 100','Goal',NULL,'#007bff',100),(22,'Total Valid Annotations 500','Goal',NULL,'#007bff',500),(23,'Total Valid Annotations 1000','Goal',NULL,'#007bff',1000);
/*!40000 ALTER TABLE `t_reward_badge` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed
