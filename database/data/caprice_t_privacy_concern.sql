-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: caprice
-- ------------------------------------------------------
-- Server version	5.7.20-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `t_privacy_concern`
--

LOCK TABLES `t_privacy_concern` WRITE;
/*!40000 ALTER TABLE `t_privacy_concern` DISABLE KEYS */;
INSERT INTO `t_privacy_concern` VALUES (1,'First Party Collection/Use','Privacy concern describing data collection or data use by the company/organization owning the website or mobile app.',NULL,'rgba(240, 147, 43,1.0)','rgba(255, 190, 118,1.0)'),(2,'Third Party Sharing/Collection','Privacy concern describing data sharing with third parties or possible data collection by third parties. A third party is a company/organization other than the first party company/organization that owns the website or mobile app.',NULL,'rgba(235, 77, 75,1.0)','rgba(255, 121, 121,1.0)'),(3,'User Choice/Control','Concern that describes general choices and privacy control options available to users.',NULL,'rgba(106, 176, 76,1.0)','rgba(186, 220, 88,1.0)'),(4,'Data Retention','Privacy concern specifying the retention period for collected user information.',NULL,'rgba(34, 166, 179,1.0)','rgba(126, 214, 223,1.0)'),(5,'Data Security','Concern that describes how users’ information is secured and protected, e.g., from confidentiality, integrity, or availability breaches. Common practices include the encryption of stored data and online communications.',NULL,'rgba(190, 46, 221,1.0)','rgba(224, 86, 253,1.0)'),(6,'User Access, Edit and Deletion','Privacy concern specifing whether users can access, edit or delete the data that the company/organization has about them.',NULL,'rgba(72, 52, 212,1.0)','rgba(104, 109, 224,1.0)'),(7,'Other','Describes various other concerns',NULL,'rgba(249, 202, 36,1.0)','rgba(246, 229, 141,1.0)'),(8,'International and Specific Audiences','Specific audiences mentioned in the company/organization’s privacy policy, such as children or international users, for which the company/organization may provide special provisions.',NULL,'rgba(199, 236, 238,1.0)','rgba(223, 249, 251,1.0)'),(9,'Policy Change','The company/organization’s practices concerning if and how users will be informed of changes to its privacy policy, including any choices offered to users.',NULL,'rgba(19, 15, 64,1.0)','rgba(48, 51, 107,0.7)'),(10,'Do Not Track','Practices that explain if and how Do Not Track signals (DNT) for online tracking and advertising are honored.',NULL,'rgba(83, 92, 104,1.0)','rgba(48, 51, 107,0.7)');
/*!40000 ALTER TABLE `t_privacy_concern` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed
