-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: caprice
-- ------------------------------------------------------
-- Server version	5.7.20-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `t_privacy_concern_attribute`
--

LOCK TABLES `t_privacy_concern_attribute` WRITE;
/*!40000 ALTER TABLE `t_privacy_concern_attribute` DISABLE KEYS */;
INSERT INTO `t_privacy_concern_attribute` VALUES (1,1,1,1,0,2,''),(2,1,2,0,0,6,'about '),(3,1,3,0,0,5,'User has the choice to'),(4,1,4,0,0,7,'Collection/Use is done'),(5,1,5,1,0,1,'This product'),(6,1,6,0,0,8,'By this act, the user data is'),(7,1,7,1,1,3,'information about'),(8,1,8,1,0,4,'to use it for'),(9,1,9,0,0,9,'This applies to users'),(10,2,10,1,0,3,''),(11,2,2,0,0,7,'about'),(12,2,3,0,0,6,'User has the choice to'),(13,2,5,1,0,2,','),(14,2,6,0,0,8,'By this act, the user data is'),(15,2,7,1,1,4,'information about'),(16,2,8,1,0,5,'to use it for'),(17,2,11,1,0,1,'A/The'),(18,2,9,0,0,9,'This applies to users'),(19,3,2,1,0,2,'the'),(20,3,3,1,0,1,'The user can'),(21,3,7,1,1,3,'about'),(22,3,8,1,0,4,'for'),(23,3,9,0,0,5,'This applies to users'),(24,4,7,1,1,1,''),(25,4,12,1,0,2,'data are kept for'),(26,4,13,1,0,3,'because of'),(27,5,14,1,0,1,'User data are secured by'),(28,6,15,1,0,2,'data about'),(29,6,16,1,0,1,'User can'),(30,6,9,0,0,3,'This applies to users'),(31,7,17,1,0,1,'Selected part of text is about'),(32,8,18,1,0,1,'Selected part of text is about'),(33,9,19,1,0,1,'When'),(34,9,20,1,0,2,'happens, user will be notified by'),(35,9,21,1,0,3,'having the choice to'),(36,10,22,1,0,1,'Browser\'s \'Do Not Track\' flag is');
/*!40000 ALTER TABLE `t_privacy_concern_attribute` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed
