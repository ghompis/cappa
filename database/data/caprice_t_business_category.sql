-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: caprice
-- ------------------------------------------------------
-- Server version	5.7.20-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `t_business_category`
--

LOCK TABLES `t_business_category` WRITE;
/*!40000 ALTER TABLE `t_business_category` DISABLE KEYS */;
INSERT INTO `t_business_category` VALUES (1,'Shopping',NULL,NULL),(2,'Family & Parenting',NULL,NULL),(3,'Pets',NULL,NULL),(4,'Electronics & Audio',NULL,NULL),(5,'History',NULL,NULL),(6,'Events',NULL,NULL),(7,'Communication',NULL,NULL),(8,'Business',NULL,NULL),(9,'Newspapers & Magazines',NULL,NULL),(10,'World',NULL,9),(11,'Science',NULL,9),(12,'Fashion & Style',NULL,9),(13,'Politics',NULL,9),(14,'Teens',NULL,NULL),(15,'Lifestyle',NULL,NULL),(16,'Weather',NULL,NULL),(17,'Economy',NULL,NULL),(18,'Energy & Natural Resources',NULL,NULL),(19,'Religion',NULL,NULL),(20,'Hobbies',NULL,NULL),(21,'Sports',NULL,20),(22,'Cars & Vehicles (Automotive)',NULL,20),(23,'Transport',NULL,NULL),(24,'Travel & Local Information',NULL,23),(25,'Maps & Navigation',NULL,23),(26,'Art & Design',NULL,NULL),(27,'Cooking, Food & Drink',NULL,NULL),(28,'Real Estate',NULL,NULL),(29,'Garden, House & Decoration',NULL,NULL),(30,'Fun & Entertainment',NULL,NULL),(31,'Comic',NULL,30),(32,'Games',NULL,30),(33,'Board Games',NULL,32),(34,'Puzzle',NULL,32),(35,'General Knowledge Games',NULL,32),(36,'Educational Games',NULL,32),(37,'Card Games',NULL,32),(38,'Family Games',NULL,32),(39,'Simulation Games',NULL,32),(40,'Strategy Games',NULL,32),(41,'Adventure Games',NULL,32),(42,'Arcade Games',NULL,32),(43,'Racing Games',NULL,32),(44,'Trivia Games',NULL,32),(45,'Word Games',NULL,32),(46,'RPG Games',NULL,32),(47,'Gambling Games',NULL,32),(48,'Education',NULL,NULL),(49,'Books & References',NULL,48),(50,'Libraries & Publishers',NULL,48),(51,'Tools',NULL,NULL),(52,'Productivity',NULL,51),(53,'Personalization',NULL,51),(54,'Health, Fitness & Beauty',NULL,NULL),(55,'Fitness',NULL,54),(56,'Beauty',NULL,54),(57,'Health',NULL,54),(58,'Medical',NULL,57),(59,'Medicine',NULL,57),(60,'Social',NULL,NULL),(61,'Dating & Meeting',NULL,60),(62,'Multimedia',NULL,NULL),(63,'Images & Photo',NULL,62),(64,'Movies & Video',NULL,62),(65,'Music & Sound',NULL,62),(66,'Other',NULL,NULL),(67,'News',NULL,NULL),(69,'Regional',NULL,NULL),(70,'Computers & Internet',NULL,NULL),(71,'Recreation',NULL,NULL);
/*!40000 ALTER TABLE `t_business_category` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed
