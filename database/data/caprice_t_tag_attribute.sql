-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: caprice
-- ------------------------------------------------------
-- Server version	5.7.20-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `t_tag_attribute`
--

LOCK TABLES `t_tag_attribute` WRITE;
/*!40000 ALTER TABLE `t_tag_attribute` DISABLE KEYS */;
INSERT INTO `t_tag_attribute` VALUES (1,'Action First-Party','How does the first party collect, track, or obtain user information?'),(2,'Choice Scope','What scope does the user\'s choice or control apply to, i.e., first party collection/use or third party collection/use. Note that sometimes use of information can be limited, but the information is still collected from users.'),(3,'Choice Type','Denote if user choices are explicitly offered for this practice.'),(4,'Collection Mode','Denote if the data collection performed by the first party is implicit (e.g., company collects information without user\'s explicit awareness) or explicit (e.g., user provides information).'),(5,'Does/Does Not','Denote if the policy explicitly states that something is NOT done.'),(6,'Identifiability','Denote if it is explicitly stated whether the information or data practice is linked to the user\'s identity or if it is anonymous.'),(7,'Personal Information Type','What category of information is collected or tracked by the company/organization?'),(8,'Purpose','What is the purpose of collecting or using user information?'),(9,'User Type','What kind of users are mentioned?'),(10,'Action Third Party','How does the third-party receive, collect, track, or see user information.'),(11,'Third Party Entity','The third-party involved in the data practice.'),(12,'Retention Period','Description of the retention period, i.e., how long data is stored.'),(13,'Retention Purpose','The purpose to which the retention practice applies.'),(14,'Security Measure','Policy statements that describe the type of security that the website/app implements to protect users’ information.'),(15,'Access Scope','If access is offered, what data does it apply to.'),(16,'Access Type','Options offered for users to access, edit, delete information that the company/organization has about them.'),(17,'Other Type','Select to mention other policy segments'),(18,'Audience Type','Select which audience the policy segment refers to'),(19,'Change Type','For what type of changes to the website/app’s policy are users notified.'),(20,'Notification Type','How is the user notified when the privacy policy changes.'),(21,'User Choice','What choices/options are offered to the user when the policy changes.'),(22,'Do Not Track Policy','If and how Do-Not-Track signals (DNT) are honored.');
/*!40000 ALTER TABLE `t_tag_attribute` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed
