CREATE DATABASE  IF NOT EXISTS `caprice` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `caprice`;
-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: caprice
-- ------------------------------------------------------
-- Server version	5.7.20-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Temporary view structure for view `v_user_output_votes`
--

DROP TABLE IF EXISTS `v_user_output_votes`;
/*!50001 DROP VIEW IF EXISTS `v_user_output_votes`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `v_user_output_votes` AS SELECT 
 1 AS `id`,
 1 AS `output_upvotes_count`,
 1 AS `output_downvotes_count`,
 1 AS `annotation_output_upvotes_count`,
 1 AS `annotation_output_downvotes_count`,
 1 AS `legal_document_output_upvotes_count`,
 1 AS `legal_document_output_downvotes_count`,
 1 AS `digital_product_output_upvotes_count`,
 1 AS `digital_product_output_downvotes_count`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `v_user_entities_clarified`
--

DROP TABLE IF EXISTS `v_user_entities_clarified`;
/*!50001 DROP VIEW IF EXISTS `v_user_entities_clarified`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `v_user_entities_clarified` AS SELECT 
 1 AS `id`,
 1 AS `validated_count`,
 1 AS `invalidated_count`,
 1 AS `annotation_validated_count`,
 1 AS `annotation_invalidated_count`,
 1 AS `legal_document_validated_count`,
 1 AS `legal_document_invalidated_count`,
 1 AS `digital_product_validated_count`,
 1 AS `digital_product_invalidated_count`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `v_user_entities_clarified_bckp`
--

DROP TABLE IF EXISTS `v_user_entities_clarified_bckp`;
/*!50001 DROP VIEW IF EXISTS `v_user_entities_clarified_bckp`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `v_user_entities_clarified_bckp` AS SELECT 
 1 AS `id`,
 1 AS `validated_count`,
 1 AS `invalidated_count`,
 1 AS `annotation_validated_count`,
 1 AS `annotation_invalidated_count`,
 1 AS `legal_document_validated_count`,
 1 AS `legal_document_invalidated_count`,
 1 AS `digital_product_validated_count`,
 1 AS `digital_product_invalidated_count`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `v_user_additions`
--

DROP TABLE IF EXISTS `v_user_additions`;
/*!50001 DROP VIEW IF EXISTS `v_user_additions`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `v_user_additions` AS SELECT 
 1 AS `id`,
 1 AS `metadata_created`,
 1 AS `annotations_created`,
 1 AS `legal_documents_added`,
 1 AS `digital_products_added`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `v_user_input_votes`
--

DROP TABLE IF EXISTS `v_user_input_votes`;
/*!50001 DROP VIEW IF EXISTS `v_user_input_votes`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `v_user_input_votes` AS SELECT 
 1 AS `id`,
 1 AS `input_upvotes_count`,
 1 AS `input_downvotes_count`,
 1 AS `annotation_input_upvotes_count`,
 1 AS `annotation_input_downvotes_count`,
 1 AS `legal_document_input_upvotes_count`,
 1 AS `legal_document_input_downvotes_count`,
 1 AS `digital_product_input_upvotes_count`,
 1 AS `digital_product_input_downvotes_count`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `v_controversial_entity_votes`
--

DROP TABLE IF EXISTS `v_controversial_entity_votes`;
/*!50001 DROP VIEW IF EXISTS `v_controversial_entity_votes`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `v_controversial_entity_votes` AS SELECT 
 1 AS `id`,
 1 AS `votes_up`,
 1 AS `votes_down`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `v_user_stats`
--

DROP TABLE IF EXISTS `v_user_stats`;
/*!50001 DROP VIEW IF EXISTS `v_user_stats`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `v_user_stats` AS SELECT 
 1 AS `id`,
 1 AS `output_upvotes_count`,
 1 AS `output_downvotes_count`,
 1 AS `annotation_output_upvotes_count`,
 1 AS `annotation_output_downvotes_count`,
 1 AS `legal_document_output_upvotes_count`,
 1 AS `legal_document_output_downvotes_count`,
 1 AS `digital_product_output_upvotes_count`,
 1 AS `digital_product_output_downvotes_count`,
 1 AS `input_upvotes_count`,
 1 AS `input_downvotes_count`,
 1 AS `annotation_input_upvotes_count`,
 1 AS `annotation_input_downvotes_count`,
 1 AS `legal_document_input_upvotes_count`,
 1 AS `legal_document_input_downvotes_count`,
 1 AS `digital_product_input_upvotes_count`,
 1 AS `digital_product_input_downvotes_count`,
 1 AS `validated_count`,
 1 AS `invalidated_count`,
 1 AS `annotation_validated_count`,
 1 AS `annotation_invalidated_count`,
 1 AS `legal_document_validated_count`,
 1 AS `legal_document_invalidated_count`,
 1 AS `digital_product_validated_count`,
 1 AS `digital_product_invalidated_count`,
 1 AS `metadata_created`,
 1 AS `annotations_created`,
 1 AS `legal_documents_added`,
 1 AS `digital_products_added`*/;
SET character_set_client = @saved_cs_client;

--
-- Final view structure for view `v_user_output_votes`
--

/*!50001 DROP VIEW IF EXISTS `v_user_output_votes`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `v_user_output_votes` AS select `e_user`.`id` AS `id`,sum(if((`e_vote`.`vote_value` > 0),1,0)) AS `output_upvotes_count`,sum(if((`e_vote`.`vote_value` < 0),1,0)) AS `output_downvotes_count`,sum(if(((`e_vote`.`vote_value` > 0) and (`e_annotation`.`id` is not null)),1,0)) AS `annotation_output_upvotes_count`,sum(if(((`e_vote`.`vote_value` < 0) and (`e_annotation`.`id` is not null)),1,0)) AS `annotation_output_downvotes_count`,sum(if(((`e_vote`.`vote_value` > 0) and (`e_legal_document`.`id` is not null)),1,0)) AS `legal_document_output_upvotes_count`,sum(if(((`e_vote`.`vote_value` < 0) and (`e_legal_document`.`id` is not null)),1,0)) AS `legal_document_output_downvotes_count`,sum(if(((`e_vote`.`vote_value` > 0) and (`e_digital_product`.`id` is not null)),1,0)) AS `digital_product_output_upvotes_count`,sum(if(((`e_vote`.`vote_value` < 0) and (`e_digital_product`.`id` is not null)),1,0)) AS `digital_product_output_downvotes_count` from ((((((`e_user` left join `e_metadata` on((`e_metadata`.`created_by_id` = `e_user`.`id`))) left join `e_vote` on((`e_vote`.`metadata_id` = `e_metadata`.`id`))) left join `e_controversial_entity` on((`e_controversial_entity`.`id` = `e_vote`.`controversial_entity_id`))) left join `e_annotation` on((`e_controversial_entity`.`id` = `e_annotation`.`controversial_entity_id`))) left join `e_legal_document` on((`e_controversial_entity`.`id` = `e_legal_document`.`controversial_entity_id`))) left join `e_digital_product` on((`e_controversial_entity`.`id` = `e_digital_product`.`controversial_entity_id`))) group by `e_user`.`id` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_user_entities_clarified`
--

/*!50001 DROP VIEW IF EXISTS `v_user_entities_clarified`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `v_user_entities_clarified` AS select `e_user`.`id` AS `id`,sum(if((`e_controversial_entity`.`score_min_confidence_limit` > 0),1,0)) AS `validated_count`,sum(if((`e_controversial_entity`.`score_max_confidence_limit` < 0),1,0)) AS `invalidated_count`,sum((if((`e_controversial_entity`.`score_min_confidence_limit` > 0),1,0) and (`e_annotation`.`id` is not null))) AS `annotation_validated_count`,sum((if((`e_controversial_entity`.`score_max_confidence_limit` < 0),1,0) and (`e_annotation`.`id` is not null))) AS `annotation_invalidated_count`,sum((if((`e_controversial_entity`.`score_min_confidence_limit` > 0),1,0) and (`e_legal_document`.`id` is not null))) AS `legal_document_validated_count`,sum((if((`e_controversial_entity`.`score_max_confidence_limit` < 0),1,0) and (`e_legal_document`.`id` is not null))) AS `legal_document_invalidated_count`,sum((if((`e_controversial_entity`.`score_min_confidence_limit` > 0),1,0) and (`e_digital_product`.`id` is not null))) AS `digital_product_validated_count`,sum((if((`e_controversial_entity`.`score_max_confidence_limit` < 0),1,0) and (`e_digital_product`.`id` is not null))) AS `digital_product_invalidated_count` from ((((((`e_user` left join `e_metadata` on((`e_metadata`.`created_by_id` = `e_user`.`id`))) left join `e_controversial_entity` on((`e_controversial_entity`.`metadata_id` = `e_metadata`.`id`))) left join `e_annotation` on((`e_controversial_entity`.`id` = `e_annotation`.`controversial_entity_id`))) left join `e_legal_document` on((`e_controversial_entity`.`id` = `e_legal_document`.`controversial_entity_id`))) left join `e_digital_product` on((`e_controversial_entity`.`id` = `e_digital_product`.`controversial_entity_id`))) left join `e_vote` on((`e_vote`.`controversial_entity_id` = `e_controversial_entity`.`id`))) group by `e_user`.`id` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_user_entities_clarified_bckp`
--

/*!50001 DROP VIEW IF EXISTS `v_user_entities_clarified_bckp`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `v_user_entities_clarified_bckp` AS select `votes_per_entity`.`id` AS `id`,sum(if(((`votes_per_entity`.`upvotes_count` - `votes_per_entity`.`downvotes_count`) > 2),1,0)) AS `validated_count`,sum(if(((`votes_per_entity`.`upvotes_count` - `votes_per_entity`.`downvotes_count`) < -(2)),1,0)) AS `invalidated_count`,sum((if(((`votes_per_entity`.`annotation_upvotes_count` - `votes_per_entity`.`annotation_downvotes_count`) > 2),1,0) and (`votes_per_entity`.`e_annotation_id` is not null))) AS `annotation_validated_count`,sum((if(((`votes_per_entity`.`annotation_upvotes_count` - `votes_per_entity`.`annotation_downvotes_count`) < -(2)),1,0) and (`votes_per_entity`.`e_annotation_id` is not null))) AS `annotation_invalidated_count`,sum((if(((`votes_per_entity`.`legal_document_upvotes_count` - `votes_per_entity`.`legal_document_downvotes_count`) > 2),1,0) and (`votes_per_entity`.`e_legal_document_id` is not null))) AS `legal_document_validated_count`,sum((if(((`votes_per_entity`.`legal_document_upvotes_count` - `votes_per_entity`.`legal_document_downvotes_count`) < -(2)),1,0) and (`votes_per_entity`.`e_legal_document_id` is not null))) AS `legal_document_invalidated_count`,sum((if(((`votes_per_entity`.`digital_product_upvotes_count` - `votes_per_entity`.`digital_product_downvotes_count`) > 2),1,0) and (`votes_per_entity`.`e_digital_product_id` is not null))) AS `digital_product_validated_count`,sum((if(((`votes_per_entity`.`digital_product_upvotes_count` - `votes_per_entity`.`digital_product_downvotes_count`) < -(2)),1,0) and (`votes_per_entity`.`e_digital_product_id` is not null))) AS `digital_product_invalidated_count` from (select `caprice`.`e_user`.`id` AS `id`,`vpe`.`created_by_id` AS `created_by_id`,`vpe`.`e_annotation_id` AS `e_annotation_id`,`vpe`.`e_legal_document_id` AS `e_legal_document_id`,`vpe`.`e_digital_product_id` AS `e_digital_product_id`,`vpe`.`upvotes_count` AS `upvotes_count`,`vpe`.`downvotes_count` AS `downvotes_count`,`vpe`.`annotation_upvotes_count` AS `annotation_upvotes_count`,`vpe`.`annotation_downvotes_count` AS `annotation_downvotes_count`,`vpe`.`legal_document_upvotes_count` AS `legal_document_upvotes_count`,`vpe`.`legal_document_downvotes_count` AS `legal_document_downvotes_count`,`vpe`.`digital_product_upvotes_count` AS `digital_product_upvotes_count`,`vpe`.`digital_product_downvotes_count` AS `digital_product_downvotes_count` from (`caprice`.`e_user` left join ((select `caprice`.`e_metadata`.`created_by_id` AS `created_by_id`,`caprice`.`e_annotation`.`id` AS `e_annotation_id`,`caprice`.`e_legal_document`.`id` AS `e_legal_document_id`,`caprice`.`e_digital_product`.`id` AS `e_digital_product_id`,sum(if((`caprice`.`e_vote`.`vote_value` > 0),1,0)) AS `upvotes_count`,sum(if((`caprice`.`e_vote`.`vote_value` < 0),1,0)) AS `downvotes_count`,sum((if((`caprice`.`e_vote`.`vote_value` > 0),1,0) and (`caprice`.`e_annotation`.`id` is not null))) AS `annotation_upvotes_count`,sum((if((`caprice`.`e_vote`.`vote_value` < 0),1,0) and (`caprice`.`e_annotation`.`id` is not null))) AS `annotation_downvotes_count`,sum((if((`caprice`.`e_vote`.`vote_value` > 0),1,0) and (`caprice`.`e_legal_document`.`id` is not null))) AS `legal_document_upvotes_count`,sum((if((`caprice`.`e_vote`.`vote_value` < 0),1,0) and (`caprice`.`e_legal_document`.`id` is not null))) AS `legal_document_downvotes_count`,sum((if((`caprice`.`e_vote`.`vote_value` > 0),1,0) and (`caprice`.`e_digital_product`.`id` is not null))) AS `digital_product_upvotes_count`,sum((if((`caprice`.`e_vote`.`vote_value` < 0),1,0) and (`caprice`.`e_digital_product`.`id` is not null))) AS `digital_product_downvotes_count` from (((((`caprice`.`e_metadata` left join `caprice`.`e_controversial_entity` on((`caprice`.`e_controversial_entity`.`metadata_id` = `caprice`.`e_metadata`.`id`))) left join `caprice`.`e_annotation` on((`caprice`.`e_controversial_entity`.`id` = `caprice`.`e_annotation`.`controversial_entity_id`))) left join `caprice`.`e_legal_document` on((`caprice`.`e_controversial_entity`.`id` = `caprice`.`e_legal_document`.`controversial_entity_id`))) left join `caprice`.`e_digital_product` on((`caprice`.`e_controversial_entity`.`id` = `caprice`.`e_digital_product`.`controversial_entity_id`))) left join `caprice`.`e_vote` on((`caprice`.`e_vote`.`controversial_entity_id` = `caprice`.`e_controversial_entity`.`id`))) group by `caprice`.`e_controversial_entity`.`id`)) `vpe` on((`vpe`.`created_by_id` = `caprice`.`e_user`.`id`)))) `votes_per_entity` group by `votes_per_entity`.`id` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_user_additions`
--

/*!50001 DROP VIEW IF EXISTS `v_user_additions`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `v_user_additions` AS select `e_user`.`id` AS `id`,count(`e_metadata`.`id`) AS `metadata_created`,count(`e_annotation`.`id`) AS `annotations_created`,count(`e_legal_document`.`id`) AS `legal_documents_added`,count(`e_digital_product`.`id`) AS `digital_products_added` from (((((`e_user` left join `e_metadata` on((`e_metadata`.`created_by_id` = `e_user`.`id`))) left join `e_controversial_entity` on((`e_controversial_entity`.`metadata_id` = `e_metadata`.`id`))) left join `e_annotation` on((`e_annotation`.`controversial_entity_id` = `e_controversial_entity`.`id`))) left join `e_legal_document` on((`e_legal_document`.`controversial_entity_id` = `e_controversial_entity`.`id`))) left join `e_digital_product` on((`e_digital_product`.`controversial_entity_id` = `e_controversial_entity`.`id`))) group by `e_user`.`id` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_user_input_votes`
--

/*!50001 DROP VIEW IF EXISTS `v_user_input_votes`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `v_user_input_votes` AS select `e_user`.`id` AS `id`,sum(if((`e_vote`.`vote_value` > 0),1,0)) AS `input_upvotes_count`,sum(if((`e_vote`.`vote_value` < 0),1,0)) AS `input_downvotes_count`,sum((if((`e_vote`.`vote_value` > 0),1,0) and (`e_annotation`.`id` is not null))) AS `annotation_input_upvotes_count`,sum((if((`e_vote`.`vote_value` < 0),1,0) and (`e_annotation`.`id` is not null))) AS `annotation_input_downvotes_count`,sum((if((`e_vote`.`vote_value` > 0),1,0) and (`e_legal_document`.`id` is not null))) AS `legal_document_input_upvotes_count`,sum((if((`e_vote`.`vote_value` < 0),1,0) and (`e_legal_document`.`id` is not null))) AS `legal_document_input_downvotes_count`,sum((if((`e_vote`.`vote_value` > 0),1,0) and (`e_digital_product`.`id` is not null))) AS `digital_product_input_upvotes_count`,sum((if((`e_vote`.`vote_value` < 0),1,0) and (`e_digital_product`.`id` is not null))) AS `digital_product_input_downvotes_count` from ((((((`e_user` left join `e_metadata` on((`e_metadata`.`created_by_id` = `e_user`.`id`))) left join `e_controversial_entity` on((`e_controversial_entity`.`metadata_id` = `e_metadata`.`id`))) left join `e_annotation` on((`e_controversial_entity`.`id` = `e_annotation`.`controversial_entity_id`))) left join `e_legal_document` on((`e_controversial_entity`.`id` = `e_legal_document`.`controversial_entity_id`))) left join `e_digital_product` on((`e_controversial_entity`.`id` = `e_digital_product`.`controversial_entity_id`))) left join `e_vote` on((`e_vote`.`controversial_entity_id` = `e_controversial_entity`.`id`))) group by `e_user`.`id` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_controversial_entity_votes`
--

/*!50001 DROP VIEW IF EXISTS `v_controversial_entity_votes`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `v_controversial_entity_votes` AS select `e_controversial_entity`.`id` AS `id`,sum(if((`e_vote`.`vote_value` > 0),1,0)) AS `votes_up`,sum(if((`e_vote`.`vote_value` < 0),1,0)) AS `votes_down` from (`e_controversial_entity` left join `e_vote` on((`e_vote`.`controversial_entity_id` = `e_controversial_entity`.`id`))) group by `e_controversial_entity`.`id` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_user_stats`
--

/*!50001 DROP VIEW IF EXISTS `v_user_stats`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `v_user_stats` AS select `e_user`.`id` AS `id`,`v_user_output_votes`.`output_upvotes_count` AS `output_upvotes_count`,`v_user_output_votes`.`output_downvotes_count` AS `output_downvotes_count`,`v_user_output_votes`.`annotation_output_upvotes_count` AS `annotation_output_upvotes_count`,`v_user_output_votes`.`annotation_output_downvotes_count` AS `annotation_output_downvotes_count`,`v_user_output_votes`.`legal_document_output_upvotes_count` AS `legal_document_output_upvotes_count`,`v_user_output_votes`.`legal_document_output_downvotes_count` AS `legal_document_output_downvotes_count`,`v_user_output_votes`.`digital_product_output_upvotes_count` AS `digital_product_output_upvotes_count`,`v_user_output_votes`.`digital_product_output_downvotes_count` AS `digital_product_output_downvotes_count`,`v_user_input_votes`.`input_upvotes_count` AS `input_upvotes_count`,`v_user_input_votes`.`input_downvotes_count` AS `input_downvotes_count`,`v_user_input_votes`.`annotation_input_upvotes_count` AS `annotation_input_upvotes_count`,`v_user_input_votes`.`annotation_input_downvotes_count` AS `annotation_input_downvotes_count`,`v_user_input_votes`.`legal_document_input_upvotes_count` AS `legal_document_input_upvotes_count`,`v_user_input_votes`.`legal_document_input_downvotes_count` AS `legal_document_input_downvotes_count`,`v_user_input_votes`.`digital_product_input_upvotes_count` AS `digital_product_input_upvotes_count`,`v_user_input_votes`.`digital_product_input_downvotes_count` AS `digital_product_input_downvotes_count`,`v_user_entities_clarified`.`validated_count` AS `validated_count`,`v_user_entities_clarified`.`invalidated_count` AS `invalidated_count`,`v_user_entities_clarified`.`annotation_validated_count` AS `annotation_validated_count`,`v_user_entities_clarified`.`annotation_invalidated_count` AS `annotation_invalidated_count`,`v_user_entities_clarified`.`legal_document_validated_count` AS `legal_document_validated_count`,`v_user_entities_clarified`.`legal_document_invalidated_count` AS `legal_document_invalidated_count`,`v_user_entities_clarified`.`digital_product_validated_count` AS `digital_product_validated_count`,`v_user_entities_clarified`.`digital_product_invalidated_count` AS `digital_product_invalidated_count`,`v_user_additions`.`metadata_created` AS `metadata_created`,`v_user_additions`.`annotations_created` AS `annotations_created`,`v_user_additions`.`legal_documents_added` AS `legal_documents_added`,`v_user_additions`.`digital_products_added` AS `digital_products_added` from ((((`e_user` left join `v_user_additions` on((`e_user`.`id` = `v_user_additions`.`id`))) left join `v_user_entities_clarified` on((`e_user`.`id` = `v_user_entities_clarified`.`id`))) left join `v_user_input_votes` on((`e_user`.`id` = `v_user_input_votes`.`id`))) left join `v_user_output_votes` on((`e_user`.`id` = `v_user_output_votes`.`id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Dumping routines for database 'caprice'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed
