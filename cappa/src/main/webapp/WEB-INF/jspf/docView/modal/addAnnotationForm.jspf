<%-- any content can be specified here e.g.: --%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="/WEB-INF/functions/functions.tld" prefix="f" %>
<%@ page pageEncoding="UTF-8" %>


<script>
    
    function addNewAnnotationFormSubmit(){
        var textParts = []; 
        var highlights = JSON.parse( documentViewer.getHighlightsManager().getHighlightsSerialized() );
        for( i=0; i<highlights.length; i++ )
            textParts.push( new TextPart( {highlightsData: JSON.stringify(highlights[i])} ).getData() );
        
        var form = $("#annotation-modal form");
        var annotationForm = {
            textParts: textParts,
            privacyConcernId: form.find("input[data-input=privacyConcern]").val(),
            tagIds: form.find("input[data-input=tags]").val().split(',').map( i => parseInt(i) ), 
            comment:  form.find("textarea[data-input=comment]").val()
        };
        
        AJAX({
            url: '/api/data/product/${document.digitalProduct.id}/document/${document.id}/annotation/new',
            method: 'POST',
            contentType: 'application/json',
            data: JSON.stringify(annotationForm),
            onSuccess: function(response, textStatus, jqXHR){
                if( response.status === "success" ){
                    // Remove highlights
                    documentViewer.getHighlightsManager().removeHighlights();
                    documentViewer.annotationCreated(response.data);
//                    $('div[data-area=annotations] ul').append( response.html );
//                    setInterval( () => location.reload(), 2000);
                }
                
            }
        });
    }
    
</script>

<script>
    
    var Tags = ${f:toJson(tags)};
    window.addEventListener('DOMContentLoaded', function(event){
        
        var getUniquesBy = function(array, fn){
            var seen = {};
            return array.filter( function(item) {
                var k = fn(item);
                return seen.hasOwnProperty(k) ? false : (seen[k] = true);
            });
        };
        
        var privacyConcerns = getUniquesBy( Tags.map(t => t.privacyConcern), t => t.id );
        var selectizers = [];
        
        // Privacy Concerns selectizer
        var privacyConcernSelectizer = $("#annotation-modal input[data-input=privacyConcern]").selectize({
                persist: false,
                maxItems: 1,
                placeholder: 'Privacy Concern  ',
                valueField: 'id',
                labelField: 'name',
                searchField: ['name'],
                options: privacyConcerns,
                render: { 
                    item: function(item, escape) {
                        return "<div class='d-inline-block p-1'>" + 
                            '<span class="py-1 px-3 rounded text-light" style="background-color: '+item.uiColor+'"\n\
                                data-toggle="tooltip" data-placement="top" title="'+escape(item.description)+'">' +
                                (item.name ? '<span class="tag font-weight-bold">' + escape(item.name) + '</span>' : '') +
                            '</span>' + 
                        '</div>';
                    }
                },
                onChange: () => fixTagsUi(),
                openOnFocus: true
            })[0].selectize;
    
        
        var fixTagsUi = function(){
            selectizers = [];
            var concernAttributes = getUniquesBy( Tags.map( t => t.privacyConcernAttribute ), attr => attr.id )
                    .filter( attr => attr.privacyConcern.id+'' === $('input[data-input=privacyConcern]').val() );
            var mandatoryAttrs = concernAttributes.filter( a => a.isMandatory===true ).sort( (a,b) => a.uiIndex - b.uiIndex );
            var optionalAttrs = concernAttributes.filter( a => a.isMandatory===false ).sort( (a,b) => a.uiIndex - b.uiIndex );
            var concernTags = Tags.filter( t => t.privacyConcern.id+'' === $('input[data-input=privacyConcern]').val() );

            // Build Tags Division
            var fixTagsDiv = function(division, concernAttributes){
                $("#annotation-modal div[data-input=tags]").val('');
                $("#annotation-modal div[data-input-div="+division+"-tags]").html('');
                $(concernAttributes).each( (index, elem) => {
                    var html = 
                           '<div class="d-flex py-1">'
                            + '<h5 class="d-inline-block" style="line-height:1.75">' + elem.uiPrefixText + '</h5>' +'&nbsp;&nbsp;' 
                            + '<input type="text" class="" data-input="'+division+'-tags'+elem.id+'">' + '&nbsp;&nbsp;' 
                            + '</div>';
                    $("#annotation-modal div[data-input-div="+division+"-tags]").append(html);
                    // Gather tag options
                    var inputTags = concernTags.filter( t => t.privacyConcernAttribute.id === elem.id )
                            .map( t => $.extend(t, {privacyConcernColor: elem.privacyConcern.uiColor}) );
                    selectizers.push( $("#annotation-modal input[data-input="+division+"-tags"+elem.id+"]").selectize({
                                persist: false,
                                maxItems: elem.isMultivalue? null:1,
                                placeholder: elem.tagAttribute.name + '       ',
                                valueField: 'id',
                                labelField: 'name',
                                searchField: ['name'],
                                options: inputTags,
                                render: { 
                                    item: function(item, escape) {
                                        return "<div class='d-inline-block p-1'>" + 
                                            '<span class="py-1 px-3 rounded text-light" style="background-color: '+item.privacyConcernColor+'"\n\
                                                data-toggle="tooltip" data-placement="top" title="'+escape(item.description)+'">' +
                                                (item.name ? '<span class="tag font-weight-bold">' + escape(item.name) + '</span>' : '') +
                                            '</span>' + 
                                        '</div>';
                                    }
                                },
                                onChange: () => collectTags(),
                                openOnFocus: true
                            })[0].selectize
                    );
                });
            };
            fixTagsDiv('mandatory', mandatoryAttrs);
            fixTagsDiv('optional', optionalAttrs);
        };
        fixTagsUi();
        
        var collectTags = function(){
            var vals = selectizers.map( s => s.items ).reduce( (acc, item) => acc.concat(item), [] );
            $("#annotation-modal form input[data-input=tags]").val(vals);
        };
    });
    
</script>


<div class="modal fade" id="annotation-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="form" style="max-width: 85%;">
        <div class="modal-content">
            
            <div class="modal-header">
                <h3 class="modal-title">Create Annotation</h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            
            <div class="modal-body">
                <div class="font-weight-bold">Highlighted Text</div>
                <div class="bg-light border border-secondary rounded">
                    <!--<h3 class="bg-secondary px-1 py-0 p-2 rounded text-light">Highlighted Text</h3>-->
                    <ul class="" style="list-style-type: initial;"></ul>
                </div>
                <!--<hr/>-->
                <br>
                <form>
                    <div class="form-group">
                        <label for="tags" class="font-weight-bold">Tags</label>
                        <input type="hidden" data-input="tags" />
                        <div class="input-group mb-3">
                            <div class="d-inline-block py-0 px-3">
                                <h5><label class="input-group-text" style="line-height: 1.75" for="privacyConcern">Privacy Concern: </label></h5>
                            </div>
                            <input type="text" class='w-25' data-input='privacyConcern' />
<!--                            <select class="" id="privacyConcern" data-input="privacyConcern">
                                <option selected>Choose...</option>
                                <c:forEach var="privacyConcern" items="${privacyConcerns}" >
                                    <option value="${privacyConcern.id}">${privacyConcern.name}</option>
                                </c:forEach>
                            </select>-->
                        </div>
                        
                        <div>
                            <div class="py-0 px-3">
                                <label class="input-group-text font-weight-bold" for="mandatory-tags">Mandatory </label>
                            </div>
                            <div class="input-group mb-1 px-3" data-input-div='mandatory-tags'>
<!--                                This product <input type="text" class="" aria-label="Text input with dropdown button" data-input="mandatory-tags1">
                                asks <input type="text" class="" aria-label="Text input with dropdown button" data-input="mandatory-tags2">
                                for <input type="text" class="" aria-label="Text input with dropdown button" data-input="mandatory-tags3">-->
                            </div>
                            <br>
                        </div>
                        <div>
                            <div class="py-0 px-3">
                                <label class="input-group-text font-weight-bold" for="optional-tags">Optional </label>
                            </div>
                            <div class="mb-3 px-3 d-inline-block" data-input-div='optional-tags'>
                                <h5 class="d-inline-block" style="line-height:1.75">elem.uiPrefixText </h5>&nbsp;&nbsp;
                                <input type="text" class="" data-input="'+division+'-tags'+elem.id+'"> &nbsp;&nbsp;
                                <h5 class="d-inline-block" style="line-height:1.75">elem.uiPrefixText </h5>&nbsp;&nbsp;
                                <input type="text" class="" data-input="'+division+'-tags'+elem.id+'"> &nbsp;&nbsp;
                            </div>
                            <br>
                        </div>
                        
                    </div>
                    <div class="form-group">
                        <label for="comment" class="col-form-label font-weight-bold">Comment</label>
                        <textarea class="form-control" id="comment" rows="3" maxlength="100" style="resize: none;" data-input="comment"></textarea>
                    </div>
                </form>
            </div>
            
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal" data-button="create-annotation-close">Cancel</button>
                <button type="button" class="btn btn-primary" data-dismiss="modal" data-button="create-annotation" onclick="addNewAnnotationFormSubmit()" >Create</button>
            </div>
            
        </div>
    </div>
</div>