<%-- 
    Document   : submitVote
    Created on : Feb 6, 2018, 1:44:34 PM
    Author     : Giorgos Hompis <ghompis@csd.uoc.gr>
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<script defer="defer">
    function submitVote(controversialEntity_id, voteValue, annotationId){
        
        var voteForm = {
            controversialEntityId: controversialEntity_id,
            value: voteValue
        };
        
        AJAX({
            url: '/api/data/controversialEntity/vote/new',
            method: 'POST',
            contentType: 'application/json',
            data: JSON.stringify(voteForm),
            onSuccess: function(response, textStatus, jqXHR){
                if( response.status === "success" ){
                    if( annotationId ){
                        documentViewer.annotationVoted(annotationId);
                    } else {
                        setInterval( () => location.reload(), 2000);
//                    $('div[data-area=annotations] ul').append( response.html );
                    }
                }
                
            }
        });
    }
</script>
