<%-- 
    Document   : privacyPrinciple
    Created on : Oct 17, 2017, 4:57:27 PM
    Author     : Giorgos Hompis <ghompis@csd.uoc.gr>
--%>

<%@tag description="put the tag description here" pageEncoding="UTF-8"%>

<%-- The list of normal or fragment attributes can be specified here: --%>
<%@attribute name="principle" required="true" %>
<%@attribute name="value" required="true" %>

<%-- any content can be specified here e.g.: --%>

<%
    String cssClass;
    value = value.toLowerCase();
    if( value.equals("a") )
        cssClass = "success";
    else if( value.equals("b") )
        cssClass = "warning";
    else if( value.equals("c") )
        cssClass = "danger";
    else if( value.equals("?") )
        cssClass = "secondary";
    else
        cssClass = "secondary";
%>


<button type="button" class="col btn btn-outline-<%= cssClass %> m-1 p-0">
    <div class="privacy-principle text-right p-0">
        <div class="col-md-10 d-inline"><%= principle %></div>
        <div class="col-md-2 d-inline-block w-25 text-center bg-<%=cssClass%> border rounded text-light py-0 px-2"><%= value.toUpperCase() %></div>
    </div>
</button>


