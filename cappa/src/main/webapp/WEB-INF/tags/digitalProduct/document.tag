<%-- 
    Document   : document
    Created on : Oct 19, 2017, 12:53:49 PM
    Author     : Giorgos Hompis <ghompis@csd.uoc.gr>
--%>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<%@tag description="Document tag" pageEncoding="UTF-8"%>

<%-- The list of normal or fragment attributes can be specified here: --%>
<%@attribute name="document" type="org.caprice.model.db.LegalDocument" required="true"%>

<%-- any content can be specified here e.g.: --%>


<div class="d-inline-block border-secondary w-100">

    <div class="row no-gutters">

        <div class="col-3 p-2 rating text-center d-inline-block bg-light border-right border-dark">
            <div class='w-25 d-inline-block text-left align-top p-2'>
                <div class='d-inline text-secondary' style='cursor: pointer' onclick="setDeleteEntityModal('Are you sure you want to delete this document?', ${document.controversialEntity.id})" data-toggle="modal" data-target="#delete-entity-modal">
                    <i class="fa fa-trash fa-lg"></i>
                </div>
            </div>
            <div class="align-text-bottom d-inline-block p-1 px-3 mx-3 badge rounded 
                 ${fn:toLowerCase(document.controversialEntity.state.name) == "invalid"? "badge-danger" : 
                   fn:toLowerCase(document.controversialEntity.state.name) == "valid"? "badge-success" : "badge-secondary"}" 
                 style="font-size:2rem; line-height: inherit;" data-toggle="tooltip" data-placement="top" 
                 title="Up: ${document.controversialEntity.votes.votesUp} 
                 <br> Down: ${document.controversialEntity.votes.votesDown}
                 <br> Min Confidence: <fmt:formatNumber type="number" maxFractionDigits="2" value="${document.controversialEntity.minConfidenceLimit}" />
                 <br> Max Confidence: <fmt:formatNumber type="number" maxFractionDigits="2" value="${document.controversialEntity.maxConfidenceLimit}" />" >
                ${document.controversialEntity.votes.votesUp - document.controversialEntity.votes.votesDown}
                <%--<fmt:formatNumber type="number" maxFractionDigits="2" value="${document.controversialEntity.score}" />--%>
            </div>
            <div class="d-inline-block">
                <div class="p-1">
                    <button type="button" onclick="submitVote(${document.controversialEntity.id}, 1)" class="btn btn-outline-success ${votesByEntity[document.controversialEntity].voteValue>0? 'selected':''}" style="padding:.2rem .6rem;">
                        <i class="fa fa-sort-up" aria-hidden="true"></i>
                    </button>
                </div>
                <div class="p-1">
                    <button type="button" onclick="submitVote(${document.controversialEntity.id}, -1)" class="btn btn-outline-danger ${votesByEntity[document.controversialEntity].voteValue<0? 'selected':''}" style="padding:.2rem .6rem;">
                        <i class="fa fa-sort-down" aria-hidden="true"></i>
                    </button>
                </div>
            </div>
        </div>

        <a class="col-9 btn btn-light text-left d-inline p-2 rounded" href="${pageContext.request.contextPath}/product/${product.id}/document/${document.id}">
            <div class="col-1 text-center align-text-bottom d-inline-block">
                <h1 class="p-2">
                    <i class="fa fa-file-text text-primary " aria-hidden="true"></i>
                </h1>
            </div>

            <div class="col-8 d-inline-block px-1">
                <h2 class="my-0">${document.name}</h2> 
                <div class="py-2 px-1 text-secondary">
                    From <span class="text-primary">${document.url}</span>
                </div>
            </div>

            <div class="col-3 d-inline-block align-top text-right">
                <div class="align-top my-2">
                    <span class="d-inline px-2 " data-toggle="tooltip" data-placement="top" title="Total Annotations: ${ fn:length(document.annotations) }"> 
                        ${ fn:length(document.annotations) } <i class="fa fa-pencil px-1"></i> 
                    </span>
                    <span class="mx-2 flag-icon flag-icon-${fn:toLowerCase(document.language.flagIsoCode)}" 
                          data-toggle="tooltip" data-placement="top" title="Document Language: ${document.language.name}"></span>
                </div>
                
                <div class="text-secondary mt-3">
                    <span class="float-right mx-2">
                        Downloaded: <fmt:formatDate value="${document.collectedAt}" pattern="dd-MM-yyyy" />
                    </span>
                </div>
            </div>
        </a>

    </div>

</div>
