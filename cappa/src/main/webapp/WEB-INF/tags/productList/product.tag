<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@tag description="put the tag description here" pageEncoding="UTF-8"%>

<%-- The list of normal or fragment attributes can be specified here: --%>

<%@attribute name="product" type="org.caprice.model.db.DigitalProduct" required="true" %>
<%--<%@attribute name="items" type="java.util.List" required="true" %>--%>



<div class="row rounded w-100 no-gutters">
    
    <div class="col-3 p-3 rating text-center d-inline-block bg-light border-right border-dark">
        <div class='w-25 d-inline-block text-left align-top p-2'>
            <div class='d-inline text-secondary' style='cursor: pointer' onclick="setDeleteEntityModal('Are you sure you want to delete this product?', ${product.controversialEntity.id})" data-toggle="modal" data-target="#delete-entity-modal">
                <i class="fa fa-trash fa-lg"></i>
            </div>
        </div>
        <div class="align-text-bottom d-inline-block p-1 px-3 mx-3 badge rounded 
                 ${fn:toLowerCase(product.controversialEntity.state.name) == "invalid"? "badge-danger" : 
                   fn:toLowerCase(product.controversialEntity.state.name) == "valid"? "badge-success" : "badge-secondary"}" 
             style="font-size:2rem; line-height: inherit;" data-toggle="tooltip" data-placement="top" 
             title="Up: ${product.controversialEntity.votes.votesUp} 
             <br> Down: ${product.controversialEntity.votes.votesDown}
             <br> Min Confidence: <fmt:formatNumber type="number" maxFractionDigits="2" value="${product.controversialEntity.minConfidenceLimit}" />
             <br> Max Confidence: <fmt:formatNumber type="number" maxFractionDigits="2" value="${product.controversialEntity.maxConfidenceLimit}" />" >
            ${product.controversialEntity.votes.votesUp - product.controversialEntity.votes.votesDown}
                <%--<fmt:formatNumber type="number" maxFractionDigits="2" value="${product.controversialEntity.score}" />--%>
        </div>
        <div class="d-inline-block">
            <div class="p-1">
                <button type="button" onclick="submitVote(${product.controversialEntity.id}, 1)" class="btn btn-outline-success ${votesByEntity[product.controversialEntity].voteValue>0? 'selected':''}" style="padding:.2rem .6rem;">
                    <i class="fa fa-sort-up" aria-hidden="true"></i>
                </button>
            </div>
            <div class="p-1">
                <button type="button" onclick="submitVote(${product.controversialEntity.id}, -1)" class="btn btn-outline-danger ${votesByEntity[product.controversialEntity].voteValue<0? 'selected':''}" style="padding:.2rem .6rem;">
                    <i class="fa fa-sort-down" aria-hidden="true"></i>
                </button>
            </div>
        </div>
    </div>

    <a class="col-9 btn btn-light w-100 text-left d-inline p-2 rounded-0" href="${pageContext.request.contextPath}/product/${product.id}">
        
        <div class="col-md-1 no-gutters d-inline-block text-center px-2 py-1 align-top">
            <img class="img-responsive img-rounded p-1" style='width:5em' src="data:image/bmp;base64,${product.logoImageData}">
        </div>

        <div class="col-md-8 no-gutters d-inline-block px-0">
            <div class="container-fluid p-1">
                <div class="row">
                    <div class="col align-middle">
                        <h2 class="d-inline-block m-0">${product.name}</h2>
                        <div class='text-secondary text-small p-1 d-block'>
                            ${product.productType.name} @ <span class='text-info'>${product.url}</span>
                        </div>
                    </div>
                </div>

                <div class="row pt-1">
                    <div class="col"> 
                        <c:forEach var="category" items="${product.businessCategories}">
                            <span class="badge badge-primary">${category.name}</span>
                        </c:forEach>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-3 no-gutters d-inline-block p-1 align-top">
<!--            <div class="d-inline px-2 w-25"> 
                123
                <i class="fa fa-group px-1"></i>
            </div>-->
            <div class="d-inline px-2 w-25" data-toggle="tooltip" data-placement="top" title="Total Documents: ${fn:length(product.legalDocuments)}"> 
                ${fn:length(product.legalDocuments)}
                <i class="fa fa-file-text px-1"></i>
            </div>
            <div class="d-inline px-2 w-25" data-toggle="tooltip" data-placement="top" title="Total Annotations: ${product.legalDocuments.stream().map( d -> d.annotations ).flatMap( a -> a.stream() ).count()}"> 
                ${product.legalDocuments.stream().map( d -> d.annotations ).flatMap( a -> a.stream() ).count()}
                <i class="fa fa-pencil px-1"></i>
            </div>
        </div>
        
    </a>
                    
</div>

<!--<hr class="row no-gutters seperator bg-secondary"/>-->