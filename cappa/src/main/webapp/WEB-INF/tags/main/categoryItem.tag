
<%@tag description="put the tag description here" pageEncoding="UTF-8"%>

<%-- The list of normal or fragment attributes can be specified here: --%>
<%@attribute name="imageSource" required="true" %>
<%@attribute name="title" required="true" %>
<%@attribute name="description" required="true" %>
<%@attribute name="url" required="true" %>
<%--<%@attribute name="items" type="java.util.List" required="true" %>--%>
        
<div class="container-fluid">
    
    <div class="row">
        
        <div class="col-8">
            <a href="${url}" class="d-block"/>
            
                <div class="row">
                    <div class="col-4 text-center">
                        <img class="border border-dark" src="${imageSource}" width="75%" height="65%" />
                        <div class="bg-primary text-light px-2 py-1 w-75 text-center m-auto" >
                            ${title}
                        </div>
                    </div>
                    <div class="col-8">
                        <p class="text-dark py-2">
                            ${description}
                        </p>
                    </div>
                </div>
                
            </a>
        </div>
                    
        <div class="col-4"></div>
        
    </div>
</div>