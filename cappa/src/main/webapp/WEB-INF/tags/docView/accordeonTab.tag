<%-- 
    Document   : accordeonTab
    Created on : Oct 20, 2017, 4:35:05 PM
    Author     : Giorgos Hompis <ghompis@csd.uoc.gr>
--%>

<%@tag description="put the tag description here" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<%-- The list of normal or fragment attributes can be specified here: --%>
<%@attribute name="accordeonId" required="true"%>
<%@attribute name="tagsDataEntry" type="java.util.AbstractMap.Entry" required="true"%>


<%-- any content can be specified here e.g.: --%>
<c:set var="privacyConcern" value="${tagsDataEntry.key}" />
<c:set var="tagsByAttribute" value="${tagsDataEntry.value}" />

<c:set var="accordeonConcernHeaderId" value="${accordeonId}_head_concern_${privacyConcern.id}" />
<c:set var="accordeonConcernBodyId" value="${accordeonId}_body_concern_${privacyConcern.id}" />

<c:set var="accordeonConcern_AccordeonId" value="${accordeonId}_concern_${privacyConcern.id}" />

<%-- ===================== PRIVACY CONCERN ===================== --%>
<div class="card" data-concern-id="${privacyConcern.id}">
    
    <%-- Accordeon Header --%>
    <div class="card-header buttons button-primary" role="tab" id="${accordeonConcernHeaderId}">
        <h6 class="mb-0">
            <a data-toggle="collapse" href="#${accordeonConcernBodyId}" aria-expanded="true" aria-controls="${accordeonConcernBodyId}">
                <div>
                    <div class="float-left" style="width: 1.5rem;">
                        <span class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="${privacyConcern.description}"></span>
                    </div>
                    <div class="float-left w-75"> 
                        <span>${privacyConcern.name}</span>
                    </div>
                    <div class="float-right"> 
                        <span class="badge" style="width: 2rem; color: #fff; background-color: ${privacyConcern.uiColor}" data-area="concern-total"
                            data-toggle="tooltip" data-placement="top" title="Total Annotations for ${privacyConcern.name}">
                            ${fn:length(annotationsData[privacyConcern])}
                        </span> 
                    </div>
                </div>
            </a>
        </h6>
    </div>
            
    <%-- Accordeon Body --%>
    <div id="${accordeonConcernBodyId}" class="collapse" role="tabpanel" aria-labelledby="${accordeonConcernHeaderId}" data-parent="#${accordeonId}">
        <div class="card-body">
            <div class="btn-group-vertical w-100" role="group" aria-label="Button group with nested dropdown">
                
                <div id="${accordeonConcern_AccordeonId}" class="w-100" data-children=".item">
                <c:forEach var="tagsByAttributeEntry" items="${tagsByAttribute}" >
                    <c:set var="tagAttribute" value="${tagsByAttributeEntry.key}" />
                    <c:set var="tags" value="${tagsByAttributeEntry.value}" />
                    <c:set var="accordeonTagAttributeHeaderId" value="${accordeonConcern_AccordeonId}_head_tagAttr_${tagAttribute.id}" />
                    <c:set var="accordeonTagAttributeBodyId" value="${accordeonConcern_AccordeonId}_body_tagAttr_${tagAttribute.id}" />
                    
                    <%-- ===================== TAG ATTRIBUTE ===================== --%>
                    <div class="item p-1" data-tagAttribute-id="${tagAttribute.id}" style="display:none;">
                        <!-- HEAD -->
                        <a class="d-block bg-light text-dark w-100 p-2 " data-toggle="collapse" data-parent="#${accordeonConcern_AccordeonId}" 
                           href="#${accordeonTagAttributeBodyId}" role="button" aria-expanded="false" aria-controls="${accordeonTagAttributeBodyId}">
                            <div class="float-left"> 
                                <span class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="${tagAttribute.description}"></span>
                                <span class="font-weight-bold">${tagAttribute.name}</span> 
                            </div>
                            <!--                            
                            <div class="float-right" > 
                                <span class="badge badge-dark">${ fn:length(tags) }</span> 
                            </div>
                            -->
                            <div class="clearfix"></div>
                        </a>
                        <!-- BODY -->
                        <div id="${accordeonTagAttributeBodyId}" class="collapse p-2 " role="tabpanel">
                            
                            <c:forEach var="tag" items="${tags}" >
                                <%-- ===================== TAG ===================== --%>
                                <button type="button" class="bg-caprice text-light text-left m-1 w-100 " data-tag-id="${tag.key.id}" style="display:none;"
                                        data-toggle="tooltip" data-placement="top" title="${tag.key.description}" onclick="documentViewer.tagClicked(${tag.key.id})">
                                    
                                    <div class="float-left">
                                        <span class="fa fa-question-circle"></span>&nbsp;
                                        &nbsp;${tag.key.name} 
                                    </div>
                                    <div class="float-right" > 
                                        <span class="badge badge-light" data-area="tag-total">${ tag.value }</span>
                                    </div>
                                </button>
                            </c:forEach>
                            
                        </div>
                    </div>
                        
                </c:forEach>
                </div>
                
            </div>
        </div>
    </div>
        
</div>