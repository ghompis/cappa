<%-- 
    Document   : userDetails
    Created on : Feb 8, 2018, 5:30:31 PM
    Author     : Giorgos Hompis <ghompis@csd.uoc.gr>
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<%-- USER DETAILS --%>
<div class="container-fluid">
    <div class="row no-gutters">
        <div class="col-3 no-gutters px-2 card bg-light">
            <div class="text-center text-secondary p-2">
                <i class="fa fa-user" style="font-size: 8rem" aria-hidden="true"></i>
            </div>
            <div class="card-body text-center p-2">
                <h5 class="card-title font-weight-bold">${user.username}</h5>
                <p class="card-text"><span class="text-secondary">Total Score : </span>${user.score}</p>
            </div>
        </div>

        <div class="col-9 no-gutters px-2">
            <ul class="list-group">
                <li class="list-group-item py-2">
                    <span class="text-secondary">User Roles : </span> &nbsp;
                    <c:forEach var="role" items="${user.userRoles}">
                         <span class="badge badge-secondary">${role.name}</span>
                    </c:forEach>
                </li>
                <li class="list-group-item py-2">
                    <span class="text-secondary">Email : </span> <a href="mailto:${user.email}">${user.email}</a>
                </li>
                <li class="list-group-item py-2">
                    <span class="text-secondary">Age : </span> 
                    <span class="text-dark">${user.age}</span>
                </li>
                <li class="list-group-item py-2">
                    <span class="text-secondary">Profession : </span> ${user.profession}
                </li>
                <li class="list-group-item py-2">
                    <span class="text-secondary">Education Level : </span> ${user.educationLevel}
                </li>
                <li class="list-group-item py-2">
                    <span class="text-secondary pb-1">Details</span><br>&nbsp;
                    ${user.details}
                </li>
            </ul>
        </div>
    </div>
</div>


<%-- USER REWARDS --%>
<h3 class="mt-3 px-3">Rewards</h3>
<hr class="seperator">
<div class="bg-light p-3 mt-2">

    <div>
        <h6 class="text-secondary pb-2">Goal Badges</h6>
<!--                        <div class="clearfix">
            <p class="float-right">Total Valid Annotations: ${user.userStats.annotationValidatedCount}</p>
        </div>-->
        <c:forEach var="r" items="${user.rewards.stream().filter( bc -> bc.badge.category eq 'Goal').toList()}" varStatus="status" >
            <div class="d-inline" style="opacity: ${1/3.0 + 2*r.count/3.0}; width: ${status.index < 2? 33:33}%">
                <span class="badge" style="background-color: ${r.badge.uiColor}">${r.badge.name}</span>
            </div>
        </c:forEach>
    </div>
    <hr class="seperator">

    <div>
        <h6 class="text-secondary pb-2">Simple Badges</h6>
        <c:forEach var="r" items="${user.rewards.stream().filter( bc -> bc.badge.category eq 'Simple').toList()}" varStatus="status" >
            <div class="d-inline-block ${status.index < 2? "mb-3":""}" style="width: ${status.index < 2? 33:33}%">
                <span class="badge" style="background-color: ${r.badge.uiColor}">${r.badge.name}</span> x${r.count} 
            </div>
            ${status.index == 1? "<br>":""}
        </c:forEach>
    </div>
    <hr class="seperator">

    <div>
        <h6 class="text-secondary pb-2">Special Badges</h6>
        <c:forEach var="r" items="${user.rewards.stream().filter( bc -> bc.badge.category eq 'Special').toList()}">
            <div class="d-inline-block" style="width: 33%">
                <div><span class="badge" style="background-color: ${r.badge.uiColor}">${r.badge.name}</span> x${r.count}</div>
            </div>
        </c:forEach>
    </div>
</div>
