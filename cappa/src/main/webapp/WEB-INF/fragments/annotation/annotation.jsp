<%-- any content can be specified here e.g.: --%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<%@ page pageEncoding="UTF-8" %>


<li class="py-2" style="list-style: none; ${(fn:toLowerCase(annotation.controversialEntity.state.name) == "invalid")? "opacity:.5":""}" data-annotation="${annotation.id}">
    <div class="container-fluid no-gutters">
        <div class="row no-gutters">
            
            <%-- Score + Votes UI --%>
            <div class="col-2 no-gutters rating text-center px-md-1">

                <div class="row" style="position: absolute">
                    <!--<div class="px-1">-->
                        <!--
                        <c:if test='${fn:toLowerCase(annotation.controversialEntity.state.name) == "unclarified"}' >
                            <i class="fa fa-question text-secondary px-1" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="This annotation has not been clarified yet"></i>
                        </c:if>
                        -->
                        <c:if test='${fn:toLowerCase(annotation.controversialEntity.state.name) == "valid"}' >
                            <i class="fa fa-check fa-lg text-success px-1" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="This annotation is valid"></i>
                        </c:if>
                        <c:if test='${fn:toLowerCase(annotation.controversialEntity.state.name) == "invalid"}' >
                            <i class="fa fa-times fa-lg text-danger px-1" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="This annotation is invalid"></i>
                        </c:if>
                        <i class="fa fa-eye-slash fa-lg px-1" aria-hidden="true" data-icon="annotation-not-visible" style="display: none;"
                           data-toggle="tooltip" data-placement="top" title="Highlights Not Visible due to highlight conflicts"></i>
                    <!--</div>-->
                </div>

                <div class="row">

                    <div class="col-7 p-0 m-auto ">
                        <div class="rounded w-50 float-right font-weight-bold ${fn:toLowerCase(annotation.controversialEntity.state.name) == "invalid"? "badge-danger" : 
                                            fn:toLowerCase(annotation.controversialEntity.state.name) == "valid"? "badge-success" : "badge-light" }" 
                                style="font-size:1.5rem; line-height: 2;" data-toggle="tooltip" data-placement="top" 
                                title="Up: ${annotation.controversialEntity.votes.votesUp} 
                                <br> Down: ${annotation.controversialEntity.votes.votesDown}
                                <br> Min Confidence: <fmt:formatNumber type="number" maxFractionDigits="2" value="${annotation.controversialEntity.minConfidenceLimit}" />
                                <br> Max Confidence: <fmt:formatNumber type="number" maxFractionDigits="2" value="${annotation.controversialEntity.maxConfidenceLimit}" />" >
                                ${annotation.controversialEntity.votes.votesUp - annotation.controversialEntity.votes.votesDown}
                        </div>
                    </div>

                    <div class="col-5 p-1 px-3 badge rounded ">
                        <div class="p-1">
                            <button type="button" onclick="submitVote(${annotation.controversialEntity.id}, 1, ${annotation.id})" class="btn btn-outline-success ${votesByEntity[annotation.controversialEntity].voteValue>0? 'selected':''}" 
                                    style="padding:.2rem .6rem;" data-toggle="tooltip" data-placement="top" title="This annotation seems useful and provides valid information">
                                <i class="fa fa-sort-up" aria-hidden="true"></i>
                            </button>
                        </div>
                        <div class="p-1">
                            <button type="button" onclick="submitVote(${annotation.controversialEntity.id}, -1, ${annotation.id})" class="btn btn-outline-danger ${votesByEntity[annotation.controversialEntity].voteValue<0? 'selected':''}"
                                    style="padding:.2rem .6rem;" data-toggle="tooltip" data-placement="top" title="This annotation seems useless or provides invalid information">
                                <i class="fa fa-sort-down" aria-hidden="true"></i>
                            </button>
                        </div>
                    </div>

                </div>
                
                
            </div>
            
            
            
            <div class="col-10 context px-3 no-gutters">
                
                <div class="row mb-2 no-gutters">
                    <!-- Tags -->
                    <div class="col-10">
                        <div class='clearfix'>
                            <div class="py-1">
                                <c:forEach var="tag" items="${annotation.tags}">
                                    <span class="badge" data-toggle="tooltip" data-placement="top" title="${tag.description}"
                                          style="cursor: help; background-color: ${annotation.tags[0].privacyConcern.uiColor}">
                                        ${tag.tagAttribute.name}: <b>${tag.name}</b>
                                    </span>
                                </c:forEach>
                            </div>
                        </div>
                    </div>
                    <div class="col-2 d-inline-block text-right">
                        <!-- Focus Button -->
                        <div class='d-inline-block text-right px-2 text-secondary' style='cursor: pointer' data-toggle="tooltip" data-placement="top" title="Click to focus"
                             onclick="documentViewer.focusOnAnnotationClicked(${annotation.id})">
                            <i class="fa fa-external-link fa-lg" aria-hidden="true"></i>
                        </div>
                        <!-- Info Button -->
                        <div class='d-inline-block text-right px-2 text-secondary' style='cursor: pointer' data-toggle="tooltip" data-placement="top"  data-icon="highlightText"
                             title="Highlighted Text">
                            <i class="fa fa-info-circle fa-lg " aria-hidden="true"></i>
                        </div>
                        <!-- Trash Button -->
                        <div class='d-inline-block text-right px-2 text-secondary' style='cursor: pointer' data-toggle="tooltip" data-placement="top" title="Delete Annotation" data-icon='deleteAnnotation'
                             onclick="setDeleteEntityModal('Are you sure you want to delete this annotation?', ${annotation.controversialEntity.id}, ${annotation.id})" data-toggle="modal" data-target="#delete-entity-modal">
                            <i class="fa fa-trash fa-lg"></i>
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <!-- Comment -->
                    <div class="col-9 ">
                        <p>
                            ${annotation.text}
                        </p>
                    </div>
                    <!-- User Details -->
                    <div class="col-3">
                        
                        <div class="d-inline-block clearfix w-100">
                            
                            <div class="float-right bg-light border border-secondary rounded p-md-2">
                                <div class="align-middle">
                                    <h1 class="d-inline py-3 pr-2 pl-1 text-secondary"><i class="fa fa-user" aria-hidden="true"></i></h1>
                                    <div class="d-inline-block float-right">
                                        <div>
                                            by 
                                            <a href="#" onclick="loadUserDetails(${annotation.controversialEntity.metadata.createdBy.id})" data-toggle="modal" data-target="#user-modal" data-button="show-user">
                                                ${annotation.controversialEntity.metadata.createdBy.username}
                                            </a> 
                                        </div>
                                            <span class="text-secondary text-right" data-toggle="tooltip" data-placement="top" title="<fmt:formatDate value="${annotation.controversialEntity.metadata.createdAt}" pattern="EEEE',' dd MMMM yyyy 'at' hh:mm a"/>" >
                                            on <fmt:formatDate value="${annotation.controversialEntity.metadata.createdAt}" pattern="dd/MM/yyyy"/>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
                
                
            </div>

        </div>
    </div>
    <hr/>

</li>