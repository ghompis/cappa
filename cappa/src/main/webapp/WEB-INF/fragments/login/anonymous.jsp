<%-- any content can be specified here e.g.: --%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page pageEncoding="UTF-8" %>

<script>
    function login(){
        AJAX({
            url: '/api/view/user/login',
            method: 'POST',
            data: $('.loginInfo > div').find('form').serialize(),
            onSuccess: function(response, textStatus, jqXHR){
                if( response.status === 'success' ){
                    $('.loginInfo').html(response.html);
                    setInterval( () => location.reload(), 2000);
                }
            }
        });
    }
</script>

<div>
    <button id="btnLogin" type="button" class="btn btn-light dropdown-toggle login" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Login</button>
    <div class="dropdown-menu border border-dark" aria-labelledby="btnLogin">
        <form class="m-2 py-0">
            <div class="form-group-row no-gutters">
                <label for="inputLoginId" class="col-md-12 col-form-label">Username / Email</label>
                <div class="col-md-12">
                    <input type="text" name="username" class="form-control" id="inputLoginId" placeholder="Username or Email">
                </div>
            </div>
            <div class="form-group-row no-gutters">
                <label for="inputLoginPassword" class="col-md-12 col-form-label">Password</label>
                <div class="col-md-12">
                    <input type="password" name="password" class="form-control" id="inputLoginPassword" placeholder="Password">
                </div>
            </div>
            <div class="form-group-row no-gutters my-md-3">
                <div class="d-inline-block w-100 p-1">
                    <div class="d-inline-block float-left">
                        <button type="button" onclick="login()" class="btn btn-primary">Sign in</button>
                    </div>
                    <div class="float-right">
                        or <a class="" href="${pageContext.request.contextPath}/signup" role="button">Sign up</a>
                    </div>
                </div>
            </div>
            <div class="clear"></div>
        </form>
    </div>
</div>
