<%-- any content can be specified here e.g.: --%>
<%@ page pageEncoding="UTF-8" %>

<script>
    function logout(){
        AJAX({
            url: '/api/view/user/logout',
            method: 'POST',
            onSuccess: function(response, textStatus, jqXHR){
                if( response.status === 'success' ){
                    $('.loginInfo').html(response.html);
                    setInterval( () => location.reload(), 2000);
                }
            }
        });
    }
</script>

<div>
    
    <h1 class="d-inline"><i class="fa fa-user align-top" aria-hidden="true"></i></h1>
    
    <div class="d-inline-block align-top dropdown show">
        <a class="btn text-light v-middle dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <span>${user.username} &nbsp;(${user.score})&nbsp;</span>
        </a>
        <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
            <a class="dropdown-item" href="#" onclick="loadUserDetails(${user.id})" data-toggle="modal" data-target="#user-modal" data-button="show-user">Profile</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="javascript:logout()">Logout</a>
        </div>
    </div>
    
</div>

