<%-- 
    Document   : productList
    Created on : Jan 6, 2018, 11:32:01 PM
    Author     : Giorgos Hompis <ghompis@csd.uoc.gr>
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="m" tagdir="/WEB-INF/tags/productList/" %>
<!DOCTYPE html>
<html>
    <head>
        <%@include file="/WEB-INF/jspf/includeHeaders.jspf" %>
    </head>
    
    <body>
        <%@include file="/WEB-INF/jspf/userDetailsModal.jspf" %>
        <%@include file="/WEB-INF/jspf/deleteEntityModal.jspf" %>
        
        <%-- Header --%>
        <header class="fixed-top text-light">
            <%@include file="/WEB-INF/jspf/headerBar.jspf" %>
        </header>
        
        <%-- Navigation Bar --%>
        <nav>
            <%@include file="/WEB-INF/jspf/navBar.jspf" %>
        </nav>


        <%-- Main Content Column --%>
        <div class="container col-md-10">
            
            
            <div id="content" class="container-fluid no-gutters">
                
                <%@include file="/WEB-INF/jspf/productList/header.jspf" %>
                <hr class="seperator mt-1"/>
                <%@include file="/WEB-INF/jspf/productList/list.jspf" %>
                
            </div>

        </div>



        <%-- Footer --%>
        <footer class="container-fluid fixed-bottom text-light">
            <%@include file="/WEB-INF/jspf/footerBar.jspf" %>
        </footer>

        
    </body>
</html>

