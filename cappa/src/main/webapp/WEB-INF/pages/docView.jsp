<%-- 
    Document   : docView
    Created on : Oct 19, 2017, 1:28:34 PM
    Author     : Giorgos Hompis <ghompis@csd.uoc.gr>
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <%@include file="/WEB-INF/jspf/includeHeaders.jspf" %>
    </head>
    <body>
        <%@include file="/WEB-INF/jspf/userDetailsModal.jspf" %>
        <%@include file="/WEB-INF/jspf/deleteEntityModal.jspf" %>


        <%-- Header --%>
        <header class="fixed-top text-light">
            <%@include file="/WEB-INF/jspf/headerBar.jspf" %>
        </header>
        
        <%-- Navigation Bar --%>
        <nav>
            <%@include file="/WEB-INF/jspf/navBar.jspf" %>
        </nav>


        <%-- Main Content Column --%>
        <div class="container col-md-10">
            
            <div id="content" class="container-fluid no-gutters">
                <%@include file="/WEB-INF/jspf/docView/docTitle.jspf" %>
                <hr class="row no-gutters seperator bg-secondary"/>
                <%@include file="/WEB-INF/jspf/docView/docToolbar.jspf" %>
                <%@include file="/WEB-INF/jspf/docView/docPanel.jspf" %>
                <%@include file="/WEB-INF/jspf/docView/docAnnotations.jspf" %>
            </div>

        </div>



        <%-- Footer --%>
        <footer class="container-fluid fixed-bottom text-light">
            <%@include file="/WEB-INF/jspf/footerBar.jspf" %>
        </footer>


    </body>
</html>
