<%-- 
    Document   : main
    Created on : Oct 12, 2017, 8:06:49 PM
    Author     : Giorgos Hompis <ghompis@csd.uoc.gr>
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <%@include file="/WEB-INF/jspf/includeHeaders.jspf" %>
    </head>
    
    <body>
        <%@include file="/WEB-INF/jspf/userDetailsModal.jspf" %>
        <%@include file="/WEB-INF/jspf/deleteEntityModal.jspf" %>
        
        <%-- Header --%>
        <header class="text-light fixed-top">
            <%@include file="/WEB-INF/jspf/headerBar.jspf" %>
        </header>
        <%-- Navigation Bar --%>
        <nav>
            <%@include file="/WEB-INF/jspf/navBar.jspf" %>
        </nav>
        
        
        <%-- Main Content Column --%>
        <div class="container-fluid">
            <div class="row">

                <%-- Main Content Column --%>
                <div class="col-10 m-auto clearfix">

                    <div id="content">
                        <%@include file="/WEB-INF/jspf/digitalProduct/appCard.jspf" %>
                        <hr class="seperator" />
                        <%@include file="/WEB-INF/jspf/digitalProduct/tabs.jspf" %>
                    </div>

                </div>
        
            </div>
        </div>
        
        
            
        <%-- Footer --%>
        <footer  class="text-light fixed-bottom">
            <%@include file="/WEB-INF/jspf/footerBar.jspf" %>
        </footer>
        
    </body>
</html>
