<%-- 
    Document   : main
    Created on : Oct 12, 2017, 8:06:49 PM
    Author     : Giorgos Hompis <ghompis@csd.uoc.gr>
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <%@include file="/WEB-INF/jspf/includeHeaders.jspf" %>
        <script>
            function createNewUser(){
                AJAX({
                    url: '/api/data/user/signup',
                    method: 'POST',
                    data: $('div[data-input=account-form]').find('form').serialize(),
                    onSuccess: function(response, textStatus, jqXHR){
                        alert();
                        if( response.status === 'success' ){
                            setInterval( () => history.back(), 2000);
                        }
                    }
                });
            }
        </script>
    </head>

    <body>
        <%@include file="/WEB-INF/jspf/userDetailsModal.jspf" %>

        <%-- Header --%>
        <header class="text-light fixed-top">
            <%@include file="/WEB-INF/jspf/headerBar.jspf" %>
        </header>


        <div class="container-fluid navbar-collapse" data-input="account-form">
            <div class="row">

                <div class="col-6 m-auto">

                    <div class="form-group row">
                        <h1 class="my-4 mx-2">Create Account</h1>
                    </div>
                    <form>

                        <div class="form-group row">
                            <label for="email" class="col-sm-3 col-form-label"><span class="text-danger">*</span> Email: </label>
                            <div class="col-sm-9">
                                <input type="email" class="form-control" id="email" name="email" placeholder="your email address ..." required="true"/>
                                <!--<small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>-->
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="username" class="col-sm-3 col-form-label"><span class="text-danger">*</span> Username: </label>
                            <div class="col-sm-9">
                                <input type="text" pattern="[A-Za-z0-9]+" class="form-control" id="username" name="username" placeholder="username" required="true"/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="password" class="col-sm-3 col-form-label"><span class="text-danger">*</span> Password: </label>
                            <div class="col-sm-9">
                                <!--<input type="password" pattern="(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$" class="form-control" id="password" name="password" placeholder="password" required="true">-->
                                <input type="password" class="form-control" id="password" name="password" placeholder="password" required="true">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="password-retype" class="col-sm-3 col-form-label"><span class="text-danger">*</span> Retype Password: </label>
                            <div class="col-sm-9">
                                <input type="password" class="form-control" id="password-retype" name="retypePassword" placeholder="retype password" required="true"/>
                            </div>
                        </div>
                        
                        <br/>
                        
                        <div class="form-group row">
                            <label for="age" class="col-sm-3 col-form-label">Age: </label>
                            <div class="col-sm-9">
                                <input type="number" class="form-control" id="age" name="age" />
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="education-level" class="col-sm-3 col-form-label">Education Level: </label>
                            <div class="col-sm-9">
                                <select class="form-control p-0" id="education-level" name="educationLevel">
                                    <option selected=""></option>
                                    <option>High School or Lower</option>
                                    <option>B.Sc. Degree (or similar)</option>
                                    <option>M.Sc. Degree (or similar)</option>
                                    <option>Ph.D. Degree or Higher</option>
                                    <option>Other</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="education-level" class="col-sm-3 col-form-label">Profession: </label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="profession" name="profession" placeholder="Lawyer, Developer etc">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="details" class="col-sm-3 col-form-label">User Details: </label>
                            <div class="col-sm-9">
                                <textarea style="resize:none;" class="form-control" id="details" name="details" placeholder="Write some words about yourself ..."></textarea>
                            </div>
                        </div>
                        
                        <div class="form-group row">
                            <div class="col-sm-3"></div>
                            <div class="col-sm-9">
                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input name="acceptTerms" class="form-check-input" type="checkbox"> 
                                        &nbsp;<span class="text-danger">*</span> I accept <a href="#">Terms and Conditions</a>
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row text-right">
                            <div class="form-group-row">
                                <p class="text-danger">Fields with (*) are mandatory</p>
                            </div>
                        </div>

                        <div class="form-group row text-right">
                            <div class="col-sm">
                                <button type="button" class="btn btn-secondary px-4" onclick='history.back()'>Back</button>
                                <button type="button" class="btn btn-primary px-4" onclick="createNewUser()">Create</button>
                            </div>
                        </div>
                    </form>

                </div>

            </div>
        </div>



        <%-- Footer --%>
        <footer  class="text-light fixed-bottom">
            <%@include file="/WEB-INF/jspf/footerBar.jspf" %>
        </footer>

    </body>
</html>
