<%-- 
    Document   : main
    Created on : Oct 12, 2017, 8:06:49 PM
    Author     : Giorgos Hompis <ghompis@csd.uoc.gr>
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="m" tagdir="/WEB-INF/tags/main/" %>
<!DOCTYPE html>
<html>
    <head>
        <%@include file="/WEB-INF/jspf/includeHeaders.jspf" %>
    </head>
    
    <body>
        <%@include file="/WEB-INF/jspf/userDetailsModal.jspf" %>
        
        <%-- Header --%>
        <header class="text-light fixed-top">
            <%@include file="/WEB-INF/jspf/headerBar.jspf" %>
        </header>
        <%-- Navigation Bar --%>
        <nav>
            <%@include file="/WEB-INF/jspf/navBar.jspf" %>
        </nav>
        

        <%-- Main Content Column --%>
        <div class="container col-md-10">
            
            <div id="content" class="container-fluid no-gutters">
                
                <p class="py-2 px-5">
                    <h2><strong>Collective Awareness platform for PRIvacy Concerns and Expectations.</strong></h2>
<!--                    <p><strong>The Problem</strong></p>
                    <ul>
                        <li>Most smart devices and their applications require access to our personal data.</li>
                        <li>Much of the required data is given to third parties for tracking and advertising purposes.</li>
                        <li>The associated terms and conditions are complex legal documents that most people do not read.</li>
                    </ul>
                    <p><strong>The Idea in a nutshell (<a href="http://www.caprice-community.net/idea/">see more</a>)</strong></p>
                    <p>Create a community and a crowdsourcing platform to:</p>
                    <ul>
                        <li>Help raise awareness to citizens of the privacy related consequences of digital technologies.</li>
                        <li>Inform about ways to protect our private data from unjustified access.</li>
                        <li>Put pressure to developers to comply with our privacy protection needs</li>
                    </ul>-->
                    <p style="text-align: justify;">
                        <strong>See <a href="https://www.caprice-community.net">CAPrice community</a> for details about this platform.</strong>
                        Below you can find all registered digital products that are available to explore & review.
                    </p>
                </p>
                
                <br/>
                <h3 class="font-weight-bold">Hardware</h3>
                <hr class="row no-gutters seperator bg-secondary"/>
                <m:categoryItem title="Appiances & Hardware"
                                description="This category has information about <strong>Appliances & Hardware</strong>" 
                                imageSource="${pageContext.request.contextPath}/img/main/appliances.jpg" 
                                url="${pageContext.request.contextPath}/product/list?productTypes=1" />
                
                <br/>
                <h3 class="font-weight-bold">Software</h3>
                <hr class="row no-gutters seperator bg-secondary"/>
                <m:categoryItem title="Web Applications & Websites"
                                description="This category has information about <strong>Web Applications & Websites</strong>" 
                                imageSource="${pageContext.request.contextPath}/img/main/websites.jpg" 
                                url="${pageContext.request.contextPath}/product/list?productTypes=11" />
                <m:categoryItem title="Mobile Applications"
                                description="This category has information about <strong>Mobile Applications</strong>" 
                                imageSource="${pageContext.request.contextPath}/img/main/mobile-apps.png" 
                                url="${pageContext.request.contextPath}/product/list?productTypes=9" />
                
            </div>

        </div>
        
        
        
            
        <%-- Footer --%>
        <footer  class="text-light fixed-bottom">
            <%@include file="/WEB-INF/jspf/footerBar.jspf" %>
        </footer>
        
    </body>
</html>
