/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

window.addEventListener('DOMContentLoaded', function(){
    
    $(document).ready(function(){
        
        // Fix some defaults for popups
        $.notifyDefaults({
            placement: { from: 'bottom', align: 'right'},
            offset: { y: 80, x:20 }
        });
    
        // Bootstrap enable Tooltips and Popovers
        $(function () {
            $('[data-toggle="tooltip"]').tooltip({
                delay: { "show": 300, "hide": 100 },
                html: true
            });
            $('[data-toggle="popover"]').popover();
        });
    
    
    });
});


