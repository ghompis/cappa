/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


var ToolbarManager = function(){
    
    // ============================================================================================================= //
    // =================================================  DEFINES  ================================================= //
    // ============================================================================================================= //
    
    var _docViewerEditButtons = 'docViewerEditButtons';
    const V = { 
        selectors: {    // S for Selectors (in jquery format)
            buttons : {
                mode : {
                    [C.modes.annotate] : '#'+_docViewerEditButtons+" label[data-viewMode='"+C.modes.annotate+"']",
                    [C.modes.review] : '#'+_docViewerEditButtons+" label[data-viewMode='"+C.modes.review+"']",
                    [C.modes.view] : '#'+_docViewerEditButtons+" label[data-viewMode='"+C.modes.view+"']"
                },
                edit : {
                    newAnnotation : '#'+_docViewerEditButtons+' button[data-button=new-comment]',
                    clear : '#'+_docViewerEditButtons+' button[data-button=clear]'
                },
                filters : {
                    clear : '#'+_docViewerEditButtons+' a[data-button=clear-filters]',
                    manage : '#'+_docViewerEditButtons+' a[data-button=filters-modal-show]'
                }
            }
        }
    };
    
    
    // ============================================================================================================= //
    // =================================================  API  ===================================================== //
    // ============================================================================================================= //
    
    var API = {
        // =========================================== Events Triggers
        selectors: V.selectors
    };
    
    
    
    // ============================================================================================================= //
    // =================================================  INIT  ==================================================== //
    // ============================================================================================================= //
         
    // ================================ Bind "Mode" button callbacks
    $(Object.values(V.selectors.buttons.mode).join(', ')).click( function(){
        var mode = $(this).attr('data-viewMode');
        // Fix mode buttons view
        $(Object.values(V.selectors.buttons.mode).join(',')).removeClass("btn-primary").addClass("btn-secondary");
        $(V.selectors.buttons.mode[mode]).removeClass("btn-secondary").addClass("btn-primary");
        // Fix edit buttons
        if( mode === C.modes.annotate ){
            $(Object.values(V.selectors.buttons.edit).join(',')).show();
        } else {
            $(Object.values(V.selectors.buttons.edit).join(',')).hide();
        }
        //
        documentViewer.modeChanged(mode);
    });
    
    
    // ================================ Bind "New Annotation" button callback to preview highlighted text in modal
    $(V.selectors.buttons.edit.newAnnotation).click( function(){
        $('#annotation-modal ul').html('');
        documentViewer.getHighlightsManager().getHighlightElements().forEach(function(item){
            $('#annotation-modal ul').append( $('<li>', {
                text: item.innerHTML,
                style: "list-style: initial; margin: .5rem 0rem"
            }));
        });
    });
    
    // ================================ Bind "Clear" button callback
    $(V.selectors.buttons.edit.clear).click( function(){
        documentViewer.clearHighlightsClicked();
    });
    
    
    return API;
}