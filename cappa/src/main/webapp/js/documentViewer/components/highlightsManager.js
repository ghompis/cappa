/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var HighlightsManager = function(){
    
    var createTextHighlighter = () => new TextHighlighter( document.getElementById(V.ui_element_id), {
                                    enabled: true,
                                    color: 'rgba(220, 255, 25, .75)', // default highlight color
                                    highlightedClass: 'highlighted',
                                    contextClass: 'highlighter-context',
                                    onRemoveHighlight: function (range) { // before highlight removal
                                        return true;
                                    },
                                    onAfterRemoveHighlight: function (range) { // after highlight removal
//                                                        _data.onAfterRemoveHighlights();
                                        $(Object.values(documentViewer.getToolbarManager().selectors.buttons.edit).join(','))
                                                .each( function(){ $(this).attr('disabled',''); } );
                                    },
                                    onBeforeHighlight: function (range) {
                                        return true;
                                    },
                                    onAfterHighlight: function (range, normalizedHighlights, timestamp) {
//                                                        _data.onAfterHighlights();
                                        $(Object.values(documentViewer.getToolbarManager().selectors.buttons.edit).join(','))
                                                .each( function(){ $(this).removeAttr('disabled'); } );
                                    }
                });
    
    
    

    // ========================================================================================= //
    // ======================================== DEFINES ======================================== //
    // ========================================================================================= //

    var V = { // V for Variables
        ui_element_id: 'docText',
        textHighlighter: null
    };
    V.textHighlighter = createTextHighlighter();
    
    // ========================================================================================= //
    // ===================================== FUNCTIONS ====================================== //
    // ========================================================================================= //
        
    var rerenderAnnotations = function() {
        // Clear previous highlights
        V.textHighlighter.removeHighlights();
        V.textHighlighter = createTextHighlighter();

        if( documentViewer.getMode() === C.modes.annotate )
            return;
        else
            V.textHighlighter.disable();

        // Loop through annotations
        var notVisibleIds = [];
        $(documentViewer.getActiveAnnotations()).each( (annotationIndex, annotation) => {
            // 1. Pass through valid annotations only if view mode
            if( documentViewer.getMode() === C.modes.view && annotation.controversialEntity.state.name.toLowerCase() !== 'valid' ){
                return;
            }

            // 2. Display text parts for each annotation
            if( annotation.textParts !== null ){
                // Try to deserialize them
                try{
                    // Each text part
                    $(annotation.textParts).each( (partIndex, textPart) => {
                        var attributes = {
                            style: 'cursor: pointer; background-color: '+annotation.tags[0].privacyConcern.uiLightColor, 
                            "data-annotation": annotation.id,
                            'data-color': annotation.tags[0].privacyConcern.uiColor,
                            'onclick': 'documentViewer.highlightClicked('+annotation.id+')'
                        };
                        var highlight = V.textHighlighter.deserializeHighlights( '['+textPart.highlightsData+']', attributes );
                    });
                    // Fix hover highlight effect
                    $('span[data-annotation]').hover(
                        (ev) => {
                            var value = $(ev.target).attr('data-annotation');
                            $('span[data-annotation*=\"'+value+'\"] ,li[data-annotation*=\"'+value+'\"]').addClass( 'annotation-hovered' ); }, 
                        () => { 
                            $('span[data-annotation], li[data-annotation]').removeClass( 'annotation-hovered' ); 
                    });
                } catch (e){
                    notVisibleIds.push( annotation );
                }

            }
        });
        documentViewer.getAnnotationDetailsManager().highlightNotVisible(notVisibleIds);
    };
    
        
    // Define document viewer's view modes
    var setViewMode = {
        [C.modes.review] : function(){
            V.textHighlighter.removeHighlights();
            V.textHighlighter.disable();
            rerenderAnnotations();
        },
        [C.modes.view] : function(){
            V.textHighlighter.removeHighlights();
            V.textHighlighter.disable();
            rerenderAnnotations();
        },
        [C.modes.annotate] : function(){
            V.textHighlighter.removeHighlights();
            V.textHighlighter.enable();
        }
    };
    
    
    
    var focusOnAnnotation = (annotationId) => {
        var $container = $('#'+V.ui_element_id),
            $scrollTo = $('span[data-annotation='+annotationId+']');
//        $container.scrollTop($scrollTo.offset().top - $container.offset().top + $container.scrollTop() - 20);
        $($container).animate( 
                        {scrollTop: $scrollTo.offset().top - $container.offset().top + $container.scrollTop() - 20}, 
                        500);
    };
    
    // ========================================================================================= //
    // ========================================== API ========================================== //
    // ========================================================================================= //

    var API = {
        // =========================================== Events Triggers
        modeChanged : (_mode) => setViewMode[_mode](),
        filterAdded: (type, value, $item) => { rerenderAnnotations(); },
        filterRemoved: (type, value) => { rerenderAnnotations(); },
        filtersCleared: () => { rerenderAnnotations(); },
        
        annotationDeleted: (id) => { rerenderAnnotations(); },
        annotationVoted: (id) => { rerenderAnnotations(); },
        annotationCreated: (a) => { rerenderAnnotations(); },
        // =========================================== Functions used in Annotate Mode
        getHighlightsSerialized : function(annotation){
            return V.textHighlighter.serializeHighlights();
        },
        
        removeHighlights : function(){
            return V.textHighlighter.removeHighlights();
        },
        /**
         * Returns an array with highlighted elements
         */
        getHighlightElements : function(){
            return V.textHighlighter.getHighlights();
        },
        hasHighlights : function(){
            return V.textHighlighter !== null && 
                    V.textHighlighter.getHighlights() !== null &&
                    V.textHighlighter.getHighlights().length !== 0;
        },
        
        // =========================================== Functions used in Review Mode
        focusOnAnnotation: (id) => { 
            try{
                focusOnAnnotation(id); 
            } catch(e){
                $.notify({message: "Could not focus on annotation. Is it visible?"}, {type: 'primary' });
            }
        },
        
        hideAnnotations : function() {
            V.textHighlighter.removeHighlights();
        },
        
        annotationsLoaded: () => rerenderAnnotations()
        
        
    };
    
    
    return API;
};
