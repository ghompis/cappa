/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



var AnnotationIndexManager = function(){
    
    // ============================================================================================================= //
    // =================================================  DEFINES  ================================================= //
    // ============================================================================================================= //
    
    
    const V = { 
        tagsIndex: {},
        concernIndex: {},
        ui:{
            ui_concern_selector_fn: (id) => "div[data-concern-id="+id+"]",
            ui_concernCount_selector_fn: (id) => "div[data-concern-id="+id+"] span[data-area=concern-total]",
            ui_attribute_selector_fn: (concernId, attributeId) => "div[data-concern-id="+concernId+"] div[data-tagAttribute-id="+attributeId+"]",
            ui_tag_selector_fn: (id) => "button[data-tag-id="+id+"]",
            ui_tagCount_selector_fn: (id) => "button[data-tag-id="+id+"] span[data-area=tag-total]"
        },
        concernsToAnnotations: {},  // a map of concernId -> [annotations]
        tagsToAnnotations: [],      // a map of tagId -> [annotations]
        annotationIteratorIndex: [] // Stores the index of the loop while we focus on text hightlights
    };
    
    // ============================================================================================================= //
    // ==============================================  FUNCTIONS  ================================================== //
    // ============================================================================================================= //
    
    var generateTagsIndex = () => {
        $(Tags).each( (i, tag) => {
            // Index
            V.tagsIndex[tag.id] = tag;
            V.concernIndex[tag.privacyConcern.id] = tag.privacyConcern;
        });
    };
    
    var rerender = () => {
        // Reset
        V.concernsToAnnotations = [];
        V.tagsToAnnotations = [];
        V.annotationIteratorIndex = [];
        // Generate
        $(documentViewer.getActiveAnnotations()).each( (i, annotation) => {
//            if( documentViewer.getAnnotationFilterManager().evaluate(annotation) ){
                // Concern to annotation
                var annotationConcernId = annotation.tags[0].privacyConcern.id;
                if( V.concernsToAnnotations[annotationConcernId]===undefined ){
                    V.concernsToAnnotations[annotationConcernId] = [];
                }
                V.concernsToAnnotations[annotationConcernId].push(annotation);
                // Tags to annotation
                $(annotation.tags).each( (i, tag) => {
                    // Tags to annotations mapping
                    if( V.tagsToAnnotations[tag.id] === undefined ){
                        V.tagsToAnnotations[tag.id] = [];
                        V.annotationIteratorIndex[tag.id] = 0;
                    }
                    V.tagsToAnnotations[tag.id].push( annotation );
                });
//            }
        }); 
        updateUi();
    };
    
    var updateUi = () => {
        // Hide all
        $(Tags).each( (i, tag) => {
            $(V.ui.ui_attribute_selector_fn(tag.privacyConcern.id, tag.tagAttribute.id)).fadeTo(1, 0.5);
            $(V.ui.ui_tag_selector_fn(tag.id)).fadeTo(1, 0.5);
            $(V.ui.ui_tagCount_selector_fn(tag.id)).html( 0 );
        });
        // Fix Concern labels
        Object.keys( V.concernIndex ).forEach( (concernId) => {
            $(V.ui.ui_concernCount_selector_fn(concernId)).html( V.concernsToAnnotations[concernId]? V.concernsToAnnotations[concernId].length : 0 );
        });
        // Fix Tag Labels & Show only tags that exist in this document
        Object.entries( V.tagsToAnnotations ).forEach( ([tagId,annotations]) => {
            $(V.ui.ui_tagCount_selector_fn(tagId)).html( V.tagsToAnnotations[tagId].length );
            if( V.tagsToAnnotations[tagId].length > 0 ){
                $(V.ui.ui_tag_selector_fn(tagId)).fadeTo(1, 1);
                $(V.ui.ui_attribute_selector_fn( V.tagsIndex[tagId].privacyConcern.id, V.tagsIndex[tagId].tagAttribute.id )).fadeTo(1, 1);
            }
        });
        
//        Object.keys( V.tagsIndex ).forEach( (concernId) => {
//            Object.keys( V.tagsIndex[concernId] ).forEach( (tagAttributeId) => {
//                Object.keys( V.tagsIndex[concernId][tagAttributeId] ).forEach( (tagId) => {
//                    $('div[data-tag-id='+tagId+'] span[data-area=concern-total]').html( V.concernsIndex[concernId] );
//                });
//            });
//            $(V.ui.ui_concernCount_selector_fn(concernId)).html( V.concernsIndex[concernId] );
//        });
    };
    
    
    // ============================================================================================================= //
    // =================================================  API  ===================================================== //
    // ============================================================================================================= //
    
    var API = {
        // =========================================== Events Triggers
        modeChanged: (mode) => {
            
        },
        annotationsLoaded: () => { generateTagsIndex(); rerender(); },
        
        filterAdded: (type, value, $item) => { rerender(); },
        filterRemoved: (type, value) => { rerender(); },
        filtersCleared: () => { rerender(); },
        
        annotationDeleted: (id) => { rerender(); },
        annotationVoted: (id) => { rerender(); },
        annotationCreated: (a) => { rerender(); },
        
        tagClicked: (tagId) => {
            var index = V.annotationIteratorIndex[tagId] = 
                    ((V.annotationIteratorIndex[tagId] + 1) % V.tagsToAnnotations[tagId].length);
            documentViewer.getHighlightsManager().focusOnAnnotation( V.tagsToAnnotations[tagId][index].id );
        }
        
        //
        
    };
    
    
    
    // ============================================================================================================= //
    // =================================================  INIT  ==================================================== //
    // ============================================================================================================= //
        
    
    
    return API;
};
