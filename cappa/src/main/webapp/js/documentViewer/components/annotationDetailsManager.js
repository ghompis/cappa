/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var AnnotationDetailsManager = function(){
    
    var V = {
        ui_selector: 'div[data-area=annotation-details] ul',
        sort:{
            val: {
                selectedSort: {},   // One of the corrensponding objects defined in this var. Initialized below
                orderValue: -1      // Give 1:asc, -1:desc
            },
            ui_button_selector: 'div[data-area=annotation-sorting] button[data-button=sort]',
            ui_data_area_selector: "div[data-area=annotation-sorting] span[data-area=sort-criteria]",
            ui_data_order_selector: "div[data-area=annotation-sorting] span[data-area=sort-order]",
            byCreatedDate: {
                ui_name: 'Created Date',
                ui_dropdown_selector: "div[data-area=annotation-sorting] button[data-button=sort-by-created-date]",
                eval: (a1,a2) => V.sort.val.orderValue * (a1.controversialEntity.metadata.createdAt - a2.controversialEntity.metadata.createdAt)
            },
            byAnnotationScore: {
                ui_name: 'Annotation Score',
                ui_dropdown_selector: "div[data-area=annotation-sorting] button[data-button=sort-by-annotation-score]",
                eval: (a1,a2) => V.sort.val.orderValue * ( (a1.controversialEntity.votes.votesUp - a1.controversialEntity.votes.votesDown) - 
                                (a2.controversialEntity.votes.votesUp - a2.controversialEntity.votes.votesDown) )
            },
            byUserScore: {
                ui_name: 'User Score',
                ui_dropdown_selector: "div[data-area=annotation-sorting] button[data-button=sort-by-user-score]",
                eval: (a1,a2) => V.sort.val.orderValue * (a1.controversialEntity.metadata.createdBy.score - a2.controversialEntity.metadata.createdBy.score)
            },
        },
        visibility: {
            invisibleObjs: {},
            ui_not_visible_icon_selector: "i[data-icon=annotation-not-visible]"
        },
        annotationDetailsUiElements: {} // Initialized on annotations load
    };
    
    // ========================================================================================== //
    // ======================================= INITIALIZE ======================================= //
    // ========================================================================================== //
    
    var initialize = () => {
        V.sort.val.selectedSort = V.sort.byCreatedDate;

        // Fix sort button
        $(V.sort.ui_button_selector).click( () => {
            V.sort.val.orderValue *= -1;
            updateSortUi(); 
            rerenderAnnotationDetails();
        });
        $(V.sort.byAnnotationScore.ui_dropdown_selector).click( () => {
            V.sort.val.selectedSort = V.sort.byAnnotationScore;
            updateSortUi();
            rerenderAnnotationDetails();
        });
        $(V.sort.byCreatedDate.ui_dropdown_selector).click( () => {
            V.sort.val.selectedSort = V.sort.byCreatedDate;
            updateSortUi();
            rerenderAnnotationDetails();
        });
        $(V.sort.byUserScore.ui_dropdown_selector).click( () => {
            V.sort.val.selectedSort = V.sort.byUserScore;
            updateSortUi();
            rerenderAnnotationDetails();
        });
    };
    
    
    // ================================================================================================ //
    // ========================================== Functions =========================================== //
    // ================================================================================================ //
    var requestRenderedUi = function(id, callback){
        AJAX({
            url: '/api/view/product/'+documentViewer.getProductID()+'/document/'+documentViewer.getDocumentID()+'/annotation/'+id,
            method: 'GET',
            contentType: 'application/json',
            success: function(response, textStatus, jqXHR){
                if( response.status === "success" ){
                    callback(response);
                }
            }
        });
    };
    
    
    var initializeUiElements = () => {
        $(Annotations).each( (annotationIndex, annotation) => {
            var el = $(V.ui_selector).find('li[data-annotation='+annotation.id+']');
            V.annotationDetailsUiElements[annotation.id] = el;
            // fix highlights tooltip
            var tooltip = annotation.textParts
                    .reduce( (acc,i) => acc + '<li style="list-style:initial">' + JSON.parse(i.highlightsData).content + '</li>', "Highlighted Text:<br><ul>") + "</ul>";
            $(el).find('div[data-icon=highlightText]').attr( 'title', tooltip );
        });
        rerenderAnnotationDetails();
    };
    
    
    var focudOnAnnotation = (id) => {
        var $container = $(V.ui_selector),
            $scrollTo = $('li[data-annotation='+id+']');
//        $container.scrollTop( $scrollTo.offset().top - $container.offset().top + $container.scrollTop() - 20 );
        $($container).animate( 
            {scrollTop: $scrollTo.offset().top - $container.offset().top + $container.scrollTop() - 20}, 
            500 );
    };
    
    
    var showAnnotation = (id) => {
        Object.keys(V.annotationDetailsUiElements)
            .forEach( annId => V.annotationDetailsUiElements[annId].hide() );
        V.annotationDetailsUiElements[id].show();
    };
    
    var filterAnnotationDetails = () => {
        $(Annotations).each( (annotationIndex, annotation) => {
            V.annotationDetailsUiElements[annotation.id].hide() ;
        });
        $(documentViewer.getActiveAnnotations()).each( (annotationIndex, annotation) => {
            V.annotationDetailsUiElements[annotation.id].show();
        });
    };
    
    
    // ========== Sort
    var rerenderAnnotationDetails = () => {
        // Clear
        $(V.ui_selector).html('');
        // Rerender
        var sort = V.sort.val.selectedSort;
        $( Annotations.sort(sort.eval) ).each( (annotationIndex, annotation) => {
            var el = V.annotationDetailsUiElements[annotation.id];
            $(V.ui_selector).append( el );
            // refix delete popup
            $(el).find('div[data-icon=deleteAnnotation]')
                    .click( () => { $('#delete-entity-modal').modal('show'); } );
        });
        
        // refix tooltips
        $('[data-toggle="tooltip"]').tooltip({
            delay: { "show": 300, "hide": 100 },
            html: true
        });
    };
    
    
    var updateSortUi = () => {
        // Fix sort criteria
        $(V.sort.ui_data_area_selector).html( V.sort.val.selectedSort.ui_name );
        // Fix sort icon
        $(V.sort.ui_data_order_selector).find("i")
                .removeClass("fa-sort-asc fa-sort-desc")
                .addClass( V.sort.val.orderValue===1? "fa-sort-asc" : "fa-sort-desc" );
    };
    
    // ============ Not visible
    var markNotVisilble = (annotationsList) => {
        V.visibility.invisibleObjs = annotationsList
                .map( a =>{ return {[a.id]:a}; })
                .reduce( (acc,i) => $.extend(acc, i), {});
        $(Annotations).each( (i, annotation) => {
            V.visibility.invisibleObjs[annotation.id]?
                $("li[data-annotation="+annotation.id+"] "+V.visibility.ui_not_visible_icon_selector).show() :
                $("li[data-annotation="+annotation.id+"] "+V.visibility.ui_not_visible_icon_selector).hide() ;
        });
    };
    
    
    
    // ========================================================================================== //
    // ========================================== API =========================================== //
    // ========================================================================================== //
    
    var API = {
        // =========================================== Events Triggers
        annotationsLoaded: () => { initializeUiElements(); },
        modeChanged: (mode) => {},
        
        highlightClicked: (id) => { focudOnAnnotation(id); },
        highlightNotVisible: (idList) => { markNotVisilble(idList); },
        
        
        filterAdded: (type, value, $item) => { filterAnnotationDetails(); },
        filterRemoved: (type, value) => { filterAnnotationDetails(); },
        filtersCleared: () => { filterAnnotationDetails(); },
        
        annotationDeleted: (id) => { initializeUiElements(); },
        annotationVoted: (id) => {
            requestRenderedUi(id, (response) => { 
                $(V.ui_selector).find('li[data-annotation='+id+']').remove();
                $(V.ui_selector).append(response.html); 
                initializeUiElements(); 
            }); 
        },
        annotationCreated: (a) => { 
            requestRenderedUi(a.id, (response) => { 
                $(V.ui_selector).append(response.html); 
                initializeUiElements(); 
            });
        },
        // Setters
        
        addAnnotation: function(ids){
            $(ids).forEach(function(index, element){
                requestRenderedUi( element, (response) => {
                    $(V.ui_selector).append(response.html);
                    V.annotationIds.push(element);
                });
            });
        },
        
        clearAnnotationDetails: function(){
            $(V.ui_selector).html('');
        }
        
    };
    
    
    initialize();
    return API;
};
