/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var AnnotationFiltersManager = function(){
    
    var intersectionOf = function( setA, setB ){
        return setA.slice().filter( a => {
            for( var b of setB )
                if( a == b )
                    return true;
            return false;
        });
    };
    
    var getUniquesBy = function(array, fn){
        var seen = {};
        return array.filter( function(item) {
            var k = fn(item);
            return seen.hasOwnProperty(k) ? false : (seen[k] = true);
        });
    };
    
    // ========================================================================================= //
    // ======================================== DEFINES ======================================== //
    // ========================================================================================= //
    
    var V = {
        ui_button_selector: "button[data-target='#filterCollapseDiv']",
        ui_clear_button_selector: "#filterCollapseDiv button[data-button=clearFilters]",
        ui_results_area: 'span[data-area=annotation-results-info]',
        quickFilters:{
            ui_personal_button: "div[data-group=quick-filters] button[data-button=personal]"
        },
        filters: {
            privacyConcerns: { 
                val: {},
                selector: "div[data-area=annotation-details] input[data-input=tag-privacy-concern]",
                selectizer: null,
                isActive: () => Object.keys(V.filters.privacyConcerns.val).length!==0? true : false,
                add: (value) => { V.filters.privacyConcerns.val[value] = true; },
                remove: (value) => { delete V.filters.privacyConcerns.val[value]; },
                clear: () => { 
                    V.filters.privacyConcerns.val = {}; 
                    if(V.filters.privacyConcerns.selectizer) V.filters.privacyConcerns.selectizer.clear(); 
                },
                evaluate: (annotation) => Object.keys(V.filters.privacyConcerns.val).length===0? true :
                        V.filters.privacyConcerns.val[annotation.tags[0].privacyConcern.id]===true
            },
            users: { 
                val: {},
                selector: "div[data-area=annotation-details] input[data-input=user]",
                selectizer: null,
                isActive: () => Object.keys(V.filters.users.val).length!==0? true : false,
                add: (value) => { V.filters.users.val[value] = true; },
                remove: (value) => { delete V.filters.users.val[value]; },
                clear: () => { 
                    V.filters.users.val = {}; 
                    if(V.filters.users.selectizer) V.filters.users.selectizer.clear(); 
                },
                evaluate: (annotation) => Object.keys(V.filters.users.val).length===0? true : 
                        V.filters.users.val[annotation.controversialEntity.metadata.createdBy.id]===true
            },
        },
    }
    
    
    
    
    // ================================================================================================ //
    // ========================================== Functions =========================================== //
    // ================================================================================================ //
    
    var updateFiltersUiStatus = function(){
        var areActive = Object.keys(V.filters)
                .reduce( (acc, f) => acc || V.filters[f].isActive(), false );
        // Filters Button Status
        $(V.ui_button_selector)
                .removeClass( 'btn-primary btn-secondary' )
                .addClass( areActive? 'btn-primary' : 'btn-secondary')
                .find('span').html( areActive? 'Active' : 'Inactive' );
        // Results area
        $(V.ui_results_area)
                .html( 'Total Results: ' + documentViewer.getActiveAnnotations().length
                        + ' / ' + Annotations.length );
        areActive? $(V.ui_results_area).next('i').show(500) : $(V.ui_results_area).next('i').hide(500);
    };
    
    
    var clearAnnotationFilters = function() {
        Object.keys(V.filters)
            .forEach( f => V.filters[f].clear() );
        updateAnnotationResults();      // update active annotations
        updateFiltersUiStatus();        // update ui
        documentViewer.filtersCleared();// notify
    };
    
    var addFilters = (name, value) => {
        if( V.filters[name] ){
            V.filters[name].add(value);
            updateAnnotationResults();                  // update active annotations
            updateFiltersUiStatus();                    // update ui
            documentViewer.filterAdded(name, value);    // notify
        } else {
            alert( 'Filter '+name+' not found' );
        }
    };
    var removeFilters = (name, value) => {
        if( V.filters[name] ){
            V.filters[name].remove(value);
            updateAnnotationResults();                  // update active annotations
            updateFiltersUiStatus();                    // update ui
            documentViewer.filterRemoved(name, value);  // notify
        } else {
            alert( 'Filter '+name+' not found' );
        }
    };
    
    var updateAnnotationResults = () => {
        documentViewer.setActiveAnnotations(
                Annotations.filter(a => API.evaluate(a))
        );
    };
    
    // ========================================================================================== //
    // ========================================== API =========================================== //
    // ========================================================================================== //
    
    var API = {
        // =========================================== Events Triggers
        modeChanged: (mode) => {},
        annotationsLoaded: () => { init(); },
        
        annotationDeleted: (id) => { updateAnnotationResults(); },
        annotationVoted: (id) => { updateAnnotationResults(); },
        annotationCreated: (a) => { updateAnnotationResults(); },
        // ================== Evaluate
        /**
         * Returns true if annotation passes the filter
         * @param {type} annotation
         * @returns {Boolean}
         */
        evaluate: function(annotation){ 
            for( var f of Object.keys(V.filters) ) {
                if( V.filters[f].isActive() && !V.filters[f].evaluate(annotation) )
                    return false;
            }
            return true;
        },
        
        // ================== Callbacks
        setOnFilterActivation: function(_callback){
            
        },
        setOnFilterDeactivation: function(_callback){
            
        }
        
    };
    
    // ========================================================================================== //
    // ========================================== INIT =========================================== //
    // ========================================================================================== //
    
    var init = () => {
        // Fix annotation results bar
        updateFiltersUiStatus();
        $(V.ui_results_area).next('i').hide();
        
        // =================== Generate
        V.filters.privacyConcerns.selectizer = $(V.filters.privacyConcerns.selector).selectize({
            persist: false,
            maxItems: null,
            placeholder: 'Privacy Concern  ',
            valueField: 'id',
            labelField: 'name',
            searchField: ['name'],
            options: Annotations.map( a => a.tags[0].privacyConcern ).sort( (c1,c2) => c1.id - c2.id ),
            render: { 
                item: function(item, escape) {
                    return "<div class='d-inline-block mb-2 p-1' style='line-height: 1.5rem;'>" + 
                        '<span class="py-1 px-3 rounded" style="background-color: '+item.uiColor+'">' +
                            (item.name ? '<span class="tag font-weight-bold">' + escape(item.name) + '</span>' : '') +
                        '</span>' + 
                    '</div>';
                }
            },
            onItemAdd: (value, $item) => { addFilters('privacyConcerns', value, $item); },
            onItemRemove: (value) => { removeFilters('privacyConcerns', value); },
            openOnFocus: true,
        })[0].selectize;
        V.filters.privacyConcerns.selectizer.clear();

        V.filters.users.selectizer = $(V.filters.users.selector).selectize({
            persist: false,
            maxItems: null,
            placeholder: 'User  ',
            valueField: 'id',
            labelField: 'username',
            searchField: ['username'],
            options: $.merge( Annotations.map( a => a.controversialEntity.metadata.createdBy ), User? [User] : [] ).sort(),
            render: { 
                item: function(item, escape) {
                    return "<div class='d-inline-block mb-2 p-1' style='line-height: 1.5rem;'>" + 
                        '<span class="py-1 px-3 rounded badge-primary">' +
                            (item.username ? '<span class="tag font-weight-bold">' + escape(item.username) + '</span>' : '') +
                        '</span>' + 
                    '</div>';
                }
            },
            onItemAdd: (value, $item) => { addFilters('users', value, $item); },
            onItemRemove: (value) => { removeFilters('users', value); },
            openOnFocus: true,
        })[0].selectize;
        V.filters.users.selectizer.clear();
    };
    
    
    // Clear filters button
    $(V.ui_clear_button_selector).click( () => clearAnnotationFilters() );
    
    // Show personal filters button
    $(V.quickFilters.ui_personal_button).click( () => {
        if( User ){ 
            V.filters.users.selectizer.addItem( User.id );
            addFilters('users', User.id);
        } else{
            $.notify({message: "Who are you? Please identify yourself by logging in"}, {type: "warning" });
        }
    });
    
    
    
    return API;
};
