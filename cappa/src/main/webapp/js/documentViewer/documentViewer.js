/* 
 * Handler for 
 */

var documentViewer = (function(_docViewerId, _docViewerEditButtons){

    // ============================================================================================================= //
    // =================================================  DEFINES  ================================================= //
    // ============================================================================================================= //
    
    const V = { // V for Variables
        mode: C.modes.review,
        activeAnnotations: [],
        
        annotationFilterManager: new AnnotationFiltersManager(),
        annotationIndexManager : new AnnotationIndexManager(),
        highlightsManager : new HighlightsManager(),
        annotationDetailsManager: new AnnotationDetailsManager(),
        toolbarManager: new ToolbarManager()
    };
    
    // ============================================================================================================= //
    // ============================================  API FUNCTIONALITY  ============================================ //
    // ============================================================================================================= //

    var API = {
        // Getters
        getMode: () => V.mode,
        getActiveAnnotations: () => V.activeAnnotations,
        setActiveAnnotations: (annotations) => V.activeAnnotations = annotations,
        getDocumentID: () => $('#docText').attr('data-document'),
        getProductID: () => $('#docText').attr('data-product'),
        
        getAnnotationFilterManager : () => V.annotationFilterManager,
        getAnnotationIndexManager : () => V.annotationIndexManager,
        getHighlightsManager : () => V.highlightsManager,
        getAnnotationDetailsManager : () => V.annotationDetailsManager,
        getToolbarManager : () => V.toolbarManager,
        // ============================================================================== //
        // ======================== Setters - Events Propagation ======================== //
        // ============================================================================== //
        annotationsLoaded: () => {
            V.annotationFilterManager.annotationsLoaded();
            V.annotationIndexManager.annotationsLoaded();
            V.highlightsManager.annotationsLoaded();
            V.annotationDetailsManager.annotationsLoaded();
            $(V.toolbarManager.selectors.buttons.mode[V.mode]).click();
        },
        // ======================== annotationFilterManager Events
        filterAdded: (type, value, $item) => {
            V.annotationIndexManager.filterAdded(type, value, $item);
            V.highlightsManager.filterAdded(type, value, $item);
            V.annotationDetailsManager.filterAdded(type, value, $item);
        },
        filterRemoved: (type, value) => {
            V.annotationIndexManager.filterRemoved(type, value);
            V.highlightsManager.filterRemoved(type, value);
            V.annotationDetailsManager.filterRemoved(type, value);
        },
        filtersCleared: () => {
            V.annotationIndexManager.filtersCleared();
            V.highlightsManager.filtersCleared();
            V.annotationDetailsManager.filtersCleared();
        },
        // ======================== annotationIndexManager Events
        tagClicked: (id) => {
            V.annotationIndexManager.tagClicked(id);
        },
        // ======================== highlightsManager Events
        highlightClicked: (id) => {
            V.annotationDetailsManager.highlightClicked(id);
        },
        // ======================== annotationDetailsManager Events
        sortingButtonClicked: () => {
            
        },
        focusOnAnnotationClicked: (id) => {
            V.highlightsManager.focusOnAnnotation(id);
        },
        annotationDeleted: (id) => {
            Annotations = Annotations.filter( a => a.id !== id );
            V.annotationFilterManager.annotationDeleted(id);
            V.annotationIndexManager.annotationDeleted(id);
            V.highlightsManager.annotationDeleted(id);
            V.annotationDetailsManager.annotationDeleted(id);
        },
        annotationVoted: (id) => {
            V.annotationFilterManager.annotationDeleted(id);
            V.annotationIndexManager.annotationVoted(id);
            V.annotationDetailsManager.annotationVoted(id);
            V.highlightsManager.annotationVoted(id);
        },
        // ======================== toolbarManager Events
        modeChanged: (_mode) => {
            V.mode = _mode;
            V.annotationFilterManager.modeChanged(_mode);
            V.annotationIndexManager.modeChanged(_mode);
            V.highlightsManager.modeChanged(_mode);
            V.annotationDetailsManager.modeChanged(_mode);
        },
        annotationCreated: (a) => {
            Annotations.push(a);
            V.annotationFilterManager.annotationCreated(a);
            V.annotationIndexManager.annotationCreated(a);
            V.highlightsManager.annotationCreated(a);
            V.annotationDetailsManager.annotationCreated(a);
        },
        clearHighlightsClicked: () => {
            V.highlightsManager.removeHighlights();
        },
    };

    // ============================================================================================================= //
    // ================================================  INITIALIZE  =============================================== //
    // ============================================================================================================= //
    
    
    // ================================ Load document's Annotations
    var documentID = $('#'+V.ui_docViewerId).attr('data-document');
    AJAX({
        url: '/api/data/document/'+API.getDocumentID()+'/annotations',
        method: 'GET',
        dataType: 'json',
        contentType: 'application/json',
        success: function(response, textStatus, jqXHR){
            if( response.status === "success" ){
                Annotations = response.data;
                V.activeAnnotations = Annotations;
                API.annotationsLoaded();
            }
        }
    });
    
    // ================================ Generate page minimap
    pagemap(document.querySelector('#map'), {
        viewport: document.getElementById('docText'),
        styles: {
            'b,strong': () => 'rgba(0,0,0,0.05)',
            'footer,section,article,p,h5,h6': () => 'rgba(0,0,0,0.08)',
            'header,h1': () => 'rgba(0,0,0,0.20)',
            'h2': () => 'rgba(0,0,0,0.15)',
            'h3': () => 'rgba(0,0,0,0.10)',
            'h4': () => 'rgba(0,0,0,0.09)',
            'span.highlighted': (el) => $(el).attr('data-color')
        },
        back: 'rgba(0,0,0,0.02)',
        view: 'rgba(0,0,0,0.25)',
        drag: 'rgba(0,0,0,0.30)',
        interval: 500
    });

    // ================================ Initialize view mode
//    $(V.toolbarManager.selectors.buttons.mode[V.mode]).click();
    
    // ================================ Finish
    window.documentViewer = this;
    return API;
    
})('docText', 'docViewerEditButtons');






