/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

'use strict';

var Model = function(template, data){

    /**
     * Returns objects data
     * @returns {object} - array of deserialized highlights.
     * @memberof Annotation
     */
    Model.prototype.getData = function(){
        return $.extend(true, {}, template, data);
    };
    
    Model.prototype.getTemplate = function(){
        return $.extend(true, {}, template);
    };
    
    return this;
};
