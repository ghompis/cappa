/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

'use strict';

var ControversialEntity = function(_data){
    
    var template = {
        id: null,
        Metadata: Metadata.prototype.getTemplate(),
        Responses: []       // List<Response>
    };
    
    
    return new Model( template, _data );
}
