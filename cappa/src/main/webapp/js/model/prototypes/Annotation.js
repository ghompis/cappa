/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

'use strict';

var Annotation = function(_data){
    
    var template = {
        id: null,                       
        TextParts: [],                  // List<TextPart>
        ControversialEntity: ControversialEntity.prototype.getTemplate(),
        Tags: []                        // List<T_Tag>
    };
    
    
    return new Model( template, _data );
    
};
