/*! pagemap v0.4.0 - https://larsjung.de/pagemap/ */
(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory();
	else if(typeof define === 'function' && define.amd)
		define([], factory);
	else if(typeof exports === 'object')
		exports["pagemap"] = factory();
	else
		root["pagemap"] = factory();
})(this, function() {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};

/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {

/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;

/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};

/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;

/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}


/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;

/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;

/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";

/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(global) {'use strict';

	var Rect = __webpack_require__(1);

	var win = global.window;
	var doc = win.document;
	var body = doc.querySelector('body');

	var black = function black(pc) {
	    return 'rgba(0,0,0,' + pc / 100 + ')';
	};
	var defaults = {
	    viewport: null,
	    styles: {
	        'header,footer,section,article': black(8),
	        'h1,a': black(10),
	        'h2,h3,h4': black(8)
	    },
	    back: black(2),
	    view: black(5),
	    drag: black(10),
	    interval: null
	};

	var _listener = function _listener(el, method, types, fn) {
	    return types.split(/\s+/).forEach(function (type) {
	        return el[method](type, fn);
	    });
	};
	var on = function on(el, types, fn) {
	    return _listener(el, 'addEventListener', types, fn);
	};
	var off = function off(el, types, fn) {
	    return _listener(el, 'removeEventListener', types, fn);
	};

	module.exports = function (canvas, options) {
	    var settings = Object.assign({}, defaults, options);

	    var context = canvas.getContext('2d');

	    var calcScale = function () {
	        var width = canvas.clientWidth;
	        var height = canvas.clientHeight;
	        return function (w, h) {
	            return Math.min(width / w, height / h);
	        };
	    }();

	    var resizeCanvas = function resizeCanvas(w, h) {
	        canvas.width = w;
	        canvas.height = h;
	        canvas.style.width = w + 'px';
	        canvas.style.height = h + 'px';
	    };

	    var viewport = settings.viewport;
	    var find = function find(sel) {
	        return Array.from((viewport || doc).querySelectorAll(sel));
	    };

	    var drag = false;

	    var rootRect = void 0;
	    var viewRect = void 0;
	    var scale = void 0;
	    var dragRx = void 0;
	    var dragRy = void 0;

	    var drawRect = function drawRect(rect, col) {
	        if (!col) {
	            return;
	        }
	        context.beginPath();
	        context.rect(rect.x, rect.y, rect.w, rect.h);
	        context.fillStyle = col;
	        context.fill();
	    };

	    var applyStyles = function applyStyles(styles) {
	        Object.keys(styles).forEach(function (sel) {
	            var col = styles[sel];
	            find(sel).forEach(function (el) {
	                drawRect(Rect.ofElement(el).relativeTo(rootRect), col(el));
	            });
	        });
	    };

	    var draw = function draw() {
	        rootRect = viewport ? Rect.ofContent(viewport) : Rect.ofDocument();
	        viewRect = viewport ? Rect.ofViewport(viewport) : Rect.ofWindow();
	        scale = calcScale(rootRect.w, rootRect.h);

	        resizeCanvas(rootRect.w * scale, rootRect.h * scale);

	        context.setTransform(1, 0, 0, 1, 0, 0);
	        context.clearRect(0, 0, canvas.width, canvas.height);
	        context.scale(scale, scale);

	        drawRect(rootRect.relativeTo(rootRect), settings.back);
	        applyStyles(settings.styles);
	        drawRect(viewRect.relativeTo(rootRect), drag ? settings.drag : settings.view);
                
//	        resizeCanvas(viewRect.w, viewRect.h
	        canvas.style.width = '100%';
	    };

	    var onDrag = function onDrag(ev) {
	        ev.preventDefault();
	        var cr = Rect.ofViewport(canvas);
	        var x = (ev.pageX - cr.x) / scale - viewRect.w * dragRx;
	        var y = (ev.pageY - cr.y) / scale - viewRect.h * dragRy;

	        if (viewport) {
	            viewport.scrollLeft = x;
	            viewport.scrollTop = y;
	        } else {
	            win.scrollTo(x, y);
	        }
	        draw();
	    };

	    var onDragEnd = function onDragEnd(ev) {
	        drag = false;

	        canvas.style.cursor = 'pointer';
	        body.style.cursor = 'auto';
	        off(win, 'mousemove', onDrag);
	        off(win, 'mouseup', onDragEnd);
	        onDrag(ev);
	    };

	    var onDragStart = function onDragStart(ev) {
	        drag = true;

	        var cr = Rect.ofViewport(canvas);
	        var vr = viewRect.relativeTo(rootRect);
	        dragRx = ((ev.pageX - cr.x) / scale - vr.x) / vr.w;
	        dragRy = ((ev.pageY - cr.y) / scale - vr.y) / vr.h;
	        if (dragRx < 0 || dragRx > 1 || dragRy < 0 || dragRy > 1) {
	            dragRx = 0.5;
	            dragRy = 0.5;
	        }

	        canvas.style.cursor = 'crosshair';
	        body.style.cursor = 'crosshair';
	        on(win, 'mousemove', onDrag);
	        on(win, 'mouseup', onDragEnd);
	        onDrag(ev);
	    };

	    var init = function init() {
	        canvas.style.cursor = 'pointer';
	        on(canvas, 'mousedown', onDragStart);
	        on(canvas, 'wheel', function(ev){ 
                    viewport.scrollTop += ev.deltaY * 30;
                    ev.preventDefault(); 
                });
	        on(viewport || win, 'load resize scroll', draw);
	        if (settings.interval > 0) {
	            setInterval(function () {
	                return draw();
	            }, settings.interval);
	        }
	        draw();
	    };

	    init();

	    return {
	        redraw: draw
	    };
	};
	/* WEBPACK VAR INJECTION */}.call(exports, (function() { return this; }())))

/***/ },
/* 1 */
/***/ function(module, exports) {

	/* WEBPACK VAR INJECTION */(function(global) {"use strict";

	var win = global.window;
	var docEl = win.document.documentElement;

	var Rect = module.exports = function (x, y, w, h) {
	    return Object.assign(Object.create(Rect.prototype), {
	        x: x, y: y, w: w, h: h
	    });
	};

	Rect.prototype = {
	    constructor: Rect,

	    relativeTo: function relativeTo() {
	        var pos = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : { x: 0, y: 0 };

	        return Rect(this.x - pos.x, this.y - pos.y, this.w, this.h);
	    }
	};

	Rect.ofDocument = function () {
	    return Rect(0, 0, docEl.scrollWidth, docEl.scrollHeight);
	};

	Rect.ofWindow = function () {
	    return Rect(win.pageXOffset, win.pageYOffset, docEl.clientWidth, docEl.clientHeight);
	};

	var getOffset = function getOffset(el) {
	    var br = el.getBoundingClientRect();
	    return {
	        x: br.left + win.pageXOffset,
	        y: br.top + win.pageYOffset
	    };
	};

	Rect.ofElement = function (el) {
	    var _getOffset = getOffset(el),
	        x = _getOffset.x,
	        y = _getOffset.y;

	    return Rect(x, y, el.offsetWidth, el.offsetHeight);
	};

	Rect.ofViewport = function (el) {
	    var _getOffset2 = getOffset(el),
	        x = _getOffset2.x,
	        y = _getOffset2.y;

	    return Rect(x + el.clientLeft, y + el.clientTop, el.clientWidth, el.clientHeight);
	};

	Rect.ofContent = function (el) {
	    var _getOffset3 = getOffset(el),
	        x = _getOffset3.x,
	        y = _getOffset3.y;

	    return Rect(x + el.clientLeft - el.scrollLeft, y + el.clientTop - el.scrollTop, el.scrollWidth, el.scrollHeight);
	};
	/* WEBPACK VAR INJECTION */}.call(exports, (function() { return this; }())))

/***/ }
/******/ ])
});
;