/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.caprice.cappa.flow.events;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.sql.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;
import org.caprice.cappa.config.Definitions;
import org.caprice.cappa.flow.repository.ControversialEntityRepository;
import org.caprice.cappa.flow.repository.LegalDocumentRepository;
import org.caprice.cappa.flow.repository.MetadataRepository;
import org.caprice.cappa.flow.service.LegalDocumentService;
import org.caprice.cappa.model.beans.DocumentDownloadResult;
import org.caprice.cappa.model.db.ControversialEntity;
import org.caprice.cappa.model.db.DigitalProduct;
import org.caprice.cappa.model.db.LegalDocument;
import org.caprice.cappa.model.db.Metadata;
import org.caprice.cappa.utils.AppUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Giorgos Hompis <ghompis@csd.uoc.gr>
 */
@Component
public class DocumentVersionUpdateCheck {

    
    @Autowired
    LegalDocumentRepository documentRepository;
    
    @Autowired
    LegalDocumentService documentService;
    
    @Autowired
    MetadataRepository metadataRepository;
    
    @Autowired
    ControversialEntityRepository entityRepository;
    
    @Scheduled(fixedRate = Definitions.DocumentVersionCheckInterval, initialDelay = Definitions.DocumentVersionCheckInterval)
    public void run() {
        Iterable<LegalDocument> documents = documentRepository.findAll();
        Map<DigitalProduct, List<LegalDocument>> documentsPerProduct = StreamSupport.stream(documents.spliterator(), false)
                .collect( Collectors.groupingBy( d -> d.getDigitalProduct(), Collectors.toList() ));
        
        documentsPerProduct.entrySet().stream().forEach( e -> {
            DigitalProduct product = e.getKey();
            List<LegalDocument> docs = e.getValue();
            
            // Gather documents to check (get latest document per url)
            List<LegalDocument> latestDocPerUrl = 
                    // Documents per URL -> Map<String, List<LegalDocument>>
                    product.getLegalDocuments().stream()
                    .collect( Collectors.groupingBy( d -> d.getUrl(), LinkedHashMap::new, Collectors.toList()) )
                    // Map Entry to latest document
                    .entrySet().stream()
                    .map( entry -> ((List<LegalDocument>)entry.getValue()).stream().max((d1, d2) -> d1.getVersion() - d2.getVersion()).get() )
                    .collect( Collectors.toList() );
            
            
            latestDocPerUrl.forEach( d -> {
                // Get new document data
                try{
                    DocumentDownloadResult newDocumentData = AppUtils.downloadAndSanitizeHtmlDocument( d.getUrl() );
                    boolean lastModifiedChanged = newDocumentData.getLastModified()!=null && 
                            newDocumentData.getLastModified() > d.getControversialEntity().getMetadata().getCreatedAt().getTime();
                    boolean isTextDiff = AppUtils.isTextDiffInHtml( d.getHtmlText(), newDocumentData.getDocumentHtml() );
                    if( lastModifiedChanged || isTextDiff ){
                        createUpdatedDocument(d, newDocumentData);
                    }
                } catch(RuntimeException ex){
                    System.err.println("Cannot download document "+ d.getName()+ " of product "+d.getDigitalProduct().getName()+" from url "+d.getUrl());
                }
            });
        });
    }
    
    
    @Transactional
    private void createUpdatedDocument(LegalDocument d, DocumentDownloadResult newData){
        LegalDocument document = new LegalDocument();
        ControversialEntity entity = new ControversialEntity();
        Metadata meta = new Metadata();

        meta.setCreatedAt( new Timestamp(System.currentTimeMillis()) );
        meta.setCreatedBy( Definitions.Defaults.Users.CAPRICE );

        entity.setMetadata(meta);
        entity.setResponses( new ArrayList() );
        entity.setScore(                0.0 );
        entity.setMinConfidenceLimit(   -1.0 );
        entity.setMaxConfidenceLimit(   1.0 );
        entity.setState(              Definitions.Defaults.State.UNCLARIFIED);

        document.setControversialEntity(    entity);
        document.setHtmlText(               newData.getDocumentHtml() );
        document.setDigitalProduct(         d.getDigitalProduct() );
        document.setDocumentType(           d.getDocumentType() );
        document.setLanguage(               d.getLanguage() );
        document.setName(                   d.getDocumentType().getName() + " v" + (d.getVersion()+1) );
        document.setLastModified(           newData.getLastModified()!=null? new Date(newData.getLastModified()) : null );
        document.setCollectedAt(            new Date(System.currentTimeMillis()) );
        document.setUrl(                    d.getUrl() );
        document.setVersion(                d.getVersion()+1 );

        metadataRepository.save( meta );
        entityRepository.save(entity);
        documentRepository.save( document );
    }
    
}
