/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.caprice.cappa.flow.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;
import org.caprice.cappa.config.Definitions;
import org.caprice.cappa.flow.repository.ControversialEntityRepository;
import org.caprice.cappa.flow.repository.DigitalProductRepository;
import org.caprice.cappa.flow.repository.MetadataRepository;
import org.caprice.cappa.flow.repository.UserRepository;
import org.caprice.cappa.flow.repository.enums.T_BusinessCategoryRepository;
import org.caprice.cappa.flow.repository.enums.T_DigitalProductTypeRepository;
import org.caprice.cappa.model.beans.ExceptionMessage;
import org.caprice.cappa.model.db.ControversialEntity;
import org.caprice.cappa.model.db.DigitalProduct;
import org.caprice.cappa.model.db.Metadata;
import org.caprice.cappa.model.db.User;
import org.caprice.cappa.model.db.enums.T_BusinessCategory;
import org.caprice.cappa.model.db.enums.T_DigitalProductType;
import org.caprice.cappa.model.db.enums.T_UserRole;
import org.caprice.cappa.model.formBeans.NewProductForm;
import org.caprice.cappa.model.restBeans.ServerResponse;
import org.caprice.cappa.model.restBeans.ServerResponse.Status;
import org.caprice.cappa.utils.AppUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Giorgos Hompis <ghompis@csd.uoc.gr>
 */

@Service
public class DigitalProductService {
    
    
    @Autowired
    UserRepository userRepository;
    @Autowired
    MetadataRepository metadataRepository;
    @Autowired
    T_BusinessCategoryRepository businessCategoryRepository;
    @Autowired
    T_DigitalProductTypeRepository digitalProductTypeRepository;
    @Autowired
    ControversialEntityRepository controversialEntityRepository;
    
    @Autowired
    DigitalProductRepository digitalProductRepository;
    
    
    @Transactional
    public DigitalProduct createNewDigitalProduct(User user, NewProductForm productForm) throws ExceptionMessage{
        
        if( user == null ){
            throw new ExceptionMessage(Status.WARNING, "You have to log in order to add a new product");
        }
        
        Set<T_UserRole> userRoles = user.getUserRoles();
        if( !userRoles.contains(Definitions.Defaults.Roles.ADMIN) && !userRoles.contains(Definitions.Defaults.Roles.CONTENT_EDITOR) )
            throw new ExceptionMessage(ServerResponse.Status.WARNING, "You don't have permissions to create a new product");
        
        T_DigitalProductType productType = digitalProductTypeRepository.findOne( productForm.getProductType() );
        if( productType == null ){
            throw new ExceptionMessage(Status.DANGER, "Could not find the corrensponding product type in database");
        }
        
        List<T_BusinessCategory> businessCategories = productForm.getBusinessCategories().stream()
                .map(i -> businessCategoryRepository.findOne(i))
                .collect( Collectors.toList() );
        if( businessCategories==null || businessCategories.contains(null) ){
            throw new ExceptionMessage(Status.DANGER, "Could not find the corresponding bussiness categories in database");
        }
        
        // Create
        DigitalProduct product = new DigitalProduct();
        ControversialEntity entity = new ControversialEntity();
        Metadata meta = new Metadata();
        
        meta.setCreatedAt( new Timestamp(System.currentTimeMillis()) );
        meta.setCreatedBy( user );
        
        entity.setMetadata(meta);
        entity.setResponses( new ArrayList() );
        entity.setScore(        0.0 );
        entity.setMinConfidenceLimit( -1.0 );
        entity.setMaxConfidenceLimit( 1.0 );
        entity.setState(              Definitions.Defaults.State.UNCLARIFIED);
        
        product.setControversialEntity( entity);
        product.setBusinessCategories(  businessCategories );
        product.setLegalDocuments(      new TreeSet<>() );
        product.setLogoImageData(       AppUtils.downloadAndFormatProductIcon(productForm.getImageURL()) );
        product.setLogoImagePath(       productForm.getImageURL() );
        product.setName(                productForm.getName() );
        product.setProductType(         productType );
        product.setUrl(                 productForm.getUrl() );

        metadataRepository.save( meta );
        controversialEntityRepository.save( entity );
        digitalProductRepository.save( product );
        
        return product;
    }

    
    public Iterable<DigitalProduct> findByCretiria(String name, Set<T_DigitalProductType> types, Set<T_BusinessCategory> categories){
        List<DigitalProduct> results = 
                StreamSupport.stream( digitalProductRepository.findAll().spliterator(), false)
                    .filter( p -> name==null || name.isEmpty()?             true : p.getName().toLowerCase().contains(name.toLowerCase()) || p.getUrl().toLowerCase().contains(name.toLowerCase()) )
                    .filter( p -> types==null || types.isEmpty()?           true : types.contains( p.getProductType() ) )
                    .filter( p -> categories==null || categories.isEmpty()? true : p.getBusinessCategories().stream().anyMatch( (c) -> categories.contains(c) ) )
                    .sorted((p1,p2) -> p1.getName().compareTo(p2.getName()))
                    .collect( Collectors.toList() );
        return results;
    }
    
    
    
    public DigitalProduct deleteDigitalProduct(User user, DigitalProduct p) throws ExceptionMessage {
        if( user == null )
            throw new ExceptionMessage(ServerResponse.Status.WARNING, "You have to log in order to delete a digital product");
        
        if( !user.getUserRoles().contains( Definitions.Defaults.Roles.ADMIN ) )
            throw new ExceptionMessage(ServerResponse.Status.WARNING, "You cannot delete a digital product unless you have ADMIN permissions");
        
        // Act
        try{
            digitalProductRepository.delete(p);
            //controversialEntityRepository.delete( p.getControversialEntity() );
            //metadataRepository.delete( p.getControversialEntity().getMetadata() );
        } catch(Exception e){
            throw new ExceptionMessage(ServerResponse.Status.WARNING, 
                    "This action could not be completed. Be sure to remove all product's documents before deleting this product");
        }
        return p;
    }
    
    
    public DigitalProduct save(DigitalProduct p){
        return digitalProductRepository.save(p);
    }
    
}
