/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.caprice.cappa.flow.controller.rest.data;

/**
 *
 * @author Giorgos Hompis <ghompis@csd.uoc.gr>
 */

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.caprice.cappa.flow.repository.AnnotationRepository;
import org.caprice.cappa.flow.repository.LegalDocumentRepository;
import org.caprice.cappa.model.formBeans.NewAnnotationForm;
import org.caprice.cappa.model.db.User;
import org.caprice.cappa.flow.repository.UserRepository;
import org.caprice.cappa.flow.service.AnnotationService;
import org.caprice.cappa.model.beans.ExceptionMessage;
import org.caprice.cappa.model.db.Annotation;
import org.caprice.cappa.model.restBeans.DataServerResponse;
import org.caprice.cappa.model.restBeans.ServerResponse;
import org.caprice.cappa.utils.SessionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ViewResolver;

@RestController("AnnotationData")
@RequestMapping(value = "/product/{productId}/document/{documentId}/annotation")
public class AnnotationController {
    
    @Autowired
    ViewResolver viewResolver;
    
    @Autowired
    UserRepository userRepository;
    @Autowired
    LegalDocumentRepository documentRepository;
    @Autowired
    AnnotationRepository annotationRepository;
    
    @Autowired
    AnnotationService annotationService;
    
    
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public DataServerResponse getAnnotationData(HttpServletRequest request,
            @PathVariable("id") Integer annotationId, 
            ModelMap model) {
        Annotation a = annotationRepository.findOne(annotationId);
        return DataServerResponse.get(ServerResponse.Status.SUCCESS, "Success", a);
    }
    
    
    
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public DataServerResponse deleteAnnotation(HttpServletRequest request,
            @PathVariable("productId") Integer productId, 
            @PathVariable("documentId") Integer documentId, 
            @PathVariable("id") Integer annotationId, 
            ModelMap model) {
        Annotation a = annotationRepository.findOne(annotationId);
        if( a != null ) {
            annotationRepository.delete(a);
            return DataServerResponse.get(ServerResponse.Status.SUCCESS, "Success", a);
        }
        else{
            return DataServerResponse.get(ServerResponse.Status.DANGER, "Could not find the given annotation", null);
        }
    }
    
    
    @RequestMapping(value = "/new", method = RequestMethod.POST)
    public DataServerResponse createAnnotation(HttpServletRequest request,
            @PathVariable("productId") Integer productId, 
            @PathVariable("documentId") Integer documentId, 
            ModelMap model, @Valid @RequestBody NewAnnotationForm annotationForm, BindingResult bindingResult) {
        User user = SessionUtils.getLoggedInUser(userRepository, request);
        try {
            Annotation a = annotationService.createNewAnnotation(user, documentId, annotationForm);
            return DataServerResponse.get(ServerResponse.Status.SUCCESS, "Annotation successfully created!", a);
        } catch (ExceptionMessage ex) {
            return ex.getDataResponse();
        }
    }
    
}
