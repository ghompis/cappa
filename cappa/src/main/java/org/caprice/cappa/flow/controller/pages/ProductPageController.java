/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.caprice.cappa.flow.controller.pages;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import javax.servlet.http.HttpServletRequest;
import org.caprice.cappa.flow.repository.DigitalProductRepository;
import org.caprice.cappa.flow.repository.UserRepository;
import org.caprice.cappa.flow.repository.VoteRepository;
import org.caprice.cappa.flow.repository.enums.T_BusinessCategoryRepository;
import org.caprice.cappa.flow.repository.enums.T_DigitalProductTypeRepository;
import org.caprice.cappa.flow.repository.enums.T_DocumentTypeRepository;
import org.caprice.cappa.flow.repository.enums.T_LanguageRepository;
import org.caprice.cappa.flow.service.DigitalProductService;
import org.caprice.cappa.model.db.ControversialEntity;
import org.caprice.cappa.model.db.DigitalProduct;
import org.caprice.cappa.model.db.LegalDocument;
import org.caprice.cappa.model.db.User;
import org.caprice.cappa.model.db.Vote;
import org.caprice.cappa.model.db.enums.T_BusinessCategory;
import org.caprice.cappa.model.db.enums.T_DigitalProductType;
import org.caprice.cappa.utils.SessionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author Giorgos Hompis <ghompis@csd.uoc.gr>
 */
@Controller
@RequestMapping(value = "/product")
public class ProductPageController {
    
    @Autowired
    DigitalProductRepository digitalProductRepository;
    
    @Autowired
    UserRepository userRepository;
    
    @Autowired
    T_DocumentTypeRepository documentTypeRepository;
    @Autowired
    T_LanguageRepository languageRepository;
    @Autowired
    T_DigitalProductTypeRepository digitalProductTypeRepository;
    @Autowired
    T_BusinessCategoryRepository businessCategoryRepository;
    @Autowired
    VoteRepository voteRepository;
    
    @Autowired
    DigitalProductService digitalProductService;
    
    
    /**
     * 
     * @param request
     * @param id
     * @param model
     * @return 
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public String getProduct( HttpServletRequest request, @PathVariable("id") int id, 
                              ModelMap model) {
        User user = SessionUtils.getLoggedInUser(userRepository, request);
        DigitalProduct digitalProduct = digitalProductRepository.findOne(id);
        
        Map<ControversialEntity, Vote> votesByEntity = Collections.EMPTY_MAP;
        if( user != null ){
            votesByEntity = voteRepository.getByUser(user).stream()
                    .collect( Collectors.toMap( vote -> vote.getControversialEntity(), vote -> vote));
        }
        
        Map<String, List<LegalDocument>> documentsPerUrl = digitalProduct.getLegalDocuments().stream()
                .collect( Collectors.groupingBy( d -> d.getUrl(), LinkedHashMap::new, Collectors.toList()) );
        Map<LegalDocument, List<LegalDocument>> documents = documentsPerUrl.values().stream()
                .collect( Collectors.toMap(
                        (List<LegalDocument> l) -> (LegalDocument) l.stream().max( 
                                (d1, d2) -> {
                                    boolean d1Valid = d1.getControversialEntity().getState().getName().toLowerCase().equals("valid");
                                    boolean d2Valid = d2.getControversialEntity().getState().getName().toLowerCase().equals("valid");
                                    if( (d1Valid && d2Valid) || (!d1Valid && !d2Valid) )
                                        return d1.getVersion() - d2.getVersion();
                                    else 
                                        return d1Valid? 1 : -1;
                                }).get(), 
                        Function.identity() ));
        
        
        model.addAttribute("votesByEntity", votesByEntity);
        model.addAttribute("product", digitalProduct);
        model.addAttribute("documents", documents);
        model.addAttribute("documentTypes", documentTypeRepository.findAll());
        model.addAttribute("languages", languageRepository.findAll());
        
        return "digitalProduct";
    }
    
    
    
    /**
     * 
     * @param request
     * @param name
     * @param types
     * @param categories
     * @param model
     * @return 
     */
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public String productList(  HttpServletRequest request, 
            @RequestParam(value = "name", required = false) String name,
            @RequestParam(value = "productTypes", required = false) Set<T_DigitalProductType> types,
            @RequestParam(value = "businessCategories", required = false) Set<T_BusinessCategory> categories,
            ModelMap model) {
        User user = SessionUtils.getLoggedInUser(userRepository, request);
        
        Iterable<T_DigitalProductType> digitalProductTypes = digitalProductTypeRepository.findAll();
        Iterable<T_BusinessCategory> businessCategories = businessCategoryRepository.findAll();
        Iterable<DigitalProduct> products = digitalProductService.findByCretiria(name, types, categories);
        
//        Map<DigitalProduct, Long> annotationsPerProduct = StreamSupport.stream(products.spliterator(), false)
//                .collect( Collectors.toMap(Function.identity(), 
//                        p -> p.getLegalDocuments().stream()
//                                .map( d -> d.getAnnotations() )
//                                .flatMap( a -> a.stream() )
//                                .count()) );
        
//        Map<DigitalProduct, Integer> documentsPerProduct = StreamSupport.stream(products.spliterator(), false)
//                .collect( Collectors.toMap(Function.identity(), p -> p.getLegalDocuments().size()) );
                
//        StreamSupport.stream(products.spliterator(), false)
//                .collect( Collectors.toMap(Function.identity(), 
//                        p -> p.getLegalDocuments().stream()
//                                .map( d -> d.getAnnotations() )
//                                .flatMap( a -> a.stream() )
//                                .map( a -> a.getControversialEntity().getMetadata().getCreatedBy() )
//                                .distinct()
//                                .count()) );
                
        Map<ControversialEntity, Vote> votesByEntity = Collections.EMPTY_MAP;
        if( user != null ){
            votesByEntity = voteRepository.getByUser(user).stream()
                    .collect( Collectors.toMap( vote -> vote.getControversialEntity(), vote -> vote));
        }
        
        model.addAttribute("votesByEntity", votesByEntity);
        model.addAttribute("digitalProductTypes", digitalProductTypes);
        model.addAttribute("businessCategories", businessCategories);
        model.addAttribute("productsList", products);
        
        return "productList";
    }
    
    
}
