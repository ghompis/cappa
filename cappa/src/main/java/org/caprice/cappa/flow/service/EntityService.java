/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.caprice.cappa.flow.service;

import java.sql.Timestamp;
import java.util.List;
import java.util.Set;
import org.caprice.cappa.config.Definitions;
import org.caprice.cappa.flow.repository.ControversialEntityRepository;
import org.caprice.cappa.flow.repository.MetadataRepository;
import org.caprice.cappa.flow.repository.enums.T_UserRoleRepository;
import org.caprice.cappa.flow.repository.UserRepository;
import org.caprice.cappa.flow.repository.VoteRepository;
import org.caprice.cappa.model.beans.ExceptionMessage;
import org.caprice.cappa.model.beans.WilsonScoreIntervalResult;
import org.caprice.cappa.model.db.Annotation;
import org.caprice.cappa.model.db.ControversialEntity;
import org.caprice.cappa.model.db.DigitalProduct;
import org.caprice.cappa.model.db.LegalDocument;
import org.caprice.cappa.model.db.Metadata;
import org.caprice.cappa.model.db.User;
import org.caprice.cappa.model.db.Vote;
import org.caprice.cappa.model.db.enums.T_UserRole;
import org.caprice.cappa.model.formBeans.VoteForm;
import org.caprice.cappa.model.restBeans.ServerResponse;
import org.caprice.cappa.model.restBeans.ServerResponse.Status;
import org.caprice.cappa.utils.AppUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Giorgos Hompis <ghompis@csd.uoc.gr>
 */

@Service
public class EntityService {
    
    @Autowired
    AnnotationService annotationService;
    
    @Autowired
    LegalDocumentService documentService;
    
    @Autowired
    DigitalProductService productService;
    
    
    @Autowired
    ControversialEntityRepository controversialEntityRepository;
    
    @Autowired
    UserRepository userRepository;
    
    @Autowired
    MetadataRepository metadataRepository;
    
    @Autowired
    VoteRepository voteRepository;
    
    
    @Transactional
    public Vote submitVoteForm(User user, VoteForm form) throws ExceptionMessage {
        if( user == null )
            throw new ExceptionMessage(Status.WARNING, "You have to log in order to vote");

        ControversialEntity entity = controversialEntityRepository.findOne( form.getControversialEntityId() );
        if( entity == null )
            throw new ExceptionMessage(Status.DANGER, "Could not find the corresponding entry in db");
        
        if( entity.getMetadata().getCreatedBy().getId() == user.getId() )
            throw new ExceptionMessage(Status.WARNING, "You cannot vote an entity created by you");
        
        List<Vote> alreadyVotes = voteRepository.getByControversialEntityAndUser(user, entity);
        Vote v;
        // Delete previous vote
        if( !alreadyVotes.isEmpty() ){
            v = alreadyVotes.get(0);
            if( form.getValue() == v.getVoteValue() ){
                voteRepository.delete(v);
                int newUpvotes =     entity.getVotes().getVotesUp() +    (v.getVoteValue()>0? -1:0);
                int newDownvotes =   entity.getVotes().getVotesDown() +  (v.getVoteValue()<0? -1:0);
                updateEntityScore(entity, newUpvotes, newDownvotes);
                throw new ExceptionMessage(Status.SUCCESS, "Your previous vote has been taken back");
            } else {
                throw new ExceptionMessage(Status.WARNING, "You have already voted this entity");
            }
        // Create new vote
        } else {
            v = new Vote();
             
            List<Annotation> annotation = controversialEntityRepository.getControversialEntityAsAnnotation(entity);
            List<LegalDocument> legalDocument = controversialEntityRepository.getControversialEntityAsLegalDocument(entity);
            List<DigitalProduct> digitalProduct = controversialEntityRepository.getControversialEntityAsDigitalProduct(entity);
            Set<T_UserRole> userRoles = user.getUserRoles();
            if( annotation!=null && !annotation.isEmpty() ){
//                if( !userRoles.contains(DefaultUserRoles.ADMIN) && !userRoles.contains(DefaultUserRoles.CONTENT_EDITOR) && !userRoles.contains(DefaultUserRoles.ELEVATED_USER) )
//                    throw new ExceptionMessage(ServerResponse.Status.WARNING, "You don't have permissions to vote an annotation");
            } else if( legalDocument!=null && !legalDocument.isEmpty() ){
                if( !userRoles.contains(Definitions.Defaults.Roles.ADMIN) && !userRoles.contains(Definitions.Defaults.Roles.CONTENT_EDITOR) && !userRoles.contains(Definitions.Defaults.Roles.ELEVATED_USER) )
                    throw new ExceptionMessage(ServerResponse.Status.WARNING, "You don't have permissions to vote a document");
            } else if( digitalProduct!=null && !digitalProduct.isEmpty() ){
                if( !userRoles.contains(Definitions.Defaults.Roles.ADMIN) && !userRoles.contains(Definitions.Defaults.Roles.CONTENT_EDITOR) && !userRoles.contains(Definitions.Defaults.Roles.ELEVATED_USER) )
                    throw new ExceptionMessage(ServerResponse.Status.WARNING, "You don't have permissions to vote a product");
            } else {
                throw new ExceptionMessage(Status.DANGER, "Could not find entity type. DB error");
            }

            // Create
            Metadata m = new Metadata();
            m.setCreatedAt( new Timestamp(System.currentTimeMillis()) );
            m.setCreatedBy(user);

            v.setControversialEntity(entity);
            v.setMetadata(m);
            v.setVoteValue(form.getValue());

            metadataRepository.save(m);
            voteRepository.save(v);
            
            int newUpvotes =     entity.getVotes().getVotesUp() +    (form.getValue()>0? 1:0);
            int newDownvotes =   entity.getVotes().getVotesDown() +  (form.getValue()<0? 1:0);
            updateEntityScore(entity, newUpvotes, newDownvotes);
            return v;
        }
    }

    
    private void updateEntityScore(ControversialEntity entity, int entityUpvotes, int entityDownvotes){
        // Update entity score
        WilsonScoreIntervalResult wilsonScoreResult = AppUtils.calcWilsonScoreInterval( entityUpvotes, entityDownvotes);
        entity.setScore(              Math.round(200.0 * wilsonScoreResult.getValue()) / 100.0 - 1 );
        entity.setMinConfidenceLimit( Math.round(200.0 * wilsonScoreResult.getMinLimit()) / 100.0 - 1 );
        entity.setMaxConfidenceLimit( Math.round(200.0 * wilsonScoreResult.getMaxLimit()) / 100.0 - 1 );
        entity.setState(    entity.getMaxConfidenceLimit()<0? Definitions.Defaults.State.INVALID : 
                            entity.getMinConfidenceLimit()>0? Definitions.Defaults.State.VALID : 
                                                              Definitions.Defaults.State.UNCLARIFIED);
        controversialEntityRepository.save(entity);
    }
    
    
    
    public ControversialEntity deleteEntity(User user, Integer entityId) throws ExceptionMessage {
        
        if( user == null )
            throw new ExceptionMessage(Status.WARNING, "You have to log in order to delete an entity");

        ControversialEntity controversialEntity = controversialEntityRepository.findOne(entityId );
        if( controversialEntity == null )
            throw new ExceptionMessage(Status.DANGER, "Could not find the corresponding entry in db");
        
        if( !user.getUserRoles().contains( Definitions.Defaults.Roles.ADMIN ) && 
                controversialEntity.getMetadata().getCreatedBy().getId() != user.getId() )
            throw new ExceptionMessage(Status.WARNING, "You cannot delete an entity not created by you");
        
        
        List<Annotation> annotation = controversialEntityRepository.getControversialEntityAsAnnotation(controversialEntity);
        List<LegalDocument> legalDocument = controversialEntityRepository.getControversialEntityAsLegalDocument(controversialEntity);
        List<DigitalProduct> digitalProduct = controversialEntityRepository.getControversialEntityAsDigitalProduct(controversialEntity);
        
        if( annotation!=null && !annotation.isEmpty() ){
            annotationService.deleteAnnotation(user, annotation.get(0) );
        } else if( legalDocument!=null && !legalDocument.isEmpty() ){
            documentService.deleteLegalDocument(user, legalDocument.get(0) );
        } else if( digitalProduct!=null && !digitalProduct.isEmpty() ){
            productService.deleteDigitalProduct(user, digitalProduct.get(0) );
        } else {
            throw new ExceptionMessage(Status.DANGER, "Could not find the corresponding entry type in db");
        }
        return controversialEntity;
    }
}
