/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.caprice.cappa.flow.controller.rest.view;

/**
 *
 * @author Giorgos Hompis <ghompis@csd.uoc.gr>
 */

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import org.caprice.cappa.config.Definitions;
import org.caprice.cappa.flow.repository.DigitalProductRepository;
import org.caprice.cappa.model.formBeans.NewAnnotationForm;
import org.caprice.cappa.model.db.enums.T_Tag;
import org.caprice.cappa.model.db.User;
import org.caprice.cappa.flow.repository.UserRepository;
import org.caprice.cappa.flow.service.AnnotationService;
import org.caprice.cappa.flow.service.DigitalProductService;
import org.caprice.cappa.model.beans.ExceptionMessage;
import org.caprice.cappa.model.db.Annotation;
import org.caprice.cappa.model.db.DigitalProduct;
import org.caprice.cappa.model.db.LegalDocument;
import org.caprice.cappa.model.formBeans.NewDocumentForm;
import org.caprice.cappa.model.formBeans.NewProductForm;
import org.caprice.cappa.model.restBeans.DataServerResponse;
import org.caprice.cappa.model.restBeans.ServerResponse;
import org.caprice.cappa.model.restBeans.ViewServerResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController("ProductView")
@RequestMapping(value = "/product")
public class ProductController {
    
    @Autowired
    DigitalProductRepository productRepository;
    
    @Autowired
    AnnotationService annotationService;
    @Autowired
    DigitalProductService productService;
    
    
    
    @RequestMapping(value = "/new", method = RequestMethod.POST )
    public ViewServerResponse newDigitalProduct(HttpServletRequest request, @RequestBody NewProductForm form, ModelMap model) {
        System.out.println(form);
        String html = form.toString();
        User user = (User) request.getSession().getAttribute( Definitions.Attributes.USER );
        try {
            productService.createNewDigitalProduct(user, form);
            return ViewServerResponse.get(ServerResponse.Status.SUCCESS, "A new product has been successfully created", html);
        } catch (ExceptionMessage ex) {
            return ex.getViewResponse();
        }
    }
    
}
