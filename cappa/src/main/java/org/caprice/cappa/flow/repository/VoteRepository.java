/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.caprice.cappa.flow.repository;

import java.util.List;
import org.caprice.cappa.model.db.ControversialEntity;
import org.caprice.cappa.model.db.User;
import org.caprice.cappa.model.db.Vote;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author Giorgos Hompis <ghompis@csd.uoc.gr>
 */
public interface VoteRepository extends CrudRepository<Vote, Integer> {
    
    @Query("select v from Vote v where v.Metadata.CreatedBy = ?1")
    public List<Vote> getByUser(User user);
    
    @Query("select v from Vote v where v.Metadata.CreatedBy = ?1 and v.ControversialEntity = ?2")
    public List<Vote> getByControversialEntityAndUser(User user, ControversialEntity entity);
    
    
    
}
