/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.caprice.cappa.flow.controller.rest.data;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.caprice.cappa.flow.repository.UserRepository;
import org.caprice.cappa.flow.service.EntityService;
import org.caprice.cappa.model.beans.ExceptionMessage;
import org.caprice.cappa.model.db.ControversialEntity;
import org.caprice.cappa.model.db.User;
import org.caprice.cappa.model.db.Vote;
import org.caprice.cappa.model.formBeans.VoteForm;
import org.caprice.cappa.model.restBeans.DataServerResponse;
import org.caprice.cappa.model.restBeans.ServerResponse;
import org.caprice.cappa.utils.SessionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Giorgos Hompis <ghompis@csd.uoc.gr>
 */
@RestController("ControversialEntityData")
@RequestMapping(value = "/controversialEntity")
public class ControversialEntityController {

    @Autowired
    UserRepository userRepository;
    
    @Autowired
    EntityService entityService;

    // Use @ModelAttribute for direct form submissions
    // Use @RequestBody for JSON data in request body
    @RequestMapping(value = "/vote/new", method = RequestMethod.POST)
    public DataServerResponse createNewVote(HttpServletRequest request,
            ModelMap model, @Valid @RequestBody VoteForm form, BindingResult bindingResult) {
        User user = SessionUtils.getLoggedInUser(userRepository, request);
        try {
            Vote v = entityService.submitVoteForm(user, form);
            return DataServerResponse.get(ServerResponse.Status.SUCCESS, "Vote sucessfully submitted", v);
        } catch (ExceptionMessage ex) {
            return ex.getDataResponse();
        }
    }

    
    
    
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public DataServerResponse deleteEntity(HttpServletRequest request,
            @PathVariable("id") Integer entityId, 
            ModelMap model) {
        User user = SessionUtils.getLoggedInUser(userRepository, request);
        try {
            ControversialEntity e = entityService.deleteEntity(user, entityId);
            return DataServerResponse.get(ServerResponse.Status.SUCCESS, "Entity has been successfully deleted", e);
        } catch (ExceptionMessage ex) {
            return ex.getDataResponse();
        }
    }
    
}
