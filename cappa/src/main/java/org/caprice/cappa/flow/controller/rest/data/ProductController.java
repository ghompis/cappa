/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.caprice.cappa.flow.controller.rest.data;

/**
 *
 * @author Giorgos Hompis <ghompis@csd.uoc.gr>
 */

import javax.servlet.http.HttpServletRequest;
import org.caprice.cappa.config.Definitions;
import org.caprice.cappa.flow.repository.DigitalProductRepository;
import org.caprice.cappa.model.db.User;
import org.caprice.cappa.flow.service.AnnotationService;
import org.caprice.cappa.flow.service.DigitalProductService;
import org.caprice.cappa.model.beans.ExceptionMessage;
import org.caprice.cappa.model.db.DigitalProduct;
import org.caprice.cappa.model.formBeans.NewProductForm;
import org.caprice.cappa.model.restBeans.DataServerResponse;
import org.caprice.cappa.model.restBeans.ServerResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController("ProductData")
@RequestMapping(value = "/product")
public class ProductController {
    
    @Autowired
    DigitalProductRepository productRepository;
    
    @Autowired
    AnnotationService annotationService;
    @Autowired
    DigitalProductService productService;
    
    
    
    @RequestMapping(value = "/new", method = RequestMethod.POST )
    public DataServerResponse newDigitalProductData(HttpServletRequest request, @RequestBody NewProductForm form, ModelMap model) {
        System.out.println(form);
        User user = (User) request.getSession().getAttribute( Definitions.Attributes.USER );
        try {
            DigitalProduct product = productService.createNewDigitalProduct(user, form);
            return DataServerResponse.get(ServerResponse.Status.SUCCESS, "A new product has been successfully created", product);
        } catch (ExceptionMessage ex) {
            return ex.getDataResponse();
        }
    }
    
    
    
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public DataServerResponse getDocumentData(HttpServletRequest request,
            @PathVariable("id") Integer id, 
            ModelMap model) {
        DigitalProduct p = productRepository.findOne(id);
        return DataServerResponse.get(ServerResponse.Status.SUCCESS, "Success", p);
    }
    
    
    
    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public DataServerResponse getDocumentData(HttpServletRequest request, ModelMap model) {
        Iterable<DigitalProduct> findAll = productRepository.findAll();
        return DataServerResponse.get(ServerResponse.Status.SUCCESS, "Success", findAll);
    }
    
    
    
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public DataServerResponse deleteDigitalProduct(HttpServletRequest request,
            @PathVariable("id") Integer id, 
            ModelMap model) {
        DigitalProduct d = productRepository.findOne(id);
        if( d != null ) {
            productRepository.delete(d);
            return DataServerResponse.get(ServerResponse.Status.SUCCESS, "Success", d);
        }
        else{
            return DataServerResponse.get(ServerResponse.Status.DANGER, "Could not find the given product", null);
        }
    }
    
}
