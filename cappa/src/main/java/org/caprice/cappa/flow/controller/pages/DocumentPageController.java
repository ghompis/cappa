/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.caprice.cappa.flow.controller.pages;

import com.fasterxml.jackson.core.JsonProcessingException;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;
import javax.servlet.http.HttpServletRequest;
import org.caprice.cappa.flow.repository.UserRepository;
import org.caprice.cappa.flow.repository.VoteRepository;
import org.caprice.cappa.flow.repository.enums.T_PrivacyConcernRepository;
import org.caprice.cappa.flow.repository.enums.T_TagRepository;
import org.caprice.cappa.flow.service.LegalDocumentService;
import org.caprice.cappa.model.db.Annotation;
import org.caprice.cappa.model.db.ControversialEntity;
import org.caprice.cappa.model.db.LegalDocument;
import org.caprice.cappa.model.db.User;
import org.caprice.cappa.model.db.Vote;
import org.caprice.cappa.model.db.enums.T_PrivacyConcern;
import org.caprice.cappa.model.db.enums.T_Tag;
import org.caprice.cappa.model.db.enums.T_TagAttribute;
import org.caprice.cappa.model.readability.ReadabilityScoresWrapper;
import org.caprice.cappa.utils.AppUtils;
import org.caprice.cappa.utils.SessionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author Giorgos Hompis <ghompis@csd.uoc.gr>
 */
@Controller
@RequestMapping(value = "/product/{productId}/document")
public class DocumentPageController {
    
    @Autowired
    LegalDocumentService documentService;
    
    @Autowired
    T_TagRepository tagRepository;
    @Autowired
    T_PrivacyConcernRepository privacyConcernRepository;
    @Autowired
    UserRepository userRepository;
    
    @Autowired
    VoteRepository voteRepository;
    
    
    
    
    @RequestMapping(value = "/{docId}", method = RequestMethod.GET)
    public String docView(HttpServletRequest request,
            @PathVariable int productId,
            @PathVariable int docId,
            @RequestParam(required = false) String mode,
            @RequestParam(required = false) List<Integer> filterConcerns,
            @RequestParam(required = false) List<Integer> filterUsers,
            ModelMap model) throws JsonProcessingException {
        
        LegalDocument document = documentService.findOne(docId);
        User user = SessionUtils.getLoggedInUser(userRepository, request);
        Iterable<T_PrivacyConcern> privacyConcerns = privacyConcernRepository.findAll();
        
        // =========== Available Tags
        Iterable<T_Tag> tags = tagRepository.findAll();
        
        // =========== Tags per Attribute per Privacy Concern (for all doc's tags)
        Map<T_PrivacyConcern, Map<T_TagAttribute, Map<T_Tag, Long>>> tagsIndexData = StreamSupport.stream(tags.spliterator(), false)
        // Sort them
            .sorted( (t1, t2) -> t1.getPrivacyConcernAttribute().getPrivacyConcern().getId() - t2.getPrivacyConcernAttribute().getPrivacyConcern().getId() == 0? 
                (t1.getPrivacyConcernAttribute().getUiIndex() - t2.getPrivacyConcernAttribute().getUiIndex() == 0? 
                        t1.getId() - t2.getId() : t1.getPrivacyConcernAttribute().getUiIndex() - t2.getPrivacyConcernAttribute().getUiIndex() ) : 
                t1.getPrivacyConcernAttribute().getPrivacyConcern().getId() - t2.getPrivacyConcernAttribute().getPrivacyConcern().getId() )
        // Group by concern and attribute
            .collect( Collectors.groupingBy( tag -> tag.getPrivacyConcernAttribute().getPrivacyConcern(), LinkedHashMap::new,
                        Collectors.groupingBy( tag -> tag.getPrivacyConcernAttribute().getTagAttribute(), LinkedHashMap::new,
                            Collectors.groupingBy( Function.identity(), Collectors.counting() ) ) ) );
        System.out.println(tagsIndexData);
        
        
        
        
        
        // =========== Annotations per Privacy Concern (for all doc's annotations)
        Map<T_PrivacyConcern, List<Annotation>> annotationsData = 
                // Get all annotations
            document.getAnnotations().stream()
                .collect( Collectors.groupingBy( tag -> tag.getTags().iterator().next().getPrivacyConcernAttribute().getPrivacyConcern(), 
                        LinkedHashMap::new, Collectors.toList() ) );
        
        
        // =========== Votes by Controversial Entity (for all user's controversial entities)
        Map<ControversialEntity, Vote> votesByEntity = Collections.EMPTY_MAP;
        if( user != null ){
            votesByEntity = voteRepository.getByUser(user).stream()
                    .collect( Collectors.toMap( vote -> vote.getControversialEntity(), vote -> vote));
        }
        
        
        // =========== Users of Annotations (for all doc's annotations
        Set<User> annotationUsers = annotationsData.values().stream()
                .flatMap( l -> l.stream() )
                .map( a -> a.getControversialEntity().getMetadata().getCreatedBy() )
                .collect( Collectors.toSet() );
        
        
        // =========== Get document readability metrics
        ReadabilityScoresWrapper scores = AppUtils.getReadabilityScores( document.getHtmlText() );
        
        model.addAttribute("tags", tags);
        model.addAttribute("document", document);
        model.addAttribute("privacyConcerns", privacyConcerns);
        model.addAttribute("tagsData", tagsIndexData);
//        model.addAttribute("tagMappings", tagMappings);
        model.addAttribute("annotationsData", annotationsData);
        model.addAttribute("votesByEntity", votesByEntity);
        model.addAttribute("annotationUsers", annotationUsers);
        model.addAttribute("readabilityScores", scores);
//        model.addAttribute("annotationsJSON", new ObjectMapper().writeValueAsString(document.getAnnotations()) );

        return "docView";
    }
    
    
    
}
