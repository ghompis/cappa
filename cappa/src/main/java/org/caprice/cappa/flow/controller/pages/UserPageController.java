/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.caprice.cappa.flow.controller.pages;

import javax.validation.Valid;
import org.caprice.cappa.model.formBeans.SignupForm;
import org.caprice.cappa.flow.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author Giorgos Hompis <ghompis@csd.uoc.gr>
 */

@Controller
public class UserPageController {
    
    @Autowired
    UserService userService;
    
    
    // ================================ SING-UP ================================ //
    
    @RequestMapping(value = "/signup", method = RequestMethod.GET)
    public String getSignupPage(ModelMap model) {
        return "signup";
    }

    // Use @ModelAttribute for direct form submissions
    // Use @RequestBody for JSON data in request body
    @RequestMapping(value = "/signup", method = RequestMethod.POST)
    public String signup(@Valid @ModelAttribute SignupForm userForm, BindingResult bindingResult) {
        System.out.println(userForm);
        if( bindingResult.hasErrors() )
            return "signup";
        else {
            //userService.createNewUser(userForm);
            return "main";
        }
    }

    
}
