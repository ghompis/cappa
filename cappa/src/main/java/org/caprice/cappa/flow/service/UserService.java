/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.caprice.cappa.flow.service;

import java.security.MessageDigest;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.caprice.cappa.config.Definitions;
import org.caprice.cappa.model.formBeans.SignupForm;
import org.caprice.cappa.model.db.User;
import org.caprice.cappa.flow.repository.enums.T_UserRoleRepository;
import org.caprice.cappa.flow.repository.UserRepository;
import org.caprice.cappa.model.beans.ExceptionMessage;
import org.caprice.cappa.model.formBeans.LoginForm;
import org.caprice.cappa.model.restBeans.ServerResponse;
import org.caprice.cappa.model.restBeans.ServerResponse.Status;
import org.caprice.cappa.utils.AppUtils;
import org.caprice.cappa.utils.RenderUtils;
import org.caprice.cappa.utils.SessionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Giorgos Hompis <ghompis@csd.uoc.gr>
 */

@Service
public class UserService {
    
    
    @Autowired
    UserRepository userRepository;
    
    @Autowired
    T_UserRoleRepository roleRepository;
    
    
    public User createNewUser(SignupForm userForm) throws ExceptionMessage {
        // Check
        List<User> findByEmail = userRepository.findByEmail( userForm.getEmail());
        if( !findByEmail.isEmpty() )
            throw new ExceptionMessage(Status.DANGER, "Email already in use. Please choose a different email");
        
        List<User> findByUsername = userRepository.findByUsername( userForm.getUsername() );
        if( !findByUsername.isEmpty() )
            throw new ExceptionMessage(Status.DANGER, "Username already in use. Please choose a different username");
            
        // Create
        User user = new User();
        
        user.setEmail(         userForm.getEmail() );
        user.setUsername(      userForm.getUsername() );
        user.setPasswordHash(  AppUtils.getMD5HashAsBase64( userForm.getPassword() ) );
        
        user.setAge(           userForm.getAge() );
        user.setEducationLevel(userForm.getEducationLevel() );
        user.setProfession(    userForm.getProfession() );
        user.setDetails(       userForm.getDetails() );
        
        user.setCreateTime(    new Timestamp(System.currentTimeMillis()) );
        user.setUserRoles(     new HashSet<>(Arrays.asList(Definitions.Defaults.Roles.USER)) );
        
        userRepository.save(user);

        return user;
    }

    
    public User logout(HttpServletRequest request, User user) throws ExceptionMessage {
        if( user == null ){
            throw new ExceptionMessage(Status.WARNING, "No logged in user found");
        }
        // Logout
        SessionUtils.removeLoggedInUser(request);
        return user;
    }
    
    
    public User login(HttpServletRequest request, User user, LoginForm form) throws ExceptionMessage {
        if( user != null ){
            throw new ExceptionMessage(Status.WARNING, "You have to log out first before login with another account");
        }
        if( form==null || form.getUsername()==null || form.getPassword()==null ){
            throw new ExceptionMessage(Status.DANGER, "You have to complete username and password in order to login");
        }
        
        List<User> dbUsers = userRepository.findByUsername( form.getUsername() );
        if( dbUsers.isEmpty() ){
            throw new ExceptionMessage(Status.DANGER, "Wrong credentials");
        }
        if( dbUsers.size()>1 ){
            throw new ExceptionMessage(Status.DANGER, "Multiple users found with same username");
        }
        
        User dbUser = dbUsers.get(0);
        String givenPasswordHash = AppUtils.getMD5HashAsBase64( form.getPassword() );
        if( !givenPasswordHash.equals(dbUser.getPasswordHash()) ){
            throw new ExceptionMessage(Status.DANGER, "Wrong credentials");
        }
        
        // Actual login
        SessionUtils.setLoggedInUser(request, dbUser);
        return dbUser;
    }
    
    
    public User save(User u){
        return userRepository.save(u);
    }
    
}
