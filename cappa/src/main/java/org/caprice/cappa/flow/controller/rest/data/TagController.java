/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.caprice.cappa.flow.controller.rest.data;

/**
 *
 * @author Giorgos Hompis <ghompis@csd.uoc.gr>
 */

import javax.servlet.http.HttpServletRequest;
import org.caprice.cappa.model.db.enums.T_Tag;
import org.caprice.cappa.flow.repository.enums.T_TagRepository;
import org.caprice.cappa.model.db.enums.T_PrivacyConcern;
import org.caprice.cappa.model.restBeans.DataServerResponse;
import org.caprice.cappa.model.restBeans.ServerResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController("TagData")
@RequestMapping(value = "/tag")
public class TagController {
    
    @Autowired
    T_TagRepository tagRepository;
    
    
    
    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public DataServerResponse getTags(HttpServletRequest request, ModelMap model) {
        Iterable<T_Tag> tags = tagRepository.findAll();
        return DataServerResponse.get(ServerResponse.Status.SUCCESS, "Success", tags);
    }
    
    
    
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public DataServerResponse getTagsByPrivacyConcern(HttpServletRequest request, 
            @RequestParam(value = "privacyConcern", required=false) T_PrivacyConcern concern, 
            ModelMap model) {
        Iterable<T_Tag> tags = tagRepository.findByPrivacyConcern(concern);
        return DataServerResponse.get(ServerResponse.Status.SUCCESS, "Success", tags);
    }
    
}
