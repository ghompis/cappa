/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.caprice.cappa.flow.repository.enums;

import java.util.List;
import org.caprice.cappa.model.db.enums.T_PrivacyConcern;
import org.caprice.cappa.model.db.enums.T_PrivacyConcernAttribute;
import org.caprice.cappa.model.db.enums.T_Tag;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author Giorgos Hompis <ghompis@csd.uoc.gr>
 */
public interface T_TagRepository extends CrudRepository<T_Tag, Integer> {

    public List<T_Tag> findByName(String name);
    
    
    @Query("select t from T_Tag t where t.name = ?1 and t.PrivacyConcernAttribute = ?2")
    public List<T_Tag> findByNameAndPrivacyConcernAttribute(String name, T_PrivacyConcernAttribute attribute);
    
    
    @Query("select t from T_Tag t where t.PrivacyConcernAttribute.PrivacyConcern = ?1")
    public List<T_Tag> findByPrivacyConcern(T_PrivacyConcern concern);
    
}
