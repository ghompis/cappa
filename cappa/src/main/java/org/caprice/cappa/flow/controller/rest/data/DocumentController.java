/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.caprice.cappa.flow.controller.rest.data;

/**
 *
 * @author Giorgos Hompis <ghompis@csd.uoc.gr>
 */

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.caprice.cappa.flow.repository.LegalDocumentRepository;
import org.caprice.cappa.model.db.User;
import org.caprice.cappa.flow.repository.UserRepository;
import org.caprice.cappa.flow.service.LegalDocumentService;
import org.caprice.cappa.model.beans.ExceptionMessage;
import org.caprice.cappa.model.db.Annotation;
import org.caprice.cappa.model.db.LegalDocument;
import org.caprice.cappa.model.formBeans.NewDocumentForm;
import org.caprice.cappa.model.restBeans.DataServerResponse;
import org.caprice.cappa.model.restBeans.ServerResponse;
import org.caprice.cappa.model.restBeans.ServerResponse.Status;
import org.caprice.cappa.utils.SessionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;



@RestController("DocumentData")
@RequestMapping(value = "/document")
public class DocumentController {
    
    @Autowired
    LegalDocumentService documentService;
    
    @Autowired
    UserRepository userRepository;
    @Autowired
    LegalDocumentRepository documentRepository;
    
    
    
    @RequestMapping(value = "/new", method = RequestMethod.POST )
    public DataServerResponse newDocumentData(HttpServletRequest request, @RequestBody NewDocumentForm form, ModelMap model, BindingResult bindingResult) {
        User user = SessionUtils.getLoggedInUser(userRepository, request);
        System.out.println(form);
        if( bindingResult.hasErrors() ){
            System.out.println("Form with errors");
        }
        try {
            LegalDocument document = documentService.createNewLegalDocument(user, form);
            return DataServerResponse.get(Status.SUCCESS, "A New Document successfully created", document);
        } catch (ExceptionMessage ex) {
            System.out.println(ex);
            return ex.getDataResponse();
        }
    }
    
    
    
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public DataServerResponse getDocumentData(HttpServletRequest request,
            @PathVariable("id") Integer id, 
            ModelMap model) {
        LegalDocument d = documentRepository.findOne(id);
        return DataServerResponse.get(ServerResponse.Status.SUCCESS, "Success", d);
    }
    
    
    
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public DataServerResponse deleteLegalDocument(HttpServletRequest request,
            @PathVariable("id") Integer id, 
            ModelMap model) {
        LegalDocument d = documentRepository.findOne(id);
        if( d != null ) {
            documentRepository.delete(d);
            return DataServerResponse.get(ServerResponse.Status.SUCCESS, "Success", d);
        }
        else{
            return DataServerResponse.get(ServerResponse.Status.DANGER, "Could not find the given document", null);
        }
    }
    
    
    
    @RequestMapping(value = "/{id}/annotations", method = RequestMethod.GET)
    public DataServerResponse getDocumentAnnotations(HttpServletRequest request,
            @PathVariable("id") Integer id, 
            ModelMap model) {
        List<Annotation> annotations = documentRepository.findOne(id).getAnnotations();
        annotations.forEach(a -> { // Map legal document to null to avoid a non-uniform JSON serialization
            a.setLegalDocument(null);
        });
        return DataServerResponse.get(ServerResponse.Status.SUCCESS, "Success", annotations);
    }
    
    
}
