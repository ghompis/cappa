/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.caprice.cappa.flow.repository.enums;

import java.util.List;
import org.caprice.cappa.model.db.enums.T_UserRole;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author Giorgos Hompis <ghompis@csd.uoc.gr>
 */
public interface T_UserRoleRepository extends CrudRepository<T_UserRole, Integer> {

    public List<T_UserRole> findByName(String name);
    
    
}
