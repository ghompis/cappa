/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.caprice.cappa.flow.controller.rest.view;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.caprice.cappa.config.Definitions;
import org.caprice.cappa.model.db.User;
import org.caprice.cappa.flow.repository.UserRepository;
import org.caprice.cappa.flow.service.UserService;
import org.caprice.cappa.model.beans.ExceptionMessage;
import org.caprice.cappa.model.formBeans.LoginForm;
import org.caprice.cappa.model.formBeans.SignupForm;
import org.caprice.cappa.model.restBeans.DataServerResponse;
import org.caprice.cappa.model.restBeans.ServerResponse;
import org.caprice.cappa.model.restBeans.ViewServerResponse;
import org.caprice.cappa.utils.RenderUtils;
import org.caprice.cappa.utils.SessionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ViewResolver;

/**
 *
 * @author Giorgos Hompis <ghompis@csd.uoc.gr>
 */
@RestController("UserView")
@RequestMapping(value = "/user")
public class UserController {
    
    @Autowired
    ViewResolver viewResolver;
    
    @Autowired
    UserRepository userRepository;
    
    @Autowired
    UserService userService;
    
    
    
    // ================================ User Details ================================ //
    @RequestMapping(value = "/{user}", method = RequestMethod.GET )
    public ViewServerResponse getUserDetails(HttpServletRequest request, @PathVariable User user, ModelMap model) {
        model.put("user", user);
        String html = RenderUtils.renderView("userDetails", viewResolver, request, model);
        return ViewServerResponse.get(ServerResponse.Status.SUCCESS, "success", html);
    }
    
    
    
    
    
    // ================================ LOGIN / LOGOUT ================================ //
    @RequestMapping(value = "/login", method = RequestMethod.POST )
    public ViewServerResponse login(HttpServletRequest request, @Valid @ModelAttribute LoginForm form, BindingResult result, ModelMap model) {
        User user = SessionUtils.getLoggedInUser(userRepository, request);
        try {
            userService.login( request, user, form );
            String html = RenderUtils.renderView("login/logged", viewResolver, request, model);
            return ViewServerResponse.get(ServerResponse.Status.SUCCESS, "Login Successfull", html);
        } catch (ExceptionMessage ex) {
            return ex.getViewResponse();
        }
    }
    

    @RequestMapping(value = "/logout", method = {RequestMethod.GET, RequestMethod.POST})
    public ViewServerResponse logout(HttpServletRequest request, ModelMap model) {
        User user = SessionUtils.getLoggedInUser(userRepository, request);
        try {
            userService.logout(request, user);
            String html = RenderUtils.renderView("login/anonymous", viewResolver, request, model);
            return ViewServerResponse.get(ServerResponse.Status.SUCCESS, "Logout successfull", html);
        } catch (ExceptionMessage ex) {
            return ex.getViewResponse();
        }
    }
    
    
}