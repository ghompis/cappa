/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.caprice.cappa.flow.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;
import org.caprice.cappa.config.Definitions;
import org.caprice.cappa.flow.repository.AnnotationRepository;
import org.caprice.cappa.flow.repository.ControversialEntityRepository;
import org.caprice.cappa.flow.repository.LegalDocumentRepository;
import org.caprice.cappa.flow.repository.MetadataRepository;
import org.caprice.cappa.flow.repository.enums.T_UserRoleRepository;
import org.caprice.cappa.flow.repository.UserRepository;
import org.caprice.cappa.flow.repository.enums.T_PrivacyConcernAttributeRepository;
import org.caprice.cappa.flow.repository.enums.T_PrivacyConcernRepository;
import org.caprice.cappa.flow.repository.enums.T_TagRepository;
import org.caprice.cappa.model.beans.ExceptionMessage;
import org.caprice.cappa.model.db.Annotation;
import org.caprice.cappa.model.db.ControversialEntity;
import org.caprice.cappa.model.db.DigitalProduct;
import org.caprice.cappa.model.db.LegalDocument;
import org.caprice.cappa.model.db.Metadata;
import org.caprice.cappa.model.db.TextPart;
import org.caprice.cappa.model.db.User;
import org.caprice.cappa.model.db.enums.T_PrivacyConcern;
import org.caprice.cappa.model.db.enums.T_Tag;
import org.caprice.cappa.model.db.enums.T_UserRole;
import org.caprice.cappa.model.formBeans.NewAnnotationForm;
import org.caprice.cappa.model.restBeans.ServerResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Giorgos Hompis <ghompis@csd.uoc.gr>
 */
@Service
public class AnnotationService {
    
    
    @Autowired
    UserRepository userRepository;
    
    @Autowired
    T_UserRoleRepository roleRepository;
    
    @Autowired
    AnnotationRepository annotationRepository;
    
    @Autowired
    ControversialEntityRepository controversialEntityRepository;
    
    @Autowired
    MetadataRepository metadataRepository;
    
    @Autowired
    LegalDocumentRepository documentRepository;
    
    @Autowired
    T_PrivacyConcernRepository privacyConcernRepository;
    
    @Autowired
    T_PrivacyConcernAttributeRepository privacyConcernAttributeRepository;
    
    @Autowired
    T_TagRepository tagRepository;
    
    @Transactional
    public Annotation createNewAnnotation(User user, int documentId, NewAnnotationForm form) throws ExceptionMessage{
        
        if( user == null ){
            throw new ExceptionMessage(ServerResponse.Status.WARNING, "You have to log in order to create an Annotation");
        }
        
        LegalDocument document = documentRepository.findOne( documentId );
        if( document == null ){
            throw new ExceptionMessage(ServerResponse.Status.DANGER, "Unable to find the given document in database");
        }
        System.out.println(form.getPrivacyConcernId());
        
        // Collect tags data from DB
        List<T_Tag> tags = form.getTagIds().stream()
                .map( id -> tagRepository.findOne(id) )
                .filter( concern -> concern.getPrivacyConcernAttribute().getPrivacyConcern().getId() == form.getPrivacyConcernId() )
                .sorted( (t1,t2) -> t1.getPrivacyConcernAttribute().getUiIndex() - t2.getPrivacyConcernAttribute().getUiIndex() )
                .collect( Collectors.toList() );
        
        boolean allMandatoriesCompleted = StreamSupport.stream(privacyConcernAttributeRepository.findAll().spliterator(), false)
                .filter( att -> att.getPrivacyConcern().getId() == form.getPrivacyConcernId() )
                .filter( att -> att.getIsMandatory() )
                .allMatch( att -> tags.stream().map(t->t.getPrivacyConcernAttribute().getId()).anyMatch( id -> id == att.getId() ) );
        if( !allMandatoriesCompleted ){
            throw new ExceptionMessage(ServerResponse.Status.WARNING, "Annotation has not created. You have to complete all mandatory tags");
        }
        
        // Create
        Annotation a = new Annotation();
        ControversialEntity entity = new ControversialEntity();
        Metadata meta = new Metadata();
        
        form.getTextParts().stream() .forEach( part -> { part.setAnnotation(a); }); // By Directional relationship to text parts
        
        meta.setCreatedAt(  new Timestamp(System.currentTimeMillis()) );
        meta.setCreatedBy(  user );
        
        entity.setMetadata(     meta );
        entity.setResponses(    new ArrayList<>() );
        entity.setScore(        0.0 );
        entity.setMinConfidenceLimit( -1.0 );
        entity.setMaxConfidenceLimit( 1.0 );
        entity.setState(              Definitions.Defaults.State.UNCLARIFIED);
        
        a.setControversialEntity(   entity);
        a.setLegalDocument(         document );
        a.setTags(                  tags );
        a.setText(                  form.getComment() );
        a.setTextParts(             form.getTextParts());

        metadataRepository.save(meta);
        controversialEntityRepository.save(entity);
        annotationRepository.save(a);

        return a;
    }
    
    
    
    
    public Annotation deleteAnnotation(User user, Annotation a) throws ExceptionMessage {
        if( user == null )
            throw new ExceptionMessage(ServerResponse.Status.WARNING, "You have to log in order to delete an annotation");
        
        if( !user.getUserRoles().contains( Definitions.Defaults.Roles.ADMIN ) && 
                a.getControversialEntity().getMetadata().getCreatedBy().getId() != user.getId() )
            throw new ExceptionMessage(ServerResponse.Status.WARNING, "You cannot delete an annotation not created by you");
        
        // Act
        try{
            annotationRepository.delete(a);
            //controversialEntityRepository.delete( a.getControversialEntity() );
            //metadataRepository.delete( a.getControversialEntity().getMetadata() );
        } catch(Exception e){
            throw new ExceptionMessage(ServerResponse.Status.DANGER, 
                    "This action could not be completed.");
        }
        return a;
    }
}
