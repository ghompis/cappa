/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.caprice.cappa.flow.repository;

import java.util.List;
import org.caprice.cappa.model.db.Annotation;
import org.caprice.cappa.model.db.ControversialEntity;
import org.caprice.cappa.model.db.DigitalProduct;
import org.caprice.cappa.model.db.LegalDocument;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author Giorgos Hompis <ghompis@csd.uoc.gr>
 */
public interface ControversialEntityRepository extends CrudRepository<ControversialEntity, Integer> {
    
    
    @Query("select a from Annotation a where a.ControversialEntity = ?1")
    public List<Annotation> getControversialEntityAsAnnotation(ControversialEntity entity);
    
    @Query("select d from LegalDocument d where d.ControversialEntity = ?1")
    public List<LegalDocument> getControversialEntityAsLegalDocument(ControversialEntity entity);
    
    @Query("select p from DigitalProduct p where p.ControversialEntity = ?1")
    public List<DigitalProduct> getControversialEntityAsDigitalProduct(ControversialEntity entity);
    
}
