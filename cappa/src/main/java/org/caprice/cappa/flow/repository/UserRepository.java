/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.caprice.cappa.flow.repository;

import java.util.List;
import org.caprice.cappa.model.db.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author Giorgos Hompis <ghompis@csd.uoc.gr>
 */
public interface UserRepository extends CrudRepository<User, Integer> {
    
    Page<User> findAll(Pageable pageable);

    List<User> findByEmail(String email);
    List<User> findByUsername(String username);
    List<User> findByPasswordHash(String passwordHash);
    
    
    
}
