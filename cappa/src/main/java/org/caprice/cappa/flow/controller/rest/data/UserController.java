/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.caprice.cappa.flow.controller.rest.data;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.caprice.cappa.model.db.User;
import org.caprice.cappa.flow.repository.UserRepository;
import org.caprice.cappa.flow.service.UserService;
import org.caprice.cappa.model.beans.ExceptionMessage;
import org.caprice.cappa.model.formBeans.LoginForm;
import org.caprice.cappa.model.formBeans.SignupForm;
import org.caprice.cappa.model.restBeans.DataServerResponse;
import org.caprice.cappa.model.restBeans.ServerResponse;
import org.caprice.cappa.utils.SessionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ViewResolver;

/**
 *
 * @author Giorgos Hompis <ghompis@csd.uoc.gr>
 */
@RestController("UserData")
@RequestMapping(value = "/user")
public class UserController {
    
    @Autowired
    ViewResolver viewResolver;
    
    @Autowired
    UserRepository userRepository;
    
    @Autowired
    UserService userService;
    
    
    // ====================================
    
    @RequestMapping(value = "/{user}")
    public DataServerResponse getUserById(@PathVariable User user, ModelMap model) {
        return DataServerResponse.get(ServerResponse.Status.SUCCESS, "success", user);
    }
    
    // ================================ SIGNUP ================================ //
    
    @RequestMapping(value = "/signup", method = RequestMethod.POST)
    public DataServerResponse signup(HttpServletRequest request, @Valid @ModelAttribute SignupForm userForm, BindingResult bindingResult, ModelMap model) {
        if( bindingResult.hasErrors() )
            return DataServerResponse.get(ServerResponse.Status.DANGER, "The form contains errors. Please fill in the form with correct values", null);
        try{
            User createdUser = userService.createNewUser(userForm);
            return DataServerResponse.get(ServerResponse.Status.SUCCESS, "User has been successfully created", createdUser);
        } catch(ExceptionMessage ex){
            return ex.getDataResponse();
        }
    }
    
    // ================================ LOGIN / LOGOUT ================================ //

    @RequestMapping(value = "/login", method = RequestMethod.POST )
    public DataServerResponse loginData(HttpServletRequest request, @Valid LoginForm form, BindingResult result, ModelMap model) {
        User user = SessionUtils.getLoggedInUser(userRepository, request);
        try {
            User loggedUser = userService.login( request, user, form );
            return DataServerResponse.get(ServerResponse.Status.SUCCESS, "Login Successfull", loggedUser);
        } catch (ExceptionMessage ex) {
            return ex.getDataResponse();
        }
    }

    
    @RequestMapping(value = "/logout", method = {RequestMethod.GET, RequestMethod.POST})
    public DataServerResponse logoutData(HttpServletRequest request, ModelMap model) {
        User user = SessionUtils.getLoggedInUser(userRepository, request);
        try {
            User logoutUser= userService.logout(request, user);
            return DataServerResponse.get(ServerResponse.Status.SUCCESS, "Logout successfull", logoutUser);
        } catch (ExceptionMessage ex) {
            return ex.getDataResponse();
        }
    }
    
}