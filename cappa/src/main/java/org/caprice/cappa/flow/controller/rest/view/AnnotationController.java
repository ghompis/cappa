/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.caprice.cappa.flow.controller.rest.view;

/**
 *
 * @author Giorgos Hompis <ghompis@csd.uoc.gr>
 */

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.caprice.cappa.config.Definitions;
import org.caprice.cappa.flow.repository.AnnotationRepository;
import org.caprice.cappa.flow.repository.LegalDocumentRepository;
import org.caprice.cappa.model.formBeans.NewAnnotationForm;
import org.caprice.cappa.model.db.enums.T_Tag;
import org.caprice.cappa.model.db.User;
import org.caprice.cappa.flow.repository.UserRepository;
import org.caprice.cappa.flow.repository.VoteRepository;
import org.caprice.cappa.flow.service.AnnotationService;
import org.caprice.cappa.model.beans.ExceptionMessage;
import org.caprice.cappa.model.db.Annotation;
import org.caprice.cappa.model.db.ControversialEntity;
import org.caprice.cappa.model.db.Vote;
import org.caprice.cappa.model.restBeans.DataServerResponse;
import org.caprice.cappa.model.restBeans.ServerResponse;
import org.caprice.cappa.model.restBeans.ViewServerResponse;
import org.caprice.cappa.utils.RenderUtils;
import org.caprice.cappa.utils.SessionUtils;
import org.caprice.cappa.utils.converters.TextHighlightUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ViewResolver;

@RestController("AnnotationView")
@RequestMapping(value = "/product/{productId}/document/{documentId}/annotation")
public class AnnotationController {
    
    @Autowired
    ViewResolver viewResolver;
    
    @Autowired
    UserRepository userRepository;
    @Autowired
    LegalDocumentRepository documentRepository;
    @Autowired
    AnnotationRepository annotationRepository;
    @Autowired
    VoteRepository voteRepository;
    
    @Autowired
    AnnotationService annotationService;
    
    
    
    
    @RequestMapping(value = "/new", method = RequestMethod.POST)
    public ViewServerResponse createAnnotation(HttpServletRequest request,
            @PathVariable("productId") Integer productId, 
            @PathVariable("documentId") Integer documentId, 
            ModelMap model, @Valid @RequestBody NewAnnotationForm annotationForm, BindingResult bindingResult) {
        User user = SessionUtils.getLoggedInUser(userRepository, request);
        
        try {
            Annotation a = annotationService.createNewAnnotation(user, documentId, annotationForm);
            model.put("annotation", a);
            String html = RenderUtils.renderView("annotation/annotation", viewResolver, request, model);
            return ViewServerResponse.get(ServerResponse.Status.SUCCESS, "Annotation successfully created!", html);
        } catch (ExceptionMessage ex) {
            return ex.getViewResponse();
        }
    }
    
    
    
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ViewServerResponse getAnnotationDetailsView(HttpServletRequest request,
            @PathVariable("id") Integer annotationId, 
            ModelMap model) {
        User user = SessionUtils.getLoggedInUser(userRepository, request);
        Map<ControversialEntity, Vote> votesByEntity = Collections.EMPTY_MAP;
        if( user != null ){
            votesByEntity = voteRepository.getByUser(user).stream()
                    .collect( Collectors.toMap( vote -> vote.getControversialEntity(), vote -> vote));
        }
        Annotation a = annotationRepository.findOne(annotationId);
        model.put("annotation", a);
        model.put("votesByEntity", votesByEntity);
        String html = RenderUtils.renderView("annotation/annotation", viewResolver, request, model);
        return ViewServerResponse.get(ServerResponse.Status.SUCCESS, "success", html);
    }
    
    
}
