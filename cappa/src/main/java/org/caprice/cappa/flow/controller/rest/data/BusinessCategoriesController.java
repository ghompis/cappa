/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.caprice.cappa.flow.controller.rest.data;

/**
 *
 * @author Giorgos Hompis <ghompis@csd.uoc.gr>
 */

import javax.servlet.http.HttpServletRequest;
import org.caprice.cappa.flow.repository.enums.T_BusinessCategoryRepository;
import org.caprice.cappa.model.db.enums.T_BusinessCategory;
import org.caprice.cappa.model.restBeans.DataServerResponse;
import org.caprice.cappa.model.restBeans.ServerResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RestController;

@RestController("BussinessCategoriesData")
@RequestMapping(value = "/businessCategories")
public class BusinessCategoriesController {
    
    @Autowired
    T_BusinessCategoryRepository businessCategoriesRepository;
    
    
    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public DataServerResponse getBusinessCategories(HttpServletRequest request, ModelMap model) {
        Iterable<T_BusinessCategory> categories = businessCategoriesRepository.findAll();
        return DataServerResponse.get(ServerResponse.Status.SUCCESS, "Success", categories);
    }
    
    
}
