/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.caprice.cappa.flow.controller.rest.view;

/**
 *
 * @author Giorgos Hompis <ghompis@csd.uoc.gr>
 */

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import org.caprice.cappa.config.Definitions;
import org.caprice.cappa.flow.repository.LegalDocumentRepository;
import org.caprice.cappa.model.formBeans.NewAnnotationForm;
import org.caprice.cappa.model.db.enums.T_Tag;
import org.caprice.cappa.model.db.User;
import org.caprice.cappa.flow.repository.UserRepository;
import org.caprice.cappa.flow.service.AnnotationService;
import org.caprice.cappa.flow.service.LegalDocumentService;
import org.caprice.cappa.model.beans.ExceptionMessage;
import org.caprice.cappa.model.db.Annotation;
import org.caprice.cappa.model.db.LegalDocument;
import org.caprice.cappa.model.formBeans.NewDocumentForm;
import org.caprice.cappa.model.restBeans.DataServerResponse;
import org.caprice.cappa.model.restBeans.ServerResponse;
import org.caprice.cappa.model.restBeans.ServerResponse.Status;
import org.caprice.cappa.model.restBeans.ViewServerResponse;
import org.caprice.cappa.utils.SessionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;



@RestController("DocumentView")
@RequestMapping(value = "/document")
public class DocumentController {
    
    @Autowired
    LegalDocumentService documentService;
    
    @Autowired
    UserRepository userRepository;
    @Autowired
    LegalDocumentRepository documentRepository;
    
    
    @RequestMapping(value = "/new", method = RequestMethod.POST )
    public ViewServerResponse newDocument(HttpServletRequest request, @RequestBody NewDocumentForm form, ModelMap model, BindingResult bindingResult) {
        User user = SessionUtils.getLoggedInUser(userRepository, request);
        System.out.println(form);
        
        if( bindingResult.hasErrors() ){
            System.out.println("Form with errors");
        }
        
        ServerResponse.Status status;
        String message;
        String html;
        
        try {
            LegalDocument document = documentService.createNewLegalDocument(user, form);
            status = ServerResponse.Status.SUCCESS;
            message = "A New Document successfully created";
            html = form.toString();
        } catch (Exception ex) {
            System.out.println(ex);
            status = ServerResponse.Status.DANGER;
            message = ex.getMessage();
            html = null;
        }
        
        return ViewServerResponse.get(status, message, html);
    }
    
    
}
