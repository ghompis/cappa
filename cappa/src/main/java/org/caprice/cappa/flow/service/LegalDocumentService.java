/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.caprice.cappa.flow.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.caprice.cappa.config.Definitions;
import org.caprice.cappa.flow.repository.ControversialEntityRepository;
import org.caprice.cappa.flow.repository.DigitalProductRepository;
import org.caprice.cappa.flow.repository.LegalDocumentRepository;
import org.caprice.cappa.flow.repository.MetadataRepository;
import org.caprice.cappa.model.db.User;
import org.caprice.cappa.flow.repository.UserRepository;
import org.caprice.cappa.flow.repository.enums.T_DocumentTypeRepository;
import org.caprice.cappa.flow.repository.enums.T_LanguageRepository;
import org.caprice.cappa.model.beans.DocumentDownloadResult;
import org.caprice.cappa.model.beans.ExceptionMessage;
import org.caprice.cappa.model.db.Annotation;
import org.caprice.cappa.model.db.ControversialEntity;
import org.caprice.cappa.model.db.DigitalProduct;
import org.caprice.cappa.model.db.LegalDocument;
import org.caprice.cappa.model.db.Metadata;
import org.caprice.cappa.model.db.enums.T_DocumentType;
import org.caprice.cappa.model.db.enums.T_Language;
import org.caprice.cappa.model.db.enums.T_UserRole;
import org.caprice.cappa.model.formBeans.NewDocumentForm;
import org.caprice.cappa.model.restBeans.ServerResponse;
import org.caprice.cappa.model.restBeans.ServerResponse.Status;
import org.caprice.cappa.utils.AppUtils;
import org.jsoup.Jsoup;
import org.owasp.html.PolicyFactory;
import org.owasp.html.Sanitizers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Giorgos Hompis <ghompis@csd.uoc.gr>
 */

@Service
public class LegalDocumentService {
    
    
    @Autowired
    UserRepository userRepository;
    @Autowired
    MetadataRepository metadataRepository;
    @Autowired
    T_DocumentTypeRepository documentTypeRepository;
    @Autowired
    T_LanguageRepository languageRepository;
    @Autowired
    ControversialEntityRepository controversialEntityRepository;
    
    
    @Autowired
    LegalDocumentRepository documentRepository;
    
    @Autowired
    DigitalProductRepository productRepository;
    
    
    @Transactional
    public LegalDocument createNewLegalDocument(User user, NewDocumentForm documentForm) throws ExceptionMessage{
        if( user == null )
            throw new ExceptionMessage(Status.WARNING, "You have to log in order to add a New Document");
        
        Set<T_UserRole> userRoles = user.getUserRoles();
        if( !userRoles.contains(Definitions.Defaults.Roles.ADMIN) && !userRoles.contains(Definitions.Defaults.Roles.CONTENT_EDITOR) )
            throw new ExceptionMessage(ServerResponse.Status.WARNING, "You don't have permissions to create a document");
        
        T_Language language = languageRepository.findOne( documentForm.getLanguage() );
        if( language== null)
            throw new ExceptionMessage(Status.DANGER, "Unable to find selected Language");
        
        T_DocumentType documentType = documentTypeRepository.findOne( documentForm.getDocumentType() );
        if( documentType==null )
            throw new ExceptionMessage(Status.DANGER, "Unable to find selected Document Type");
        
        DigitalProduct product = productRepository.findOne( documentForm.getProductId() );
        if( product == null )
            throw new ExceptionMessage(Status.DANGER, "Unable to find product with given product ID");
        
        String htmlText;
        Date lastModified, collectedAt;
        if( documentForm.getHtmlText()==null || documentForm.getHtmlText().isEmpty() ){
            DocumentDownloadResult downloadResult = AppUtils.downloadAndSanitizeHtmlDocument( documentForm.getUrl() );
            htmlText = downloadResult.getDocumentHtml();
            lastModified = new Date( downloadResult.getLastModified() );
            collectedAt = new Date( System.currentTimeMillis() );
            if( htmlText == null )
                throw new ExceptionMessage(Status.WARNING, "Unable to download document. Please check the given URL");
        } else {
            htmlText = AppUtils.sanitizeHtmlDocument( documentForm.getHtmlText() );
            lastModified = documentForm.getLastModified();
            collectedAt = documentForm.getCollectedAt();
        }
        
        // Creation
        LegalDocument document = new LegalDocument();
        ControversialEntity entity = new ControversialEntity();
        Metadata meta = new Metadata();
        
        meta.setCreatedAt( new Timestamp(System.currentTimeMillis()) );
        meta.setCreatedBy( user );
        
        entity.setMetadata(meta);
        entity.setResponses( new ArrayList() );
        entity.setScore(              0.0 );
        entity.setMinConfidenceLimit( -1.0 );
        entity.setMaxConfidenceLimit( 1.0 );
        entity.setState(              Definitions.Defaults.State.UNCLARIFIED);
        
        document.setControversialEntity(    entity);
        document.setDigitalProduct(         product );
        document.setDocumentType(           documentType );
        document.setHtmlText(               htmlText );
        document.setLanguage(               language);
        document.setLastModified(           lastModified );
        document.setCollectedAt(            collectedAt );
        document.setName(                   documentType.getName() + " v1" );
        document.setUrl(                    documentForm.getUrl() );
        document.setVersion(                1 );

        metadataRepository.save( meta );
        controversialEntityRepository.save(entity);
        documentRepository.save( document );
        
        return document;
    }
    
    
    public LegalDocument deleteLegalDocument(User user, LegalDocument d) throws ExceptionMessage {
        if( user == null )
            throw new ExceptionMessage(ServerResponse.Status.WARNING, "You have to log in order to delete a document");
        
        if( !user.getUserRoles().contains( Definitions.Defaults.Roles.ADMIN ) && 
                d.getControversialEntity().getMetadata().getCreatedBy().getId() != user.getId() )
            throw new ExceptionMessage(ServerResponse.Status.WARNING, "You cannot delete a document not created by you");
        
        // Act
        try{
            documentRepository.delete(d);
            //controversialEntityRepository.delete( d.getControversialEntity() );
            //metadataRepository.delete( d.getControversialEntity().getMetadata() );
        } catch(Exception e){
            throw new ExceptionMessage(ServerResponse.Status.WARNING, 
                    "This action could not be completed. Be sure to remove all document's annotations before deleting this document");
        }
        return d;
    }
    
    
    
    public LegalDocument findOne(int id){
        return documentRepository.findOne(id);
    }
    
    public LegalDocument save(LegalDocument d){
        return documentRepository.save(d);
    }
    
}
