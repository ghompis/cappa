/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.caprice.cappa.flow.repository.enums;

import java.util.List;
import org.caprice.cappa.model.db.enums.T_PrivacyConcern;
import org.caprice.cappa.model.db.enums.T_PrivacyConcernAttribute;
import org.caprice.cappa.model.db.enums.T_TagAttribute;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author Giorgos Hompis <ghompis@csd.uoc.gr>
 */
public interface T_PrivacyConcernAttributeRepository extends CrudRepository<T_PrivacyConcernAttribute, Integer> {

    
    @Query("select a from T_PrivacyConcernAttribute a where a.PrivacyConcern = ?1 and a.TagAttribute = ?2")
    public List<T_PrivacyConcernAttribute> findByPrivacyConcernAndTagAttribute(T_PrivacyConcern c, T_TagAttribute t);
    
    
}
