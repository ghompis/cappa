package org.caprice.cappa.model.db;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import org.caprice.cappa.model.db.enums.T_BusinessCategory;
import java.io.Serializable;
import javax.persistence.*;
import java.util.List;
import java.util.SortedSet;
import org.caprice.cappa.model.db.enums.T_DigitalProductType;


/**
 * The persistent class for the e_digital_product database table.
 * 
 */
@Entity
@Table(name="e_digital_product")
@NamedQuery(name="DigitalProduct.findAll", query="SELECT d FROM DigitalProduct d")
@JsonIdentityInfo(generator=ObjectIdGenerators.PropertyGenerator.class, property="id")
public class DigitalProduct implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;

	private String name;
        
	private String url;
        
	@Column(name="logo_image_path")
	private String logoImagePath;

	@Column(name="logo_image_base64")
        private String logoImageData;
        
	//uni-directional many-to-one association to Metadata
	@ManyToOne
	@JoinColumn(name="controversial_entity_id")
	private ControversialEntity ControversialEntity;

	//bi-directional many-to-one association to LegalDocument
	@OneToMany(mappedBy="DigitalProduct", fetch = FetchType.EAGER)
        @OrderBy("id ASC")
	private SortedSet<LegalDocument> LegalDocuments;

	//uni-directional many-to-many association to T_BusinessCategory
	@ManyToMany
	@JoinTable(
		name="r_digital_product_has_t_business_category"
		, joinColumns={
			@JoinColumn(name="digital_product_id")
			}
		, inverseJoinColumns={
			@JoinColumn(name="t_business_category_id")
			}
		)
	private List<T_BusinessCategory> BusinessCategories;
        
	//uni-directional many-to-one association to T_DigitalProductType
	@ManyToOne
	@JoinColumn(name="digital_product_type_id")
        private T_DigitalProductType ProductType;

        
	public DigitalProduct() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public String getLogoImagePath() {
            return logoImagePath;
        }
        
        public void setLogoImagePath(String logoImagePath) {
            this.logoImagePath = logoImagePath;
        }

        public String getLogoImageData() {
            return logoImageData;
        }

        public void setLogoImageData(String logoImageData) {
            this.logoImageData = logoImageData;
        }

        public ControversialEntity getControversialEntity() {
            return ControversialEntity;
        }

        public void setControversialEntity(ControversialEntity ControversialEntity) {
            this.ControversialEntity = ControversialEntity;
        }

	public SortedSet<LegalDocument> getLegalDocuments() {
		return this.LegalDocuments;
	}

	public void setLegalDocuments(SortedSet<LegalDocument> LegalDocuments) {
		this.LegalDocuments = LegalDocuments;
	}

	public LegalDocument addLegalDocument(LegalDocument LegalDocument) {
		getLegalDocuments().add(LegalDocument);
		LegalDocument.setDigitalProduct(this);

		return LegalDocument;
	}

	public LegalDocument removeLegalDocument(LegalDocument LegalDocument) {
		getLegalDocuments().remove(LegalDocument);
		LegalDocument.setDigitalProduct(null);

		return LegalDocument;
	}

	public List<T_BusinessCategory> getBusinessCategories() {
		return this.BusinessCategories;
	}

	public void setBusinessCategories(List<T_BusinessCategory> BusinessCategories) {
		this.BusinessCategories = BusinessCategories;
	}

        public T_DigitalProductType getProductType() {
            return ProductType;
        }

        public void setProductType(T_DigitalProductType ProductType) {
            this.ProductType = ProductType;
        }
        

}