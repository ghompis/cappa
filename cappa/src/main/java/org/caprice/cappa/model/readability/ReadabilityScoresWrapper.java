/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.caprice.cappa.model.readability;

import com.ipeirotis.readability.engine.Readability;
import com.ipeirotis.readability.enums.MetricType;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Giorgos Hompis <ghompis@csd.uoc.gr>
 */
public class ReadabilityScoresWrapper {
    
    private final Map<ReadabilityMetric, Double> metricScores;

    
    public ReadabilityScoresWrapper(String text) {
        Readability r = new Readability(text);
        this.metricScores = new HashMap();
        this.metricScores.put(ReadabilityMetric.ARI,  r.getMetric(MetricType.ARI));
        this.metricScores.put(ReadabilityMetric.CLI,  r.getMetric(MetricType.COLEMAN_LIAU));
        this.metricScores.put(ReadabilityMetric.FKG,  r.getMetric(MetricType.FLESCH_KINCAID));
        this.metricScores.put(ReadabilityMetric.FRES, r.getMetric(MetricType.FLESCH_READING));
        this.metricScores.put(ReadabilityMetric.GFI,  r.getMetric(MetricType.GUNNING_FOG));
        this.metricScores.put(ReadabilityMetric.SMOG, r.getMetric(MetricType.SMOG));
    }

    public Double getScore(ReadabilityMetric metric) {
        return metricScores.get(metric);
    }
    
    public DifficultyLevel getDifficulty(ReadabilityMetric metric) {
        return metric.getDifficultyLevel( metricScores.get(metric) );
    }

    public Map<ReadabilityMetric, Double> getMetricScores() {
        return metricScores;
    }
    
}
