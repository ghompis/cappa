package org.caprice.cappa.model.db.enums;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the t_android_permission database table.
 * 
 */
@Entity
@Table(name="t_android_permission")
@NamedQuery(name="T_AndroidPermission.findAll", query="SELECT t FROM T_AndroidPermission t")
public class T_AndroidPermission implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;

	private String description;

	private String name;

	@Column(name="since_version")
	private String sinceVersion;

	@Column(name="until_version")
	private String untilVersion;

	public T_AndroidPermission() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSinceVersion() {
		return this.sinceVersion;
	}

	public void setSinceVersion(String sinceVersion) {
		this.sinceVersion = sinceVersion;
	}

	public String getUntilVersion() {
		return this.untilVersion;
	}

	public void setUntilVersion(String untilVersion) {
		this.untilVersion = untilVersion;
	}

}