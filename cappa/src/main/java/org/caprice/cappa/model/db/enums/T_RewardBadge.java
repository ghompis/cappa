package org.caprice.cappa.model.db.enums;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the t_reward_badge database table.
 * 
 */
@Entity
@Table(name="t_reward_badge")
@NamedQuery(name="T_RewardBadge.findAll", query="SELECT t FROM T_RewardBadge t")
public class T_RewardBadge implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;

	private String description;

	private String name;

	private String category;
        
	@Column(name="ui_color")
	private String uiColor;
        
	private int scoreValue;

	public T_RewardBadge() {
	}
        
        public T_RewardBadge(String name, String category, int scoreValue, String uiColor) {
            this.name = name;
            this.category = category;
            this.scoreValue = scoreValue;
            this.uiColor = uiColor;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

        public String getCategory() {
            return category;
        }

        public void setCategory(String category) {
            this.category = category;
        }

        public String getUiColor() {
            return uiColor;
        }

        public void setUiColor(String uiColor) {
            this.uiColor = uiColor;
        }
        
	public int getScoreValue() {
		return this.scoreValue;
	}

	public void setScoreValue(int value) {
		this.scoreValue = value;
	}

}