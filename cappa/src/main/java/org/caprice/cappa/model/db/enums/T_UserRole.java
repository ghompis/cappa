package org.caprice.cappa.model.db.enums;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.*;


/**
 * The persistent class for the t_user_role database table.
 * 
 */
@Entity
@Table(name="t_user_role")
@NamedQuery(name="T_UserRole.findAll", query="SELECT t FROM T_UserRole t")
public class T_UserRole implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;

	private String description;

	private String name;

	public T_UserRole() {
	}
        public T_UserRole(String name) {
            this.name = name;
        }

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 37 * hash + this.id;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final T_UserRole other = (T_UserRole) obj;
        if (this.id != other.id) {
            return false;
        }
        if (!Objects.equals(this.description, other.description)) {
            return false;
        }
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        return true;
    }

        
        
        
    @Override
    public String toString() {
        return "T_UserRole{" + "id=" + id + ", description=" + description + ", name=" + name + '}';
    }

        
}