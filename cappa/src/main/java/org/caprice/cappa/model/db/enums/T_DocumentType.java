package org.caprice.cappa.model.db.enums;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the t_document database table.
 * 
 */
@Entity
@Table(name="t_document")
@NamedQuery(name="T_DocumentType.findAll", query="SELECT t FROM T_DocumentType t")
public class T_DocumentType implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;

	private String description;

	private String name;

        
        public T_DocumentType(String name) {
            this.name = name;
        }
        
	public T_DocumentType() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

        @Override
        public String toString() {
            return "T_DocumentType{" + "id=" + id + ", description=" + description + ", name=" + name + '}';
        }
        
}