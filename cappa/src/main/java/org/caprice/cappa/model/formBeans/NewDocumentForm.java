/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.caprice.cappa.model.formBeans;

import java.sql.Date;
import javax.validation.constraints.NotNull;
import org.caprice.cappa.model.db.enums.T_DocumentType;
import org.caprice.cappa.model.db.enums.T_Language;
import org.hibernate.validator.constraints.NotEmpty;

/**
 *
 * @author Giorgos Hompis <ghompis@csd.uoc.gr>
 */
public class NewDocumentForm {
    
    @NotNull
    private Integer productId;
    @NotEmpty
    private Integer language;
    @NotEmpty
    private Integer documentType;
    @NotEmpty
    private String url;
    
    private String htmlText;
    private Date lastModified;
    private Date collectedAt;

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public Integer getLanguage() {
        return language;
    }

    public void setLanguage(Integer language) {
        this.language = language;
    }

    public Integer getDocumentType() {
        return documentType;
    }

    public void setDocumentType(Integer documentType) {
        this.documentType = documentType;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        url = url.toLowerCase();
        this.url = url.startsWith("http")? url : "http://"+url;
    }

    public String getHtmlText() {
        return htmlText;
    }

    public void setHtmlText(String htmlText) {
        this.htmlText = htmlText;
    }

    public Date getLastModified() {
        return lastModified;
    }

    public void setLastModified(Date lastModified) {
        this.lastModified = lastModified;
    }

    public Date getCollectedAt() {
        return collectedAt;
    }

    public void setCollectedAt(Date collectedAt) {
        this.collectedAt = collectedAt;
    }

    
    @Override
    public String toString() {
        return "NewDocumentForm{" + "productId=" + productId + ", language=" + language + ", documentType=" + documentType + ", url=" + url + ", htmlText=" + htmlText + ", lastModified=" + lastModified + ", collectedAt=" + collectedAt + '}';
    }
    
}
