/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.caprice.cappa.model.beans;

/**
 *
 * @author Giorgos Hompis <ghompis@csd.uoc.gr>
 */
public class WilsonScoreIntervalResult {
    private double value;
    private double minLimit;
    private double maxLimit;

    public WilsonScoreIntervalResult(double value, double minLimit, double maxLimit) {
        this.value = value;
        this.minLimit = minLimit;
        this.maxLimit = maxLimit;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public double getMinLimit() {
        return minLimit;
    }

    public void setMinLimit(double minLimit) {
        this.minLimit = minLimit;
    }

    public double getMaxLimit() {
        return maxLimit;
    }

    public void setMaxLimit(double maxLimit) {
        this.maxLimit = maxLimit;
    }
    
    
}
