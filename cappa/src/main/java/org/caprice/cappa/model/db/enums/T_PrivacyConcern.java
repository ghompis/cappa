package org.caprice.cappa.model.db.enums;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the t_privacy_concern database table.
 * 
 */
@Entity
@Table(name="t_privacy_concern")
@NamedQuery(name="T_PrivacyConcern.findAll", query="SELECT t FROM T_PrivacyConcern t")
public class T_PrivacyConcern implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;

	private String description;

	private String name;
        
        @Column(name="ui_color")
        private String uiColor;
        
        @Column(name="ui_light_color")
        private String uiLightColor;

	//uni-directional many-to-one association to T_PrivacyConcern
	@ManyToOne
	@JoinColumn(name="parent_id")
	private T_PrivacyConcern ParentPrivacyConcern;

	public T_PrivacyConcern() {
	}
        
        public T_PrivacyConcern(String name, String color, String lightColor, String description) {
            this.name = name;
            this.uiColor = color;
            this.uiLightColor = lightColor;
            this.description = description;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

        public String getUiColor() {
            return uiColor;
        }

        public void setUiColor(String uiColor) {
            this.uiColor = uiColor;
        }

        public String getUiLightColor() {
            return uiLightColor;
        }

        public void setUiLightColor(String uiLightColor) {
            this.uiLightColor = uiLightColor;
        }
        
	public T_PrivacyConcern getParentPrivacyConcern() {
		return this.ParentPrivacyConcern;
	}

	public void setParentPrivacyConcern(T_PrivacyConcern ParentPrivacyConcern) {
		this.ParentPrivacyConcern = ParentPrivacyConcern;
	}

    @Override
    public String toString() {
        return "T_PrivacyConcern{" + "id=" + id + ", description=" + description + ", name=" + name + ", ParentPrivacyConcern=" + ParentPrivacyConcern + '}';
    }

        
}