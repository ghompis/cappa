package org.caprice.cappa.model.db.enums;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The persistent class for the T_Language database table.
 *
 */
@Entity
@Table(name="t_language")
@NamedQuery(name = "T_Language.findAll", query = "SELECT l FROM T_Language l")
public class T_Language implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    private String description;

    private String name;

    @Column(name="flag_iso3166_2_code")
    private String flagIsoCode;

    public T_Language() {
    }
    
    public T_Language(String name) {
        this.name = name;
    }
    
    public T_Language(String name, String flagIsoCode) {
        this.name = name;
        this.flagIsoCode = flagIsoCode;
    }
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFlagIsoCode() {
        return flagIsoCode;
    }

    public void setFlagIsoCode(String flagIsoCode) {
        this.flagIsoCode = flagIsoCode;
    }

    

}
