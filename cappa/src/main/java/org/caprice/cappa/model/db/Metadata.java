package org.caprice.cappa.model.db;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the e_metadata database table.
 * 
 */
@Entity
@Table(name="e_metadata")
@NamedQuery(name="Metadata.findAll", query="SELECT m FROM Metadata m")
public class Metadata implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;

	@Column(name="created_at")
	private Timestamp createdAt;

	//uni-directional many-to-one association to User
	@ManyToOne
	@JoinColumn(name="created_by_id")
	private User CreatedBy;

	public Metadata() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Timestamp getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public User getCreatedBy() {
		return this.CreatedBy;
	}

	public void setCreatedBy(User CreatedBy) {
		this.CreatedBy = CreatedBy;
	}

}