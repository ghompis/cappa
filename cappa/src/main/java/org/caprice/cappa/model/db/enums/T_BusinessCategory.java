package org.caprice.cappa.model.db.enums;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the t_business_category database table.
 * 
 */
@Entity
@Table(name="t_business_category")
@NamedQuery(name="T_BusinessCategory.findAll", query="SELECT t FROM T_BusinessCategory t")
public class T_BusinessCategory implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;

	private String description;

	private String name;

	//uni-directional many-to-one association to T_BusinessCategory
	@ManyToOne
	@JoinColumn(name="parent_id")
	private T_BusinessCategory ParentBusinessCategory;

	public T_BusinessCategory() {
	}
        
        public T_BusinessCategory(String name, T_BusinessCategory parent) {
            this.name = name;
            this.ParentBusinessCategory = parent;
        }
        
        public T_BusinessCategory(String name) {
            this.name = name;
            this.ParentBusinessCategory = null;
        }

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public T_BusinessCategory getParentBusinessCategory() {
		return this.ParentBusinessCategory;
	}

	public void setParentBusinessCategory(T_BusinessCategory ParentBusinessCategory) {
		this.ParentBusinessCategory = ParentBusinessCategory;
	}

    @Override
    public String toString() {
        return "T_BusinessCategory{" + "id=" + id + ", description=" + description + ", name=" + name + ", ParentBusinessCategory=" + ParentBusinessCategory + '}';
    }

        
        
}