package org.caprice.cappa.model.db.enums;

import java.io.Serializable;
import javax.persistence.*;
import org.caprice.cappa.utils.converters.Converters;


/**
 * The persistent class for the t_digital_product_type database table.
 * 
 */
@Entity
@Table(name="t_digital_product_type")
@NamedQuery(name="T_DigitalProductType.findAll", query="SELECT t FROM T_DigitalProductType t")
public class T_DigitalProductType implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;

	private String description;

	private String name;
        
	//uni-directional many-to-one association to T_PrivacyConcern
	@ManyToOne
	@JoinColumn(name="parent_id")
	private T_DigitalProductType ParentProductType;

	public T_DigitalProductType() {
	}
        
        public T_DigitalProductType(String name) {
            this.name = name;
	}
        public T_DigitalProductType(String name, T_DigitalProductType parent) {
            this.name = name;
            this.ParentProductType = parent;
	}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public T_DigitalProductType getParentProductType() {
        return ParentProductType;
    }

    public void setParentProductType(T_DigitalProductType ParentProductType) {
        this.ParentProductType = ParentProductType;
    }
    
    @Override
    public String toString() {
        return "T_DigitalProductType{" + "id=" + id + ", description=" + description + ", name=" + name + ", ParentProductType=" + ParentProductType + '}';
    }
        
}