/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.caprice.cappa.model.beans;

import org.caprice.cappa.model.restBeans.DataServerResponse;
import org.caprice.cappa.model.restBeans.ServerResponse;
import org.caprice.cappa.model.restBeans.ViewServerResponse;

/**
 *
 * @author Giorgos Hompis <ghompis@csd.uoc.gr>
 */
public class ExceptionMessage extends Exception {
    
    private final ServerResponse.Status exceptionStatus;

    public ExceptionMessage(ServerResponse.Status exceptionStatus, String message) {
        super(message);
        this.exceptionStatus = exceptionStatus;
    }

    public ServerResponse.Status getExceptionStatus() {
        return exceptionStatus;
    }
    
    public ViewServerResponse getViewResponse(){
        return ViewServerResponse.get(exceptionStatus, this.getMessage(), null);
    }
    public DataServerResponse getDataResponse(){
        return DataServerResponse.get(exceptionStatus, this.getMessage(), null);
    }
}
