package org.caprice.cappa.model.db;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;
import org.caprice.cappa.model.db.enums.T_EntityState;
import org.caprice.cappa.model.db.views.ControversialEntityVotes;


/**
 * The persistent class for the e_controversial_entity database table.
 * 
 */
@Entity
@Table(name="e_controversial_entity")
@NamedQuery(name="ControversialEntity.findAll", query="SELECT c FROM ControversialEntity c")
public class ControversialEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;

	//uni-directional many-to-one association to Metadata
	@ManyToOne
	private Metadata Metadata;
        
        @OneToOne
        @JoinColumn(name = "id", insertable = false, updatable = false)
        private ControversialEntityVotes votes;
        
        private double score;
        
	@Column(name="score_min_confidence_limit")
        private double minConfidenceLimit;
        
	@Column(name="score_max_confidence_limit")
        private double maxConfidenceLimit;
        
	//uni-directional many-to-one association to T_EntityState
	@ManyToOne
	@JoinColumn(name="entity_state_id")
        private T_EntityState state;
        
	//bi-directional many-to-one association to Response
	@OneToMany(mappedBy="ControversialEntity")
	private List<Response> Responses;

	public ControversialEntity() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Metadata getMetadata() {
		return this.Metadata;
	}

	public void setMetadata(Metadata Metadata) {
		this.Metadata = Metadata;
	}
        
        public ControversialEntityVotes getVotes() {
            return votes;
        }

        public void setVotes(ControversialEntityVotes votes) {
            this.votes = votes;
        }

        public double getScore() {
            return score;
        }

        public void setScore(double score) {
            this.score = score;
        }

        public double getMinConfidenceLimit() {
            return minConfidenceLimit;
        }

        public void setMinConfidenceLimit(double minConfidenceLimit) {
            this.minConfidenceLimit = minConfidenceLimit;
        }

        public double getMaxConfidenceLimit() {
            return maxConfidenceLimit;
        }

        public void setMaxConfidenceLimit(double maxConfidenceLimit) {
            this.maxConfidenceLimit = maxConfidenceLimit;
        }

        public T_EntityState getState() {
            return state;
        }

        public void setState(T_EntityState state) {
            this.state = state;
        }


        
	public List<Response> getResponses() {
		return this.Responses;
	}

	public void setResponses(List<Response> Responses) {
		this.Responses = Responses;
	}

	public Response addRespons(Response Respons) {
		getResponses().add(Respons);
		Respons.setControversialEntity(this);

		return Respons;
	}

	public Response removeRespons(Response Respons) {
		getResponses().remove(Respons);
		Respons.setControversialEntity(null);

		return Respons;
	}

        @Override
        public String toString() {
            return "ControversialEntity{" + "id=" + id + ", Metadata=" + Metadata + ", votes=" + votes + ", Responses=" + Responses + '}';
        }
    
}