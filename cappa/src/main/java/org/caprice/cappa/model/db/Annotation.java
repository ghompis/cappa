package org.caprice.cappa.model.db;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import org.caprice.cappa.model.db.enums.T_Tag;
import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the e_annotation database table.
 * 
 */
@Entity
@Table(name="e_annotation")
@NamedQuery(name="Annotation.findAll", query="SELECT a FROM Annotation a")
@JsonIdentityInfo(generator=ObjectIdGenerators.PropertyGenerator.class, property="id")
public class Annotation implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;

	private String text;
        
	//uni-directional many-to-one association to ControversialEntity
	@ManyToOne
	@JoinColumn(name="controversial_entity_id")
	private ControversialEntity ControversialEntity;
        
	@ManyToOne
	@JoinColumn(name="legal_document_id")
	private LegalDocument LegalDocument;
        
	//bi-directional many-to-one association to TextPart
	@OneToMany(mappedBy="Annotation", cascade = {CascadeType.ALL} )
	private List<TextPart> TextParts;

	//uni-directional many-to-many association to T_Tag
	@ManyToMany
	@JoinTable(
		name="r_annotation_has_t_tag"
		, joinColumns={
			@JoinColumn(name="annotation_id")
			}
		, inverseJoinColumns={
			@JoinColumn(name="t_tag_id")
			}
		)
	private List<T_Tag> Tags;

	public Annotation() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getText() {
		return this.text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public ControversialEntity getControversialEntity() {
		return this.ControversialEntity;
	}

	public void setControversialEntity(ControversialEntity ControversialEntity) {
		this.ControversialEntity = ControversialEntity;
	}
        
        public LegalDocument getLegalDocument() {
            return LegalDocument;
        }

        public void setLegalDocument(LegalDocument LegalDocument) {
            this.LegalDocument = LegalDocument;
        }
        
	public List<TextPart> getTextParts() {
		return this.TextParts;
	}

	public void setTextParts(List<TextPart> TextParts) {
		this.TextParts = TextParts;
	}

	public TextPart addTextPart(TextPart TextPart) {
		getTextParts().add(TextPart);
		TextPart.setAnnotation(this);

		return TextPart;
	}

	public TextPart removeTextPart(TextPart TextPart) {
		getTextParts().remove(TextPart);
		TextPart.setAnnotation(null);

		return TextPart;
	}

	public List<T_Tag> getTags() {
		return this.Tags;
	}

	public void setTags(List<T_Tag> Tags) {
		this.Tags = Tags;
	}

        @Override
        public String toString() {
            return "Annotation{" + "id=" + id + '}';
        }



}