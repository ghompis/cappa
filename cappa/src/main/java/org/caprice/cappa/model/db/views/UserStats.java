package org.caprice.cappa.model.db.views;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;

/**
 * The persistent class for the e_controversial_entity database table.
 *
 */
@Entity
@Table(name = "v_user_stats")
@NamedQuery(name = "UserStats.findAll", query = "SELECT us FROM UserStats us")
public class UserStats implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id", insertable = false, updatable = false)
    private int id;

    @Column(name = "output_upvotes_count", insertable = false, updatable = false)
    private int outputUpvotesCount;

    @Column(name = "output_downvotes_count", insertable = false, updatable = false)
    private int outputDownvotesCount;

    @Column(name = "annotation_output_upvotes_count", insertable = false, updatable = false)
    private int annotationOutputUpvotesCount;

    @Column(name = "annotation_output_downvotes_count", insertable = false, updatable = false)
    private int annotationOutputDownvotesCount;

    @Column(name = "legal_document_output_upvotes_count", insertable = false, updatable = false)
    private int legalDocumentOutputUpvotesCount;

    @Column(name = "legal_document_output_downvotes_count", insertable = false, updatable = false)
    private int legalDocumentOutputDownvotesCount;

    @Column(name = "digital_product_output_upvotes_count", insertable = false, updatable = false)
    private int digitalProductOutputUpvotesCount;

    @Column(name = "digital_product_output_downvotes_count", insertable = false, updatable = false)
    private int digitalProductOutputDownvotesCount;

    @Column(name = "input_upvotes_count", insertable = false, updatable = false)
    private int inputUpvotesCount;

    @Column(name = "input_downvotes_count", insertable = false, updatable = false)
    private int inputDownvotesCount;

    @Column(name = "annotation_input_upvotes_count", insertable = false, updatable = false)
    private int annotationInputUpvotesCount;

    @Column(name = "annotation_input_downvotes_count", insertable = false, updatable = false)
    private int annotationInputDownvotesCount;

    @Column(name = "legal_document_input_upvotes_count", insertable = false, updatable = false)
    private int legalDocumentInputUpvotesCount;

    @Column(name = "legal_document_input_downvotes_count", insertable = false, updatable = false)
    private int legalDocumentInputDownvotesCount;

    @Column(name = "digital_product_input_upvotes_count", insertable = false, updatable = false)
    private int digitalProductInputUpvotesCount;

    @Column(name = "digital_product_input_downvotes_count", insertable = false, updatable = false)
    private int digitalProductInputDownvotesCount;

    @Column(name = "validated_count", insertable = false, updatable = false)
    private int validatedCount;

    @Column(name = "invalidated_count", insertable = false, updatable = false)
    private int invalidatedCount;

    @Column(name = "annotation_validated_count", insertable = false, updatable = false)
    private int annotationValidatedCount;

    @Column(name = "annotation_invalidated_count", insertable = false, updatable = false)
    private int annotationInvalidatedCount;

    @Column(name = "legal_document_validated_count", insertable = false, updatable = false)
    private int legalDocumentValidatedCount;

    @Column(name = "legal_document_invalidated_count", insertable = false, updatable = false)
    private int legalDocumentInvalidatedCount;

    @Column(name = "digital_product_validated_count", insertable = false, updatable = false)
    private int digitalProductValidatedCount;

    @Column(name = "digital_product_invalidated_count", insertable = false, updatable = false)
    private int digitalProductInvalidatedCount;

    @Column(name = "metadata_created", insertable = false, updatable = false)
    private int metadataCreated;

    @Column(name = "annotations_created", insertable = false, updatable = false)
    private int annotationsCreated;

    @Column(name = "legal_documents_added", insertable = false, updatable = false)
    private int legalDocumentsAdded;

    @Column(name = "digital_products_added", insertable = false, updatable = false)
    private int digitalProductsAdded;

    
    public UserStats() {
    }
    

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getOutputUpvotesCount() {
        return outputUpvotesCount;
    }

    public void setOutputUpvotesCount(int outputUpvotesCount) {
        this.outputUpvotesCount = outputUpvotesCount;
    }

    public int getOutputDownvotesCount() {
        return outputDownvotesCount;
    }

    public void setOutputDownvotesCount(int outputDownvotesCount) {
        this.outputDownvotesCount = outputDownvotesCount;
    }

    public int getAnnotationOutputUpvotesCount() {
        return annotationOutputUpvotesCount;
    }

    public void setAnnotationOutputUpvotesCount(int annotationOutputUpvotesCount) {
        this.annotationOutputUpvotesCount = annotationOutputUpvotesCount;
    }

    public int getAnnotationOutputDownvotesCount() {
        return annotationOutputDownvotesCount;
    }

    public void setAnnotationOutputDownvotesCount(int annotationOutputDownvotesCount) {
        this.annotationOutputDownvotesCount = annotationOutputDownvotesCount;
    }

    public int getLegalDocumentOutputUpvotesCount() {
        return legalDocumentOutputUpvotesCount;
    }

    public void setLegalDocumentOutputUpvotesCount(int legalDocumentOutputUpvotesCount) {
        this.legalDocumentOutputUpvotesCount = legalDocumentOutputUpvotesCount;
    }

    public int getLegalDocumentOutputDownvotesCount() {
        return legalDocumentOutputDownvotesCount;
    }

    public void setLegalDocumentOutputDownvotesCount(int legalDocumentOutputDownvotesCount) {
        this.legalDocumentOutputDownvotesCount = legalDocumentOutputDownvotesCount;
    }

    public int getDigitalProductOutputUpvotesCount() {
        return digitalProductOutputUpvotesCount;
    }

    public void setDigitalProductOutputUpvotesCount(int digitalProductOutputUpvotesCount) {
        this.digitalProductOutputUpvotesCount = digitalProductOutputUpvotesCount;
    }

    public int getDigitalProductOutputDownvotesCount() {
        return digitalProductOutputDownvotesCount;
    }

    public void setDigitalProductOutputDownvotesCount(int digitalProductOutputDownvotesCount) {
        this.digitalProductOutputDownvotesCount = digitalProductOutputDownvotesCount;
    }

    public int getInputUpvotesCount() {
        return inputUpvotesCount;
    }

    public void setInputUpvotesCount(int inputUpvotesCount) {
        this.inputUpvotesCount = inputUpvotesCount;
    }

    public int getInputDownvotesCount() {
        return inputDownvotesCount;
    }

    public void setInputDownvotesCount(int inputDownvotesCount) {
        this.inputDownvotesCount = inputDownvotesCount;
    }

    public int getAnnotationInputUpvotesCount() {
        return annotationInputUpvotesCount;
    }

    public void setAnnotationInputUpvotesCount(int annotationInputUpvotesCount) {
        this.annotationInputUpvotesCount = annotationInputUpvotesCount;
    }

    public int getAnnotationInputDownvotesCount() {
        return annotationInputDownvotesCount;
    }

    public void setAnnotationInputDownvotesCount(int annotationInputDownvotesCount) {
        this.annotationInputDownvotesCount = annotationInputDownvotesCount;
    }

    public int getLegalDocumentInputUpvotesCount() {
        return legalDocumentInputUpvotesCount;
    }

    public void setLegalDocumentInputUpvotesCount(int legalDocumentInputUpvotesCount) {
        this.legalDocumentInputUpvotesCount = legalDocumentInputUpvotesCount;
    }

    public int getLegalDocumentInputDownvotesCount() {
        return legalDocumentInputDownvotesCount;
    }

    public void setLegalDocumentInputDownvotesCount(int legalDocumentInputDownvotesCount) {
        this.legalDocumentInputDownvotesCount = legalDocumentInputDownvotesCount;
    }

    public int getDigitalProductInputUpvotesCount() {
        return digitalProductInputUpvotesCount;
    }

    public void setDigitalProductInputUpvotesCount(int digitalProductInputUpvotesCount) {
        this.digitalProductInputUpvotesCount = digitalProductInputUpvotesCount;
    }

    public int getDigitalProductInputDownvotesCount() {
        return digitalProductInputDownvotesCount;
    }

    public void setDigitalProductInputDownvotesCount(int digitalProductInputDownvotesCount) {
        this.digitalProductInputDownvotesCount = digitalProductInputDownvotesCount;
    }

    public int getValidatedCount() {
        return validatedCount;
    }

    public void setValidatedCount(int validatedCount) {
        this.validatedCount = validatedCount;
    }

    public int getInvalidatedCount() {
        return invalidatedCount;
    }

    public void setInvalidatedCount(int invalidatedCount) {
        this.invalidatedCount = invalidatedCount;
    }

    public int getAnnotationValidatedCount() {
        return annotationValidatedCount;
    }

    public void setAnnotationValidatedCount(int annotationValidatedCount) {
        this.annotationValidatedCount = annotationValidatedCount;
    }

    public int getAnnotationInvalidatedCount() {
        return annotationInvalidatedCount;
    }

    public void setAnnotationInvalidatedCount(int annotationInvalidatedCount) {
        this.annotationInvalidatedCount = annotationInvalidatedCount;
    }

    public int getLegalDocumentValidatedCount() {
        return legalDocumentValidatedCount;
    }

    public void setLegalDocumentValidatedCount(int legalDocumentValidatedCount) {
        this.legalDocumentValidatedCount = legalDocumentValidatedCount;
    }

    public int getLegalDocumentInvalidatedCount() {
        return legalDocumentInvalidatedCount;
    }

    public void setLegalDocumentInvalidatedCount(int legalDocumentInvalidatedCount) {
        this.legalDocumentInvalidatedCount = legalDocumentInvalidatedCount;
    }

    public int getDigitalProductValidatedCount() {
        return digitalProductValidatedCount;
    }

    public void setDigitalProductValidatedCount(int digitalProductValidatedCount) {
        this.digitalProductValidatedCount = digitalProductValidatedCount;
    }

    public int getDigitalProductInvalidatedCount() {
        return digitalProductInvalidatedCount;
    }

    public void setDigitalProductInvalidatedCount(int digitalProductInvalidatedCount) {
        this.digitalProductInvalidatedCount = digitalProductInvalidatedCount;
    }

    public int getMetadataCreated() {
        return metadataCreated;
    }

    public void setMetadataCreated(int metadataCreated) {
        this.metadataCreated = metadataCreated;
    }

    public int getAnnotationsCreated() {
        return annotationsCreated;
    }

    public void setAnnotationsCreated(int annotationsCreated) {
        this.annotationsCreated = annotationsCreated;
    }

    public int getLegalDocumentsAdded() {
        return legalDocumentsAdded;
    }

    public void setLegalDocumentsAdded(int legalDocumentsAdded) {
        this.legalDocumentsAdded = legalDocumentsAdded;
    }

    public int getDigitalProductsAdded() {
        return digitalProductsAdded;
    }

    public void setDigitalProductsAdded(int digitalProductsAdded) {
        this.digitalProductsAdded = digitalProductsAdded;
    }
    
    
    
}
