/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.caprice.cappa.model.formBeans;

import java.sql.Timestamp;
import javax.persistence.Column;
import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import org.hibernate.validator.constraints.NotEmpty;

/**
 *
 * @author Giorgos Hompis <ghompis@csd.uoc.gr>
 */
public class SignupForm {
    
    @NotEmpty
    private String email;
    @NotEmpty
    private String username;
    
    @NotEmpty
//    @Size(min=8)
//    @Pattern(regexp = "(?=^.{8,}$)((?=.*\\d)|(?=.*\\W+))(?![.\\n])(?=.*[A-Z])(?=.*[a-z]).*$")
    private String password;
    @NotEmpty
    private String retypePassword;
    
    
    @Min(18)
    @Max(99)
    private Integer age;
    
    private String educationLevel;
    
    private String profession;
    
    private String details;
    
    @AssertTrue
    private boolean acceptTerms;

    
    public SignupForm() {
    }
    
    
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRetypePassword() {
        return retypePassword;
    }

    public void setRetypePassword(String retypePassword) {
        this.retypePassword = retypePassword;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getEducationLevel() {
        return educationLevel;
    }

    public void setEducationLevel(String educationLevel) {
        this.educationLevel = educationLevel;
    }

    public String getProfession() {
        return profession;
    }

    public void setProfession(String profession) {
        this.profession = profession;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public boolean isAcceptTerms() {
        return acceptTerms;
    }

    public void setAcceptTerms(boolean acceptTerms) {
        this.acceptTerms = acceptTerms;
    }
    
    
    @Override
    public String toString() {
        return "SignupForm{" + "email=" + email + 
                        "\n username=" + username + 
                        "\n password=" + password + 
                        "\n retypePassword=" + retypePassword + 
                        "\n age=" + age + 
                        "\n educationLevel=" + educationLevel + 
                        "\n profession=" + profession + 
                        "\n details=" + details + 
                        "\n acceptTerms=" + acceptTerms + '}';
    }

    
}
