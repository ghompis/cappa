package org.caprice.cappa.model.db.enums;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the t_reaction database table.
 * 
 */
@Entity
@NamedQuery(name="T_Reaction.findAll", query="SELECT t FROM T_Reaction t")
public class T_Reaction implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;

	private String description;

	private String domain;

	private String name;

	public T_Reaction() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDomain() {
		return this.domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

}