package org.caprice.cappa.model.db.enums;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the t_tag_attribute database table.
 * 
 */
@Entity
@Table(name="t_tag_attribute")
@NamedQuery(name="T_TagAttribute.findAll", query="SELECT t FROM T_TagAttribute t")
public class T_TagAttribute implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;

	private String description;

	private String name;


	public T_TagAttribute() {
	}
        
        public T_TagAttribute(String name, String description) {
            this.name = name;
            this.description = description;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

}