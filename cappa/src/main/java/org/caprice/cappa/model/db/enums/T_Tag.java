package org.caprice.cappa.model.db.enums;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the t_tag database table.
 * 
 */
@Entity
@NamedQuery(name="T_Tag.findAll", query="SELECT t FROM T_Tag t")
public class T_Tag implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;

	private String description;

	private String name;
        
	//uni-directional many-to-one association to T_PrivacyConcern
	@ManyToOne
	@JoinColumn(name="t_privacy_concern_attribute_id")
	private T_PrivacyConcernAttribute PrivacyConcernAttribute;
        
        public T_Tag(String name, T_PrivacyConcernAttribute PrivacyConcernAttribute, String description) {
            this.name = name;
            this.PrivacyConcernAttribute = PrivacyConcernAttribute;
            this.description = description;
        }
        
	public T_Tag() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

        public T_PrivacyConcernAttribute getPrivacyConcernAttribute() {
            return PrivacyConcernAttribute;
        }

        public void setPrivacyConcernAttribute(T_PrivacyConcernAttribute PrivacyConcernAttribute) {
            this.PrivacyConcernAttribute = PrivacyConcernAttribute;
        }
        
        // Xtra getters
	public T_PrivacyConcern getPrivacyConcern() {
		return this.PrivacyConcernAttribute.getPrivacyConcern();
	}
        public T_TagAttribute getTagAttribute() {
            return this.PrivacyConcernAttribute.getTagAttribute();
        }

}