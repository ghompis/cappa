package org.caprice.cappa.model.db;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import org.caprice.cappa.model.db.enums.T_DocumentType;
import java.io.Serializable;
import java.sql.Date;
import java.util.List;
import javax.persistence.*;
import org.caprice.cappa.model.db.enums.T_Language;


/**
 * The persistent class for the e_legal_document database table.
 * 
 */
@Entity
@Table(name="e_legal_document")
@NamedQuery(name="LegalDocument.findAll", query="SELECT l FROM LegalDocument l")
@JsonIdentityInfo(generator=ObjectIdGenerators.PropertyGenerator.class, property="id")
public class LegalDocument implements Serializable, Comparable<LegalDocument> {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;

	@Lob
	@Column(name="text_data")
	private String htmlText;

	private String name;

	@Column(name="last_modified_header")
	private Date lastModified;

	@Column(name="collected_at")
	private Date collectedAt;
        
	private String url;

	@Column(name="internal_version")
	private Integer version;
        
        
	@OneToMany(mappedBy="LegalDocument", fetch = FetchType.EAGER)
	private List<Annotation> Annotations;
        

	//bi-directional many-to-one association to DigitalProduct
	@ManyToOne
	@JoinColumn(name="belongs_to_id")
	private DigitalProduct DigitalProduct;

	//uni-directional many-to-one association to Metadata
	@ManyToOne
	@JoinColumn(name="controversial_entity_id")
	private ControversialEntity ControversialEntity;

	//uni-directional many-to-one association to T_DocumentType
	@ManyToOne
	@JoinColumn(name="document_type_id")
	private T_DocumentType DocumentType;

        //uni-directional many-to-one association to T_DocumentType
	@ManyToOne
	@JoinColumn(name="language_id")
	private T_Language Language;
        
	public LegalDocument() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

        public String getHtmlText() {
            return htmlText;
        }

        public void setHtmlText(String htmlText) {
            this.htmlText = htmlText;
        }

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

        public Date getLastModified() {
            return lastModified;
        }

        public void setLastModified(Date lastModified) {
            this.lastModified = lastModified;
        }

        public Date getCollectedAt() {
            return collectedAt;
        }

        public void setCollectedAt(Date collectedAt) {
            this.collectedAt = collectedAt;
        }

	public String getUrl() {
		return this.url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Integer getVersion() {
		return this.version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}

        public List<Annotation> getAnnotations() {
            return Annotations;
        }

        public void setAnnotations(List<Annotation> Annotations) {
            this.Annotations = Annotations;
        }
        
	public DigitalProduct getDigitalProduct() {
		return this.DigitalProduct;
	}

	public void setDigitalProduct(DigitalProduct DigitalProduct) {
		this.DigitalProduct = DigitalProduct;
	}

        public ControversialEntity getControversialEntity() {
            return ControversialEntity;
        }

        public void setControversialEntity(ControversialEntity ControversialEntity) {
            this.ControversialEntity = ControversialEntity;
        }

	public T_DocumentType getDocumentType() {
		return this.DocumentType;
	}

	public void setDocumentType(T_DocumentType DocumentType) {
		this.DocumentType = DocumentType;
	}

    public T_Language getLanguage() {
        return Language;
    }

    public void setLanguage(T_Language Language) {
        this.Language = Language;
    }

        

    @Override
    public int compareTo(LegalDocument o) {
        return this.id - o.id;
    }

}