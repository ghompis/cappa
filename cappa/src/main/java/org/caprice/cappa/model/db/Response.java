package org.caprice.cappa.model.db;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the e_response database table.
 * 
 */
@Entity
@Table(name="e_response")
@NamedQuery(name="Response.findAll", query="SELECT r FROM Response r")
public class Response implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;

	private String text;

	//bi-directional many-to-one association to ControversialEntity
	@ManyToOne
	@JoinColumn(name="is_about_id")
	private ControversialEntity ControversialEntity;

	//uni-directional many-to-one association to Metadata
	@ManyToOne
	private Metadata Metadata;

	//bi-directional many-to-one association to Response
	@ManyToOne
	@JoinColumn(name="parent_id")
	private Response ParentResponse;

	//bi-directional many-to-one association to Response
	@OneToMany(mappedBy="ParentResponse")
	private List<Response> Responses;

	public Response() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getText() {
		return this.text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public ControversialEntity getControversialEntity() {
		return this.ControversialEntity;
	}

	public void setControversialEntity(ControversialEntity ControversialEntity) {
		this.ControversialEntity = ControversialEntity;
	}

	public Metadata getMetadata() {
		return this.Metadata;
	}

	public void setMetadata(Metadata Metadata) {
		this.Metadata = Metadata;
	}

	public Response getParentResponse() {
		return this.ParentResponse;
	}

	public void setParentResponse(Response ParentResponse) {
		this.ParentResponse = ParentResponse;
	}

	public List<Response> getResponses() {
		return this.Responses;
	}

	public void setResponses(List<Response> Responses) {
		this.Responses = Responses;
	}

	public Response addRespons(Response Respons) {
		getResponses().add(Respons);
		Respons.setParentResponse(this);

		return Respons;
	}

	public Response removeRespons(Response Respons) {
		getResponses().remove(Respons);
		Respons.setParentResponse(null);

		return Respons;
	}

}