/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.caprice.cappa.model.beans;

/**
 *
 * @author Giorgos Hompis <ghompis@csd.uoc.gr>
 */
public class DocumentDownloadResult {
    private String documentHtml;
    private Long lastModified;

    public DocumentDownloadResult(String documentHtml, Long lastModified) {
        this.documentHtml = documentHtml;
        this.lastModified = lastModified;
    }

    public String getDocumentHtml() {
        return documentHtml;
    }

    public void setDocumentHtml(String documentHtml) {
        this.documentHtml = documentHtml;
    }

    public Long getLastModified() {
        return lastModified;
    }

    public void setLastModified(Long lastModified) {
        this.lastModified = lastModified;
    }

    @Override
    public String toString() {
        return "DocumentDownloadResult{" + "documentHtml=" + documentHtml + ", lastModified=" + lastModified + '}';
    }
    
}
