package org.caprice.cappa.model.db;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the e_external_resource database table.
 * 
 */
@Entity
@Table(name="e_external_resource")
@NamedQuery(name="ExternalResource.findAll", query="SELECT e FROM ExternalResource e")
public class ExternalResource implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;

	private String url;

	//uni-directional many-to-one association to ControversialEntity
	@ManyToOne
	@JoinColumn(name="controversial_entity_id")
	private ControversialEntity ControversialEntity;

	public ExternalResource() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUrl() {
		return this.url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public ControversialEntity getControversialEntity() {
		return this.ControversialEntity;
	}

	public void setControversialEntity(ControversialEntity ControversialEntity) {
		this.ControversialEntity = ControversialEntity;
	}

}