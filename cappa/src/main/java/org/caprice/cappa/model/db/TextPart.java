package org.caprice.cappa.model.db;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the e_text_part database table.
 * 
 */
@Entity
@Table(name="e_text_part")
@NamedQuery(name="TextPart.findAll", query="SELECT t FROM TextPart t")
@JsonIdentityInfo(generator=ObjectIdGenerators.PropertyGenerator.class, property="id")
public class TextPart implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;

	private String highlightsData;

	//bi-directional many-to-one association to Annotation
	@ManyToOne
	private Annotation Annotation;

	public TextPart() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getHighlightsData() {
		return this.highlightsData;
	}

	public void setHighlightsData(String data) {
		this.highlightsData = data;
	}

	public Annotation getAnnotation() {
		return this.Annotation;
	}

	public void setAnnotation(Annotation Annotation) {
		this.Annotation = Annotation;
	}

    @Override
    public String toString() {
        return "TextPart{" + "id=" + id + ", highlightsData=" + highlightsData + ", Annotation=" + Annotation + '}';
    }

        
}