/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.caprice.cappa.model.restBeans;

/**
 *
 * @author Giorgos Hompis <ghompis@csd.uoc.gr>
 * @param <T>
 */
public class DataServerResponse<T> extends ServerResponse {
    
    private T data;

    private DataServerResponse(String status, String message, T data) {
        super(status, message);
        this.data = data;
    }
    
    public static <T> DataServerResponse<T> get(Status status, String message, T data){
        return new DataServerResponse(status.name().toLowerCase(), message, data);
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
    
    
}
