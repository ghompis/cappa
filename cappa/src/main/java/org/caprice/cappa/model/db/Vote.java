package org.caprice.cappa.model.db;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the e_user database table.
 * 
 */
@Entity
@Table(name="e_vote")
@NamedQuery(name="Vote.findAll", query="SELECT v FROM Vote v")
public class Vote implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;

        @Column(name = "vote_value")
        private int voteValue;
        
	//uni-directional many-to-one association to ControversialEntity
	@ManyToOne
	@JoinColumn(name="controversial_entity_id")
	private ControversialEntity ControversialEntity;
        
	//uni-directional many-to-one association to Metadata
	@ManyToOne
	@JoinColumn(name="metadata_id")
	private Metadata Metadata;
        
        
	public Vote() {
	}

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getVoteValue() {
            return voteValue;
        }

        public void setVoteValue(int voteValue) {
            this.voteValue = voteValue;
        }

        public ControversialEntity getControversialEntity() {
            return ControversialEntity;
        }

        public void setControversialEntity(ControversialEntity ControversialEntity) {
            this.ControversialEntity = ControversialEntity;
        }

        public Metadata getMetadata() {
            return Metadata;
        }

        public void setMetadata(Metadata Metadata) {
            this.Metadata = Metadata;
        }

        @Override
        public String toString() {
            return "Vote{" + "id=" + id + ", voteValue=" + voteValue + ", ControversialEntity=" + ControversialEntity + ", Metadata=" + Metadata + '}';
        }
        
}