/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.caprice.cappa.model.formBeans;

import javax.validation.constraints.NotNull;
import org.caprice.cappa.model.db.ControversialEntity;

/**
 *
 * @author Giorgos Hompis <ghompis@csd.uoc.gr>
 */
public class VoteForm {
    
    @NotNull
    private Integer controversialEntityId;
    
    @NotNull
    private Integer value;

    
    public VoteForm() {
    }

    public Integer getControversialEntityId() {
        return controversialEntityId;
    }

    public void setControversialEntityId(Integer controversialEntity) {
        this.controversialEntityId = controversialEntity;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }


    @Override
    public String toString() {
        return "VoteForm{" + "controversialEntity=" + controversialEntityId + ", value=" + value + '}';
    }
    
}
