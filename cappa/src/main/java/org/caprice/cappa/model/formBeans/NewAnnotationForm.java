/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.caprice.cappa.model.formBeans;

import java.sql.Date;
import java.util.List;
import javax.validation.constraints.NotNull;
import org.caprice.cappa.model.db.TextPart;
import org.caprice.cappa.model.db.User;
import org.caprice.cappa.model.db.enums.T_PrivacyConcern;
import org.caprice.cappa.model.db.enums.T_Tag;
import org.hibernate.validator.constraints.NotEmpty;

/**
 *
 * @author Giorgos Hompis <ghompis@csd.uoc.gr>
 */
public class NewAnnotationForm {
    
    @NotEmpty
    private List<TextPart> textParts;
    @NotNull
    private Integer privacyConcernId;
    @NotEmpty
    private List<Integer> tagIds;
    
    private String comment;

    public List<TextPart> getTextParts() {
        return textParts;
    }

    public void setTextParts(List<TextPart> textParts) {
        this.textParts = textParts;
    }

    public Integer getPrivacyConcernId() {
        return privacyConcernId;
    }

    public void setPrivacyConcernId(Integer privacyConcernId) {
        this.privacyConcernId = privacyConcernId;
    }

    public List<Integer> getTagIds() {
        return tagIds;
    }

    public void setTagIds(List<Integer> tagIds) {
        this.tagIds = tagIds;
    }
    
    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    @Override
    public String toString() {
        return "NewAnnotationForm{" + "textParts=" + textParts + ", privacyConcern=" + privacyConcernId + ", tags=" + tagIds + ", comment=" + comment + '}';
    }
    
}
