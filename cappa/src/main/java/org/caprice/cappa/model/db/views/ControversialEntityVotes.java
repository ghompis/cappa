package org.caprice.cappa.model.db.views;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the e_controversial_entity database table.
 * 
 */
@Entity
@Table(name = "v_controversial_entity_votes")
@NamedQuery(name="ControversialEntityVotes.findAll", query="SELECT cv FROM ControversialEntityVotes cv")
public class ControversialEntityVotes implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id", insertable = false, updatable = false)
	private int id;
        
	@Column(name="votes_up", insertable = false, updatable = false)
	private int votesUp;
        
	@Column(name="votes_down", insertable = false, updatable = false)
	private int votesDown;

        
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getVotesUp() {
        return votesUp;
    }

    public void setVotesUp(int votesUp) {
        this.votesUp = votesUp;
    }

    public int getVotesDown() {
        return votesDown;
    }

    public void setVotesDown(int votesDown) {
        this.votesDown = votesDown;
    }
        
        
        

}