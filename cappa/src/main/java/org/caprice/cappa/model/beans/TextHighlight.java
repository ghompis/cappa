/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.caprice.cappa.model.beans;

/**
 *
 * @author Giorgos Hompis <ghompis@csd.uoc.gr>
 */
public class TextHighlight {
    
    private String wrapper;
    private String content;
    private String path;
    private String offset;
    private String length;

    public String getWrapper() {
        return wrapper;
    }

    public void setWrapper(String wrapper) {
        this.wrapper = wrapper;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getOffset() {
        return offset;
    }

    public void setOffset(String offset) {
        this.offset = offset;
    }

    public String getLength() {
        return length;
    }

    public void setLength(String length) {
        this.length = length;
    }

    @Override
    public String toString() {
        return "TextHighlight{" + "wrapper=" + wrapper + ", content=" + content + ", path=" + path + ", offset=" + offset + ", length=" + length + '}';
    }
    
}
