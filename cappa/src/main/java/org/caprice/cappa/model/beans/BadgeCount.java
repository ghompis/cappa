/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.caprice.cappa.model.beans;

import org.caprice.cappa.model.db.enums.T_RewardBadge;

/**
 *
 * @author Giorgos Hompis <ghompis@csd.uoc.gr>
 */
public class BadgeCount {
    
    private T_RewardBadge badge;
    private int count;

    public BadgeCount(T_RewardBadge badge, int count) {
        this.badge = badge;
        this.count = count;
    }

    public T_RewardBadge getBadge() {
        return badge;
    }

    public void setBadge(T_RewardBadge badge) {
        this.badge = badge;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
    
}
