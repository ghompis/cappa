/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.caprice.cappa.model.readability;

import java.util.Arrays;
import java.util.List;

/**
 *
 * @author Giorgos Hompis <ghompis@csd.uoc.gr>
 */
public enum ReadabilityMetric {
    
    FRES("Flesche Readability Ease Score", Arrays.asList(
            new ScoreToReadabilityLevel(90, 100, DifficultyLevel.VERY_EASY),
            new ScoreToReadabilityLevel(80, 90, DifficultyLevel.EASY),
            new ScoreToReadabilityLevel(70, 80, DifficultyLevel.SOMEWHAT_EASY),
            new ScoreToReadabilityLevel(60, 70, DifficultyLevel.STANDARD),
            new ScoreToReadabilityLevel(50, 60, DifficultyLevel.SOMEWHAT_DIFFICULT),
            new ScoreToReadabilityLevel(30, 50, DifficultyLevel.DIFFICULT),
//            new ScoreToReadabilityLevel(75, 90, DifficultyLevel.EASY),
//            new ScoreToReadabilityLevel(55, 75, DifficultyLevel.STANDARD),
//            new ScoreToReadabilityLevel(30, 55, DifficultyLevel.DIFFICULT),
            new ScoreToReadabilityLevel(0,  30, DifficultyLevel.VERY_DIFFICULT) )),

    SMOG("Simple Measure of Gobbledygook", Arrays.asList(
            new ScoreToReadabilityLevel(0,  7,  DifficultyLevel.VERY_EASY),
            new ScoreToReadabilityLevel(7,  8,  DifficultyLevel.EASY),
            new ScoreToReadabilityLevel(8,  9,  DifficultyLevel.EASY),
            new ScoreToReadabilityLevel(9,  10, DifficultyLevel.SOMEWHAT_EASY),
            new ScoreToReadabilityLevel(10, 11, DifficultyLevel.SOMEWHAT_EASY),
            new ScoreToReadabilityLevel(11, 12, DifficultyLevel.STANDARD),
            new ScoreToReadabilityLevel(12, 13, DifficultyLevel.STANDARD),
            new ScoreToReadabilityLevel(13, 16, DifficultyLevel.SOMEWHAT_DIFFICULT),
            new ScoreToReadabilityLevel(16, 17, DifficultyLevel.DIFFICULT),
            new ScoreToReadabilityLevel(17, 19, DifficultyLevel.DIFFICULT),
            new ScoreToReadabilityLevel(19, 99, DifficultyLevel.VERY_DIFFICULT) )),

    CLI("Coleman-Liau Index", Arrays.asList( // US Grade Level
            new ScoreToReadabilityLevel(0,  6,  DifficultyLevel.VERY_EASY),
            new ScoreToReadabilityLevel(6,  7,  DifficultyLevel.EASY),
            new ScoreToReadabilityLevel(7,  8,  DifficultyLevel.EASY),
            new ScoreToReadabilityLevel(8,  9,  DifficultyLevel.EASY),
            new ScoreToReadabilityLevel(9,  10, DifficultyLevel.SOMEWHAT_EASY),
            new ScoreToReadabilityLevel(10, 11, DifficultyLevel.SOMEWHAT_EASY),
            new ScoreToReadabilityLevel(11, 13, DifficultyLevel.STANDARD),
            new ScoreToReadabilityLevel(13, 15, DifficultyLevel.SOMEWHAT_DIFFICULT),
            new ScoreToReadabilityLevel(15, 16, DifficultyLevel.SOMEWHAT_DIFFICULT),
            new ScoreToReadabilityLevel(16, 18, DifficultyLevel.DIFFICULT),
            new ScoreToReadabilityLevel(18, 99, DifficultyLevel.VERY_DIFFICULT) )),

    FKG("Flesh-Kincaid Grade Level", Arrays.asList( // US Grade Level
            new ScoreToReadabilityLevel(0,  6,  DifficultyLevel.VERY_EASY),
            new ScoreToReadabilityLevel(6,  7,  DifficultyLevel.EASY),
            new ScoreToReadabilityLevel(7,  8,  DifficultyLevel.EASY),
            new ScoreToReadabilityLevel(8,  9,  DifficultyLevel.EASY),
            new ScoreToReadabilityLevel(9,  10, DifficultyLevel.SOMEWHAT_EASY),
            new ScoreToReadabilityLevel(10, 11, DifficultyLevel.SOMEWHAT_EASY),
            new ScoreToReadabilityLevel(11, 13, DifficultyLevel.STANDARD),
            new ScoreToReadabilityLevel(13, 15, DifficultyLevel.SOMEWHAT_DIFFICULT),
            new ScoreToReadabilityLevel(15, 16, DifficultyLevel.SOMEWHAT_DIFFICULT),
            new ScoreToReadabilityLevel(16, 18, DifficultyLevel.DIFFICULT),
            new ScoreToReadabilityLevel(18, 99, DifficultyLevel.VERY_DIFFICULT) )),

    ARI("Automated Readability Index", Arrays.asList( // US Grade Level
            new ScoreToReadabilityLevel(0,  6,  DifficultyLevel.VERY_EASY),
            new ScoreToReadabilityLevel(6,  7,  DifficultyLevel.EASY),
            new ScoreToReadabilityLevel(7,  8,  DifficultyLevel.EASY),
            new ScoreToReadabilityLevel(8,  9,  DifficultyLevel.EASY),
            new ScoreToReadabilityLevel(9,  10, DifficultyLevel.SOMEWHAT_EASY),
            new ScoreToReadabilityLevel(10, 11, DifficultyLevel.SOMEWHAT_EASY),
            new ScoreToReadabilityLevel(11, 13, DifficultyLevel.STANDARD),
            new ScoreToReadabilityLevel(13, 15, DifficultyLevel.SOMEWHAT_DIFFICULT),
            new ScoreToReadabilityLevel(15, 16, DifficultyLevel.SOMEWHAT_DIFFICULT),
            new ScoreToReadabilityLevel(16, 18, DifficultyLevel.DIFFICULT),
            new ScoreToReadabilityLevel(18, 99, DifficultyLevel.VERY_DIFFICULT) )),

    GFI("Gunning Fog Index", Arrays.asList( // US Grade Level
            new ScoreToReadabilityLevel(0,  6,  DifficultyLevel.VERY_EASY),
            new ScoreToReadabilityLevel(6,  7,  DifficultyLevel.EASY),
            new ScoreToReadabilityLevel(7,  8,  DifficultyLevel.EASY),
            new ScoreToReadabilityLevel(8,  9,  DifficultyLevel.EASY),
            new ScoreToReadabilityLevel(9,  10, DifficultyLevel.SOMEWHAT_EASY),
            new ScoreToReadabilityLevel(10, 11, DifficultyLevel.SOMEWHAT_EASY),
            new ScoreToReadabilityLevel(11, 13, DifficultyLevel.STANDARD),
            new ScoreToReadabilityLevel(13, 15, DifficultyLevel.SOMEWHAT_DIFFICULT),
            new ScoreToReadabilityLevel(15, 16, DifficultyLevel.SOMEWHAT_DIFFICULT),
            new ScoreToReadabilityLevel(16, 18, DifficultyLevel.DIFFICULT),
            new ScoreToReadabilityLevel(18, 99, DifficultyLevel.VERY_DIFFICULT) )),
    
    ;

    
    private final String name;
    private final List<ScoreToReadabilityLevel> scoreMapping;

    private ReadabilityMetric(String name, List<ScoreToReadabilityLevel> scoreMapping) {
        this.name = name;
        this.scoreMapping = scoreMapping;
    }

    public String getName() {
        return name;
    }

    
    public DifficultyLevel getDifficultyLevel(double score) {
        for( ScoreToReadabilityLevel map : scoreMapping ){
            if( score >= map.getMinValue() && score < map.getMaxValue() ){
                return map.getLevel();
            }
        }
        return null;
    }
    

    
}



class ScoreToReadabilityLevel {
    private final double minValue;
    private final double maxValue;
    private final DifficultyLevel level;

    public ScoreToReadabilityLevel(double minValue, double maxValue, DifficultyLevel level) {
        this.minValue = minValue;
        this.maxValue = maxValue;
        this.level = level;
    }
    public double getMinValue() {
        return minValue;
    }

    public double getMaxValue() {
        return maxValue;
    }

    public DifficultyLevel getLevel() {
        return level;
    }
};