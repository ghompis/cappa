/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.caprice.cappa.model.formBeans;

import java.util.List;
import javax.validation.constraints.NotNull;
import org.hibernate.validator.constraints.NotEmpty;

/**
 *
 * @author Giorgos Hompis <ghompis@csd.uoc.gr>
 */
public class NewProductForm {
    
    @NotEmpty
    private String name;
    @NotEmpty
    private String url;
    @NotEmpty
    private String imageURL;
    @NotNull
    private Integer productType;
    @NotEmpty
    private List<Integer> businessCategories;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        url = url.toLowerCase();
        this.url = url.startsWith("http")? url : "http://"+url;
    }
    
    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public Integer getProductType() {
        return productType;
    }

    public void setProductType(Integer productType) {
        this.productType = productType;
    }

    public List<Integer> getBusinessCategories() {
        return businessCategories;
    }

    public void setBusinessCategories(List<Integer> businessCategories) {
        this.businessCategories = businessCategories;
    }

    @Override
    public String toString() {
        return "NewProductForm{" + "name=" + name + ", url=" + url + ", imageURL=" + imageURL + ", productType=" + productType + ", businessCategories=" + businessCategories + '}';
    }
    
}
