/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.caprice.cappa.model.restBeans;

/**
 *
 * @author Giorgos Hompis <ghompis@csd.uoc.gr>
 */
public class ViewServerResponse extends ServerResponse {
    private String html;

    private ViewServerResponse(String status, String message, String html) {
        super(status, message);
        this.html = html;
    }
    
    public static ViewServerResponse get(Status status, String message, String html){
        return new ViewServerResponse(status.name().toLowerCase(), message, html);
    }
    
    public String getHtml() {
        return html;
    }

    public void setHtml(String html) {
        this.html = html;
    }
    
    
}
