package org.caprice.cappa.model.db.enums;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the t_privacy_concern database table.
 * 
 */
@Entity
@Table(name="t_privacy_concern_attribute")
@NamedQuery(name="T_PrivacyConcernAttribute.findAll", query="SELECT t FROM T_PrivacyConcernAttribute t")
public class T_PrivacyConcernAttribute implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;

	//uni-directional many-to-one association to T_PrivacyConcern
	@ManyToOne
	@JoinColumn(name="t_privacy_concern_id")
	private T_PrivacyConcern PrivacyConcern;
        
	//uni-directional many-to-one association to T_PrivacyConcern
	@ManyToOne
	@JoinColumn(name="t_tag_attribute_id")
	private T_TagAttribute TagAttribute;
        
        @Column(name="is_mandatory")
        private Boolean isMandatory;
        
        @Column(name="is_multivalue")
        private Boolean isMultivalue;
        
        @Column(name="ui_index")
        private Integer uiIndex;
        
        @Column(name="ui_prefix_text")
        private String uiPrefixText;

    public T_PrivacyConcernAttribute() {
    }
        
    public T_PrivacyConcernAttribute(T_PrivacyConcern PrivacyConcern, T_TagAttribute TagAttribute, Boolean isMandatory, Boolean isMultivalue, Integer uiIndex, String uiPrefixText) {
        this.PrivacyConcern = PrivacyConcern;
        this.TagAttribute = TagAttribute;
        this.isMandatory = isMandatory;
        this.isMultivalue = isMultivalue;
        this.uiIndex = uiIndex;
        this.uiPrefixText = uiPrefixText;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public T_PrivacyConcern getPrivacyConcern() {
        return PrivacyConcern;
    }

    public void setPrivacyConcern(T_PrivacyConcern PrivacyConcern) {
        this.PrivacyConcern = PrivacyConcern;
    }

    public T_TagAttribute getTagAttribute() {
        return TagAttribute;
    }

    public void setTagAttribute(T_TagAttribute TagAttribute) {
        this.TagAttribute = TagAttribute;
    }

    public Boolean getIsMandatory() {
        return isMandatory;
    }

    public void setIsMandatory(Boolean isMandatory) {
        this.isMandatory = isMandatory;
    }

    public Boolean getIsMultivalue() {
        return isMultivalue;
    }

    public void setIsMultivalue(Boolean isMultivalue) {
        this.isMultivalue = isMultivalue;
    }
    
    public Integer getUiIndex() {
        return uiIndex;
    }

    public void setUiIndex(Integer uiIndex) {
        this.uiIndex = uiIndex;
    }

    public String getUiPrefixText() {
        return uiPrefixText;
    }

    public void setUiPrefixText(String uiPrefixText) {
        this.uiPrefixText = uiPrefixText;
    }

    @Override
    public String toString() {
        return "T_PrivacyConcernAttribute{" + "id=" + id + ", PrivacyConcern=" + PrivacyConcern + ", TagAttribute=" + TagAttribute + ", isMandatory=" + isMandatory + ", isMultivalue=" + isMultivalue + ", uiIndex=" + uiIndex + ", uiPrefixText=" + uiPrefixText + '}';
    }
    
    
}