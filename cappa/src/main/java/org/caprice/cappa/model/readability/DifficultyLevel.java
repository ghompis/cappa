/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.caprice.cappa.model.readability;

import java.util.Arrays;

/**
 *
 * @author Giorgos Hompis <ghompis@csd.uoc.gr>
 */
public enum DifficultyLevel {
    
    VERY_EASY(1), 
    EASY(2), 
    SOMEWHAT_EASY(3),
    STANDARD(4), 
    SOMEWHAT_DIFFICULT(5),
    DIFFICULT(6), 
    VERY_DIFFICULT(7);
    
    private final int scaleValue;

    private DifficultyLevel(int scaleValue) {
        this.scaleValue = scaleValue;
    }
    
    public String getUiName(){
        return Arrays.stream( this.name().split("_") )
                .map( w -> w.charAt(0) + w.substring(1).toLowerCase() )
                .reduce("", (acc,word) -> acc + " " + word)
                .substring(1);        
    }

    public int getScaleValue() {
        return scaleValue;
    }
    
}
