/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.caprice.cappa.config.mvc;

import java.io.InputStream;
import java.util.Locale;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.core.Ordered;
import org.springframework.web.servlet.view.AbstractUrlBasedView;
import org.springframework.web.servlet.view.InternalResourceView;
import org.springframework.web.servlet.view.UrlBasedViewResolver;

/**
 *
 * @author Giorgos Hompis <ghompis@csd.uoc.gr>
 */
public class FragmentsResolver  extends UrlBasedViewResolver implements Ordered {
    
    public FragmentsResolver() {
        setViewClass(InternalResourceView.class);
    }

    @Override
    protected AbstractUrlBasedView buildView(String viewName) throws Exception {
        String url = getPrefix() + viewName + getSuffix();
        InputStream stream = getServletContext().getResourceAsStream(url);
        if (stream == null) {
            return new NonExistentView();
        }
        return super.buildView(viewName);
    }

    private static class NonExistentView extends AbstractUrlBasedView {
        @Override
        protected boolean isUrlRequired() {
            return false;
        }

        @Override
        public boolean checkResource(Locale locale) throws Exception {
            return false;
        }

        @Override
        protected void renderMergedOutputModel(Map<String, Object> model,
                                               HttpServletRequest request,
                                               HttpServletResponse response) throws Exception {
          // Purposely empty, it should never get called
        }
    }
    
}