/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.caprice.cappa.config;


import org.caprice.cappa.flow.repository.UserRepository;
import org.caprice.cappa.flow.repository.enums.T_EntityStateRepository;
import org.caprice.cappa.flow.repository.enums.T_RewardBadgeRepository;
import org.caprice.cappa.flow.repository.enums.T_UserRoleRepository;
import org.caprice.cappa.model.db.User;
import org.caprice.cappa.model.db.enums.T_EntityState;
import org.caprice.cappa.model.db.enums.T_RewardBadge;
import org.caprice.cappa.model.db.enums.T_UserRole;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.ContextRefreshedEvent;

/**
 *
 * @author Giorgos Hompis <ghompis@csd.uoc.gr>
 */
@Configuration
public class Definitions implements ApplicationListener<ContextRefreshedEvent> {
    
    
    @Autowired
    UserRepository userRepository;
    
    @Autowired
    T_UserRoleRepository userRoleRepository;
    
    @Autowired
    T_EntityStateRepository entityStateRepository;
    
    @Autowired
    T_RewardBadgeRepository rewardBadgeRepository;
    
    
    // Periodically check for document updates
    public static final long DocumentVersionCheckInterval = 1000 * 60 * 60 * 24 * 7;    // Every week
    
    
    /**
     * Session related definitions
     */
    public static final class Attributes {
        public static final String USER = "user";
    }
    
    
    /**
     * Votes related definitions
     */
    public static final class Votes {
        // Param for Wilson Score Interval - Use Z=1.96 for 95% confidence level
        public static final double Z = 1.62; 
    }
    
    /**
     * User role upgrades definitions
     */
    public static final class UserRoles {
        public static final int SCORE_TO_ELEVATED = 10;             // Simple to Elevated user role upgrade score threshold
        public static final int SCORE_TO_CONTENT_EDITOR = 100;      // Elevated to Content editor user role upgrade score threshold
    }
    
    
    /**
     * Define Defaults values for the applications
     */
    public static final class Defaults {
        
        // Default logo image data (base64 encoded) to use as default image in HTML pages
        public static final String DefaultLogoData = "iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAYAAABw4pVUAAAFXUlEQVR42u2bfWhXZRTHZ62otDcryhJ6If"
            + "+osKTVYvz2/srEpUiDmRMqqD8KWlGjVZgTjFCTqCY09K+CECxcoeALJERUQja0BHW0AhGESDKqVVut7xfOE4eHe++2q/SH9/uBw733ebk/Pec+53nO85y"
            + "VlQkhhBBCCCGEEEIIIYQQQgghhBBCCCGEEEIIIYQQQgghikNjY+PNdXV1g/X19X0VFRUXJbVB/f2oX4JrTVxXXV19Neq6IK+h/k1ce2praxdKszlpaGio"
            + "gBInIf9Aoc8mtUHdx9bmq1DW2dl5Mfo+ibKfrS6WbTS2NJzfIJMwyG9QYuV0DIK2a/E87gxwAvIN5C9X9m1zc/OV0nJOg5gch8zPMgiNhvsJM+J3eG4Nb"
            + "VtaWm5E+Xvufe+geJY0nc8g4evekWUQp/AJ9C8ljKhLYKgD1uZHua78BnkZ8mu47+/vvyA2CCdxjgp73p3x3uXuvQ9J0/nmkA5cn7K54RfUNcUGwZxwG6"
            + "6nrf2rae/FSusOzknWr0+azm+Qcvr9MFHD3dzkDQK5PayspmsQ/MZL0nROg7CMKyOukKycxtgXDNLU1HQNrt/PxGXhvSuk6bMwiJXfg7JT0WQfJvXtoZw"
            + "jIWFSL8e79lib03Rz0vRZGsRijVXRkjgsextd2ZcwyoLQp6qq6lJG7Qw0zV29jyDyQmn6HBjEvvaBoFwfGOL+bbT/242gz+jCuMx1xvoBLu56afncGuQq"
            + "yOexQVpbW2fjeV0UmXv5FO++SxqeIVDctVDcE5S0AA4u6VZuQCatltD/FsjzFixuR5sNkDZpVgghhBBC/A8wOmZQxgOjUql0eVZbtsFS9UHIY1i6NnOLf"
            + "RrL5Rsgy2zJvLStrW3uFO3Lsay+l/taFN6zrDAGqampmYf/+EkL0takxBfckd0VTv+ig6oBviPFEFsSAsIxyBux8Zk4gfJuRu0JwSPLutOSKwplEMskOZ"
            + "ESZYfI/RADx9CHu8AoO+jacJvka56d+OQGnhg6A2ZF88H46wptECoMMmJK/xOy2Q6e6FYq8fyhU9gHLKcL9Ee3aPNiUDwNxU1Ey1w5Q7dn2zKL3Xu4Zf8"
            + "IRxCF924bn5uQi4tskG6nqI2xLzeD7bX63+nvuS/lRsK2sihxgfMO2rzFvTEzHg+6PnHubFGC+1tkdZPWtryoBhmy0XGmo6PjsqT+llnyh7VbjesLYURB"
            + "6Q9M9fvckrfRwj6b09qxLvxb/DZ+YQzCBDc8Hw5fZVp/fzJIdwR5103E/80rNChXabFEZyZdGauvLteuvnAGwf0cyFH7Knel9W9vb7/CcrTYfwhtP7I+x"
            + "/xKisvelAXBo25+WJr2O6wrxDyS4bJmufONkQyXwy33n6zdQHAtdqw7389H/B37rTG3crovZDairjfDZfVan3HOVUWdQza6r7KU8uU+7XJ+V+D54XBymJ"
            + "S0YPHGF1Z/wFzeqL1jOCnWsD7D1maUfQppEPzH73SpoAeZ6hNP6G50jOJd11lAGIK7kfho1lzPmM/Dwjs2OcO/4ldRNAbTiJyL21SkZe9+GsXkOc4BqFv"
            + "vzsSZZ7XV6oecYsf9hIz7lS7Bml//MzSEKXbMFHskTPrcTgnxThgpdqK4wYLOUD4y1dbL+WYQP9meZB3PxO3vOdKidCa59UTxBuefPi59U/qdipfElk50"
            + "OGs3AKPt7vM+UrfomV/ioBeW+T8RMPc0aFmJRy175PWsBGkL6DiihrnqssTqNWkZJuaeHkebnZaARwPtZFkh9rHy7g5zSTyTPlQm3d9M8q64C8BYSBoXQ"
            + "gghhBBCCCGEEEIIIYQQQgghhBBCCCGEEEIIIYQQefgXKTTR2pTTch0AAAAASUVORK5CYII=";
        
        
        
        public static final class Users {
            public static User ADMIN;
            public static User CAPRICE;
            public static User USABLE_PRIVACY;
        }
        
        public static final class Roles {
            public static T_UserRole ADMIN;
            public static T_UserRole CONTENT_EDITOR;
            public static T_UserRole ELEVATED_USER;
            public static T_UserRole USER;
        }
        
        public static final class State {
            public static T_EntityState UNCLARIFIED;
            public static T_EntityState VALID;
            public static T_EntityState INVALID;
        }
        
        public static final class RewardBadge {
            public static T_RewardBadge Vote_Up;
            public static T_RewardBadge Vote_Down;

            public static T_RewardBadge Annotation_Created;
            public static T_RewardBadge Document_Added;
            public static T_RewardBadge Product_Added;

            public static T_RewardBadge Annotation_Upvoted;
            public static T_RewardBadge Document_Upvoted;
            public static T_RewardBadge Product_Upvoted;

            public static T_RewardBadge Annotation_Downvoted;
            public static T_RewardBadge Document_Downvoted;
            public static T_RewardBadge Product_Downvoted;

            // ======================================== Special Badges
            public static T_RewardBadge Annotation_Validated;
            public static T_RewardBadge Document_Validated;
            public static T_RewardBadge Product_Validated;

            public static T_RewardBadge Annotation_Invalidated;
            public static T_RewardBadge Document_Invalidated;
            public static T_RewardBadge Product_Invalidated;

            // ======================================== Goal Badges
            public static T_RewardBadge Total_Annotated_1;
            public static T_RewardBadge Total_Annotated_10;
            public static T_RewardBadge Total_Annotated_50;
            public static T_RewardBadge Total_Annotated_100;
            public static T_RewardBadge Total_Annotated_500;
            public static T_RewardBadge Total_Annotated_1000;
        }
        
    }
    
    
    
    
    /**
     * Load/Initialize default variables from database values on application startup
     * @param event 
     */
    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
        // ======================================== Default Users load
        Definitions.Defaults.Users.ADMIN            = userRepository.findByUsername("admin").get(0);
        Definitions.Defaults.Users.CAPRICE          = userRepository.findByUsername("CAPRICE").get(0);
        Definitions.Defaults.Users.USABLE_PRIVACY   = userRepository.findByUsername("Usable Privacy").get(0);
        
        // ======================================== Default User Roles load
        Definitions.Defaults.Roles.ADMIN            = userRoleRepository.findByName("admin").get(0);
        Definitions.Defaults.Roles.CONTENT_EDITOR   = userRoleRepository.findByName("Content Editor").get(0);
        Definitions.Defaults.Roles.ELEVATED_USER    = userRoleRepository.findByName("Elevated User").get(0);
        Definitions.Defaults.Roles.USER             = userRoleRepository.findByName("Simple User").get(0);
        
        // ======================================== Default Entity States load
        Definitions.Defaults.State.UNCLARIFIED  = entityStateRepository.findByName("Unclarified").get(0);
        Definitions.Defaults.State.VALID        = entityStateRepository.findByName("Valid").get(0);
        Definitions.Defaults.State.INVALID      = entityStateRepository.findByName("Invalid").get(0);
        
        // ======================================== Default Badges load from database
        Definitions.Defaults.RewardBadge.Vote_Up                 = rewardBadgeRepository.findByName("Vote Up").get(0);
        Definitions.Defaults.RewardBadge.Vote_Down               = rewardBadgeRepository.findByName("Vote Down").get(0);

        Definitions.Defaults.RewardBadge.Annotation_Created      = rewardBadgeRepository.findByName("Annotation Created").get(0);
        Definitions.Defaults.RewardBadge.Document_Added          = rewardBadgeRepository.findByName("Document Added").get(0);
        Definitions.Defaults.RewardBadge.Product_Added           = rewardBadgeRepository.findByName("Product Added").get(0);
        Definitions.Defaults.RewardBadge.Annotation_Upvoted      = rewardBadgeRepository.findByName("Annotation Upvoted").get(0);
        Definitions.Defaults.RewardBadge.Document_Upvoted        = rewardBadgeRepository.findByName("Document Upvoted").get(0);
        Definitions.Defaults.RewardBadge.Product_Upvoted         = rewardBadgeRepository.findByName("Product Upvoted").get(0);
        Definitions.Defaults.RewardBadge.Annotation_Downvoted    = rewardBadgeRepository.findByName("Annotation Downvoted").get(0);
        Definitions.Defaults.RewardBadge.Document_Downvoted      = rewardBadgeRepository.findByName("Document Downvoted").get(0);
        Definitions.Defaults.RewardBadge.Product_Downvoted       = rewardBadgeRepository.findByName("Product Downvoted").get(0);

        Definitions.Defaults.RewardBadge.Annotation_Validated    = rewardBadgeRepository.findByName("Annotation Validated").get(0);
        Definitions.Defaults.RewardBadge.Document_Validated      = rewardBadgeRepository.findByName("Document Validated").get(0);
        Definitions.Defaults.RewardBadge.Product_Validated       = rewardBadgeRepository.findByName("Product Validated").get(0);
        Definitions.Defaults.RewardBadge.Annotation_Invalidated  = rewardBadgeRepository.findByName("Annotation Invalidated").get(0);
        Definitions.Defaults.RewardBadge.Document_Invalidated    = rewardBadgeRepository.findByName("Document Invalidated").get(0);
        Definitions.Defaults.RewardBadge.Product_Invalidated     = rewardBadgeRepository.findByName("Product Invalidated").get(0);
        
    }
    
    
    
}
