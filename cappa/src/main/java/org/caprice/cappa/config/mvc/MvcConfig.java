/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.caprice.cappa.config.mvc;

import java.lang.reflect.Method;
import org.caprice.cappa.model.restBeans.DataServerResponse;
import org.caprice.cappa.model.restBeans.ViewServerResponse;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.web.WebMvcRegistrationsAdapter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.mvc.condition.PatternsRequestCondition;
import org.springframework.web.servlet.mvc.method.RequestMappingInfo;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;
import org.springframework.web.servlet.view.UrlBasedViewResolver;

/**
 *
 * @author Giorgos Hompis <ghompis@csd.uoc.gr>
 */
@Configuration
public class MvcConfig extends WebMvcConfigurerAdapter {
    
    @Value("${rest.api.base}")
    private String REST_API_BASE;
    
    @Value("${rest.api.base.data}")
    private String REST_API_BASE_DATA;
    
    @Value("${rest.api.base.view}")
    private String REST_API_BASE_VIEW;
    
    
    /**
     * Setup view resolver prefix and suffix for all jsp pages. 
     * Used in @Controller classes
     * @return 
     */
    @Bean
    public ViewResolver getPageViewResolver(){
        System.out.println("Page View Resolver Loaded");
        InternalResourceViewResolver resolver = new InternalResourceViewResolver();
        resolver.setPrefix("/WEB-INF/pages/");
        resolver.setSuffix(".jsp");
        resolver.setViewClass(JstlView.class);
        resolver.setOrder(1);
        return resolver;
    }

    
    /**
     * Setup view resolver prefix jspf page fragments. 
     * Used in @Controller classes
     * @return 
     */
    @Bean
    public ViewResolver getFragmentViewResolver(){
        UrlBasedViewResolver resolver = new FragmentsResolver();
        resolver.setPrefix("/WEB-INF/fragments/");
        resolver.setSuffix(".jsp");
        resolver.setViewClass(JstlView.class);
        resolver.setOrder(0);
        return resolver;
    }
    
    
    /**
     * Setup view resolver for all jsp pages.
     * All @RestController classes follow the REST_API_BASE prefix
     * @return 
     */
    @Bean
    public WebMvcRegistrationsAdapter webMvcRegistrationsHandlerMapping() {
        return new WebMvcRegistrationsAdapter() {
            @Override
            public RequestMappingHandlerMapping getRequestMappingHandlerMapping() {
                return new RequestMappingHandlerMapping() {
//                    private final static String API_BASE_PATH = "api";
 
                    @Override
                    protected void registerHandlerMethod(Object handler, Method method, RequestMappingInfo mapping) {
                        Class<?> beanType = method.getDeclaringClass();
                        if (AnnotationUtils.findAnnotation(beanType, RestController.class) != null) {
                            PatternsRequestCondition apiPattern;
                            // =============================== IMPORTANT ===============================
                            // Attach a url prefix to RequestMappings on @RestController classes depending on 
                            // the method's return type (api/data vs api/view)
                            // =======================================================================
                            if( method.getReturnType().equals( DataServerResponse.class ) ) {
                                apiPattern = new PatternsRequestCondition(REST_API_BASE_DATA).combine(mapping.getPatternsCondition());
                            }
                            else if( method.getReturnType().equals( ViewServerResponse.class ) ) {
                                apiPattern = new PatternsRequestCondition(REST_API_BASE_VIEW).combine(mapping.getPatternsCondition());
                            }
                            else {
                                apiPattern = new PatternsRequestCondition(REST_API_BASE).combine(mapping.getPatternsCondition());
                            }
 
                            mapping = new RequestMappingInfo(mapping.getName(), apiPattern,
                                    mapping.getMethodsCondition(), mapping.getParamsCondition(),
                                    mapping.getHeadersCondition(), mapping.getConsumesCondition(),
                                    mapping.getProducesCondition(), mapping.getCustomCondition());
                        }
 
                        super.registerHandlerMethod(handler, method, mapping);
                    }
                };
            }
        };
    }
    
}
