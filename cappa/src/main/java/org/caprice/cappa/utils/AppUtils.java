/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.caprice.cappa.utils;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Locale;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import org.caprice.cappa.config.Definitions;
import org.caprice.cappa.flow.repository.UserRepository;
import org.caprice.cappa.model.beans.DocumentDownloadResult;
import org.caprice.cappa.model.beans.ExceptionMessage;
import org.caprice.cappa.model.beans.WilsonScoreIntervalResult;
import org.caprice.cappa.model.db.User;
import org.caprice.cappa.model.readability.ReadabilityScoresWrapper;
import org.caprice.cappa.model.restBeans.ServerResponse;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.owasp.html.PolicyFactory;
import org.owasp.html.Sanitizers;

/**
 *
 * @author Giorgos Hompis <ghompis@csd.uoc.gr>
 */
public class AppUtils {

    
    public static DocumentDownloadResult downloadAndSanitizeHtmlDocument(String urlString) {
        try {
            Connection connection = Jsoup.connect(urlString);
            // Get document data
            String html = connection.get().html();
            String sanitizedHtml = sanitizeHtmlDocument(html);
            // Get last modified
            SimpleDateFormat dateFormat = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss z", Locale.US);
            dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
            String headerField = connection.response().header("Last-Modified");
            Long lastModified = headerField!=null? dateFormat.parse(headerField).getTime() : null;
            // Return
            return new DocumentDownloadResult(sanitizedHtml, lastModified);
        } catch (IOException | ParseException ex) {
            Logger.getLogger(AppUtils.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    
    public static String sanitizeHtmlDocument(String html) {
        PolicyFactory policy = Sanitizers.FORMATTING
                .and(Sanitizers.BLOCKS)
                .and(Sanitizers.LINKS);
        return policy.sanitize(html);
    }

    
    public static String getTextFromHtml(String html){
        return Jsoup.parse(html).text();
    }
    
    
    public static String getLastModifiedDate(String urlString) {
        try {
            URL obj = new URL(urlString);
            URLConnection conn = obj.openConnection();
            return conn.getHeaderField("Last-Modified");
        } catch (MalformedURLException ex) {
            Logger.getLogger(AppUtils.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(AppUtils.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static String downloadAndFormatProductIcon(String urlString) {
        try {
            // Read image file
            URL url = new URL(urlString);
            BufferedImage remoteImg = ImageIO.read(url);
            // Scale
            Image tmp = remoteImg.getScaledInstance(100, 100, Image.SCALE_SMOOTH);
            BufferedImage newImg = new BufferedImage(100, 100, BufferedImage.TYPE_INT_ARGB);
            Graphics2D g2d = newImg.createGraphics();
            g2d.drawImage(tmp, 0, 0, null);
            g2d.dispose();
            // Convert to png
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ImageIO.write(newImg, "png", baos);
            // Convert to base64
            return Base64.getEncoder().encodeToString(baos.toByteArray());
        } catch (IOException | RuntimeException e) {
            return Definitions.Defaults.DefaultLogoData;
        }
    }

    
    public static void updateUserRoles(User user, UserRepository repository) throws ExceptionMessage {
        User updatedUser = repository.findOne(user.getId());
        if (updatedUser.getScore() >= Definitions.UserRoles.SCORE_TO_ELEVATED) {
            updatedUser.removeUserRoles(Definitions.Defaults.Roles.USER);
            updatedUser.addUserRoles(Definitions.Defaults.Roles.ELEVATED_USER);
            repository.save(updatedUser);
            throw new ExceptionMessage(ServerResponse.Status.SUCCESS, "Congratulations! Your account has been upgraded to ELEVATED USER!");

        } else if (updatedUser.getScore() >= Definitions.UserRoles.SCORE_TO_CONTENT_EDITOR) {
            updatedUser.removeUserRoles(Definitions.Defaults.Roles.ELEVATED_USER);
            updatedUser.addUserRoles(Definitions.Defaults.Roles.CONTENT_EDITOR);
            repository.save(updatedUser);
            throw new ExceptionMessage(ServerResponse.Status.SUCCESS, "Congratulations! Your account has been upgraded to CONTENT EDITOR!");
        }

    }

    
    /**
     * Calculates and returns the Wilson score interval result
     * @param ns number of success trials
     * @param nf number of fail trials
     * @return 
     */
    public static WilsonScoreIntervalResult calcWilsonScoreInterval(int ns, int nf) {
        if (ns + nf == 0) {
            return new WilsonScoreIntervalResult(0, 0, 1);
        }
        // Using wilson score interval - wikipedia
        double n = ns + nf;
        double z = Definitions.Votes.Z;
        double z_2 = Math.pow(z, 2);

        double prop = 1.0 * ns / (ns + nf);
        double val = (ns + z_2 / 2.0) / (n + z_2);
        double offset = (z / (n + z_2)) * Math.sqrt((ns * nf / n) + (z_2 / 4.0));
        return new WilsonScoreIntervalResult(prop, val - offset, val + offset);
    }

    
    public static String getMD5HashAsBase64(String value) {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(value.getBytes());
            byte[] digest = md.digest();
            return Base64.getEncoder().encodeToString(digest);
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(AppUtils.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "";
    }

    
    public static ReadabilityScoresWrapper getReadabilityScores(String html){
        return new ReadabilityScoresWrapper( getTextFromHtml(html) );
    }
    
    
    public static boolean isTextDiffInHtml(String oldHtml, String newHtml) {
        return getTextFromHtml(oldHtml)
                .compareTo( getTextFromHtml(newHtml) ) != 0;
    }

}
