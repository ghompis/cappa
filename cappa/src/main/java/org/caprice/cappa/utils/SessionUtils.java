/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.caprice.cappa.utils;

import javax.servlet.http.HttpServletRequest;
import org.caprice.cappa.config.Definitions;
import org.caprice.cappa.flow.repository.UserRepository;
import org.caprice.cappa.model.db.User;

/**
 *
 * @author Giorgos Hompis <ghompis@csd.uoc.gr>
 */
public class SessionUtils {
    
    
    public static User getLoggedInUser(UserRepository userRepository, HttpServletRequest request){
        User sessionUser = (User) request.getSession().getAttribute( Definitions.Attributes.USER );
        if( sessionUser != null ){
            User dbUser = userRepository.findOne( sessionUser.getId() );    // Reload logged user from db
            setLoggedInUser(request, dbUser);                               // Update session user
            return dbUser;
        } else {
            return null;
        }
    }
    
    public static void setLoggedInUser(HttpServletRequest request, User user){
        request.getSession().setAttribute(Definitions.Attributes.USER, user);
    }
    
    public static void removeLoggedInUser(HttpServletRequest request){
        request.getSession().removeAttribute(Definitions.Attributes.USER);
    }
}
