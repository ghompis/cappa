/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.caprice.cappa.utils.converters;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import org.caprice.cappa.model.beans.TextHighlight;
import org.caprice.cappa.model.db.TextPart;

/**
 *
 * @author Giorgos Hompis <ghompis@csd.uoc.gr>
 */
public class TextHighlightUtils {
    
    
    public static TextHighlight parseHighlightData(TextPart part){
        try {
            return (TextHighlight) new ObjectMapper().readValue(part.getHighlightsData(), List.class);
        } catch (IOException ex) {
            Logger.getLogger(TextHighlightUtils.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    
    public static List<TextHighlight> parseHighlightData(List<TextPart> parts){
        final ObjectMapper objectMapper = new ObjectMapper();
        return parts.stream()
                .map( part -> part.getHighlightsData() )
                .map( data -> {
                    try{                    
                        return (TextHighlight) objectMapper.readValue(data, TextHighlight.class);
                    } catch (IOException ex) {
                        Logger.getLogger(TextHighlightUtils.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    return null;
                })
                .collect( Collectors.toList() );
    }
}
