/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.caprice.cappa.utils.converters;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.caprice.cappa.config.Definitions;
import org.caprice.cappa.model.beans.BadgeCount;
import org.caprice.cappa.model.db.views.UserStats;

/**
 *
 * @author Giorgos Hompis <ghompis@csd.uoc.gr>
 */
public class Converters {
    
    /**
     * Convert any object to a json string object
     * @param value
     * @return 
     */
    public static String toJson(Object value){
        try {   
            return new ObjectMapper().writeValueAsString( value );
        } catch (JsonProcessingException ex) {
            Logger.getLogger(Converters.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
            
    
    /**
     * Convert UserStatistics object to a list of BadgeCount objects
     * @param us
     * @return 
     */
    public static List<BadgeCount> getRewards(UserStats us){
        if( us == null ) 
            return Collections.EMPTY_LIST;
        List<BadgeCount> rewards = new ArrayList<>( Arrays.asList(
            // ================================================== Simple Badges
            new BadgeCount( Definitions.Defaults.RewardBadge.Vote_Up,                           us.getOutputUpvotesCount() ),
            new BadgeCount( Definitions.Defaults.RewardBadge.Vote_Down,                         us.getOutputDownvotesCount() ),
            
            new BadgeCount( Definitions.Defaults.RewardBadge.Annotation_Created,                us.getAnnotationsCreated() ),
            new BadgeCount( Definitions.Defaults.RewardBadge.Document_Added,                    us.getLegalDocumentsAdded() ),
            new BadgeCount( Definitions.Defaults.RewardBadge.Product_Added,                     us.getDigitalProductsAdded() ),
            
            new BadgeCount( Definitions.Defaults.RewardBadge.Annotation_Upvoted,                us.getAnnotationInputUpvotesCount() ),
            new BadgeCount( Definitions.Defaults.RewardBadge.Document_Upvoted,                  us.getLegalDocumentInputUpvotesCount() ),
            new BadgeCount( Definitions.Defaults.RewardBadge.Product_Upvoted,                   us.getDigitalProductInputUpvotesCount() ),
            
            new BadgeCount( Definitions.Defaults.RewardBadge.Annotation_Downvoted,              us.getAnnotationInputDownvotesCount() ),
            new BadgeCount( Definitions.Defaults.RewardBadge.Document_Downvoted,                us.getLegalDocumentInputDownvotesCount() ),
            new BadgeCount( Definitions.Defaults.RewardBadge.Product_Downvoted,                 us.getDigitalProductInputDownvotesCount() ),
            
            // ================================================== Special Badges
            new BadgeCount( Definitions.Defaults.RewardBadge.Annotation_Validated,              us.getAnnotationValidatedCount() ),
            new BadgeCount( Definitions.Defaults.RewardBadge.Document_Validated,                us.getLegalDocumentValidatedCount() ),
            new BadgeCount( Definitions.Defaults.RewardBadge.Product_Validated,                 us.getDigitalProductValidatedCount() ),
            
            new BadgeCount( Definitions.Defaults.RewardBadge.Annotation_Invalidated,            us.getAnnotationInvalidatedCount() ),
            new BadgeCount( Definitions.Defaults.RewardBadge.Document_Invalidated,              us.getLegalDocumentInvalidatedCount() ),
            new BadgeCount( Definitions.Defaults.RewardBadge.Product_Invalidated,               us.getDigitalProductInvalidatedCount() ),
            
            // ================================================== Goal Badges
            new BadgeCount( Definitions.Defaults.RewardBadge.Total_Annotated_1,                 us.getAnnotationValidatedCount()>=1? 1 : 0 ),
            new BadgeCount( Definitions.Defaults.RewardBadge.Total_Annotated_10,                us.getAnnotationValidatedCount()>=10? 1 : 0 ),
            new BadgeCount( Definitions.Defaults.RewardBadge.Total_Annotated_50,                us.getAnnotationValidatedCount()>=50? 1 : 0 ),
            new BadgeCount( Definitions.Defaults.RewardBadge.Total_Annotated_100,               us.getAnnotationValidatedCount()>=100? 1 : 0 ),
            new BadgeCount( Definitions.Defaults.RewardBadge.Total_Annotated_500,               us.getAnnotationValidatedCount()>=500? 1 : 0 ),
            new BadgeCount( Definitions.Defaults.RewardBadge.Total_Annotated_1000,              us.getAnnotationValidatedCount()>=1000? 1 : 0 )
        ));
        return rewards;
    }
    
    
    
    
}
