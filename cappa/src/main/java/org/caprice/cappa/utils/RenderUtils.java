/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.caprice.cappa.utils;

import java.io.UnsupportedEncodingException;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.servlet.View;
import org.springframework.web.servlet.ViewResolver;

/**
 *
 * @author Giorgos Hompis <ghompis@csd.uoc.gr>
 */
public class RenderUtils {
    
    public static String renderView(String viewName, ViewResolver resolver, HttpServletRequest req, Model model) {
        try {
            View resolvedView = resolver.resolveViewName(viewName, Locale.US);
            MockHttpServletResponse mockResp = new MockHttpServletResponse();
            resolvedView.render(model.asMap(), req, mockResp);
            return mockResp.getContentAsString();
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(RenderUtils.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(RenderUtils.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public static String renderView(String viewName, ViewResolver resolver, HttpServletRequest req, ModelMap model) {
        try {
            View resolvedView = resolver.resolveViewName(viewName, Locale.US);
            MockHttpServletResponse mockResp = new MockHttpServletResponse();
            resolvedView.render(model, req, mockResp);
            return mockResp.getContentAsString();
        } catch (Exception ex) {
            Logger.getLogger(RenderUtils.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
}
